package bd.edu.daffodilvarsity.classmanager.common.extensions

import bd.edu.daffodilvarsity.classmanager.utils.CustomException
import com.google.common.truth.Truth.assertThat
import org.junit.Test
import java.net.ConnectException

class ExtensionsTest {

    @Test
    fun testRunWithoutException() {
        runWithoutException {
            throw CustomException("This exception should not be thrown")
        }
    }

    @Test(expected = NullPointerException::class)
    fun testMakeToast() {
        throw NullPointerException()
    }

    @Test
    fun getNetworkErrorMessage_throwSomeIONetworkException_specificErrorMessageReturned() {
        val connectionException = ConnectException()
        val message = connectionException.networkErrorMessage
        assertThat(message).isEqualTo("Unable to connect to the server. Please check your connection")
    }

    @Test
    fun getNetworkErrorMessage_throwNonNetworkError_specificErrorMessageReturned() {
        val nullPointerException = NullPointerException("This is a custom message")
        val message = nullPointerException.networkErrorMessage
        assertThat(message).isEqualTo("Error occurred please try again")
    }

}