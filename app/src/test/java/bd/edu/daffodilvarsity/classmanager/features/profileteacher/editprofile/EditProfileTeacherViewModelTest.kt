package bd.edu.daffodilvarsity.classmanager.features.profileteacher.editprofile

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.SavedStateHandle
import bd.edu.daffodilvarsity.classmanager.common.models.ProfileTeacher
import bd.edu.daffodilvarsity.classmanager.common.repository.FakeRepository
import bd.edu.daffodilvarsity.classmanager.testutil.TestCoroutineRule
import bd.edu.daffodilvarsity.classmanager.testutil.getOrAwaitValue
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.hamcrest.CoreMatchers.`is`
import org.junit.Assert.*
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import java.util.concurrent.TimeoutException

@ExperimentalCoroutinesApi
class EditProfileTeacherViewModelTest {

    @get:Rule
    val testInstantTaskExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    val testCoroutineRule = TestCoroutineRule()

    private lateinit var viewModel: EditProfileTeacherViewModel

    @Before
    fun setUp() {
        viewModel = EditProfileTeacherViewModel(FakeRepository(), SavedStateHandle())
    }

    @Test
    fun `initial state of profile live data is null`() = testCoroutineRule.runBlockingTest {
        val initialProfile = try { viewModel.profile.getOrAwaitValue() } catch (e: TimeoutException) { null }
        assertNull(initialProfile)
    }

    @Test
    fun `initialize profile, profile initialized with a given value`() = testCoroutineRule.runBlockingTest {
        val profile = ProfileTeacher("123", "A", "A", "A", "A", "A", "A", 6)
        viewModel.initializeProfile(profile)
        val result = viewModel.profile.getOrAwaitValue()
        assertThat(result, `is`(profile))
    }

    @Test
    fun `save updated profile, updated profile saved`() = testCoroutineRule.runBlockingTest {
        // Arrange
        val profile = ProfileTeacher("id", "name", "email", "dep", "initial", "designation", "contact-no", 1)
        // Act
        viewModel.saveProfile(profile)
        // Assert
        val result = viewModel.profile.getOrAwaitValue()
        assertEquals(result, profile)
    }

}