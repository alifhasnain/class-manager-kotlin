package bd.edu.daffodilvarsity.classmanager.features.profilestudent.editprofile

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.SavedStateHandle
import bd.edu.daffodilvarsity.classmanager.common.models.ProfileStudent
import bd.edu.daffodilvarsity.classmanager.common.repository.FakeRepository
import bd.edu.daffodilvarsity.classmanager.testutil.TestCoroutineRule
import bd.edu.daffodilvarsity.classmanager.testutil.getOrAwaitValue
import bd.edu.daffodilvarsity.classmanager.testutil.getOrReturnNullIfTimeOut
import com.google.common.truth.Truth.assertThat
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.Before

import org.junit.Rule
import org.junit.Test

@ExperimentalCoroutinesApi
class EditProfileStudentViewModelTest {

    @get:Rule
    val testInstantTaskExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    val testCoroutineRule = TestCoroutineRule()

    private lateinit var viewModel: EditProfileStudentViewModel

    @Before
    fun setUp() {
        viewModel = EditProfileStudentViewModel(FakeRepository(), SavedStateHandle())
    }

    @Test
    fun `initialize profile livedata with initial value, livedata initialized`() = testCoroutineRule.runBlockingTest {
        // Arrange
        val profile = ProfileStudent("alk", "", "", "", "", "", "", "", "")
        // Act
        viewModel.initializeProfile(profile)
        // Assert
        assertThat(viewModel.profile.getOrAwaitValue()).isEqualTo(profile)
    }

    @Test
    fun `save profile to server, profile saved`() = testCoroutineRule.runBlockingTest {
        // Arrange
        val profile = ProfileStudent("alk", "", "", "", "", "", "", "", "")
        // Act
        viewModel.saveProfile(profile)
        // Assert
        assertThat(viewModel.profile.getOrAwaitValue()).isEqualTo(profile)
    }

    @Test
    fun `save profile and updated courses according to level term, success`() = testCoroutineRule.runBlockingTest {
        // Arrange
        val profile = ProfileStudent("alk", "", "", "", "", "", "", "", "")
        /*
        * Initially registered courses should be null
        * so we assert that case
        * */
        val registeredCourses = viewModel.registeredCourse.getOrReturnNullIfTimeOut()
        assertThat(registeredCourses).isNull()
        // Act
        viewModel.saveProfileAndUpdateCourses(profile)
        // Assert
        assertThat(viewModel.profile.getOrAwaitValue()).isEqualTo(profile)
        assertThat(viewModel.registeredCourse.getOrReturnNullIfTimeOut()).isNotNull()
    }

}