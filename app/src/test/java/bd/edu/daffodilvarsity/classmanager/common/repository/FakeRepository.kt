package bd.edu.daffodilvarsity.classmanager.common.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.paging.PagingData
import androidx.sqlite.db.SimpleSQLiteQuery
import bd.edu.daffodilvarsity.classmanager.common.models.*
import kotlinx.coroutines.flow.Flow
import okhttp3.ResponseBody

class FakeRepository: Repository {

    private var teacherProfile = ProfileTeacher(
        "id", "name", "email", "department", "XXX", "YYY", "017XXX", 1
    )

    private var studentProfile = ProfileStudent(
        "171-15-8966", "NAME", "EMAIL", "Z", "1", "2", "CSE", "CAM", "Day"
    )

    private val registeredCourse = mutableListOf(
        RegisteredCourse("CODE1", "A"),
        RegisteredCourse("CODE2", "B"),
        RegisteredCourse("CODE3", "B"),
        RegisteredCourse("CODE4", "A"),
    )

    override suspend fun fetchTeacherProfileFromServer(): ProfileTeacher = teacherProfile

    override suspend fun saveTeacherProfileToServer(profile: ProfileTeacher): ProfileTeacher {
        teacherProfile = profile
        return teacherProfile
    }

    override suspend fun fetchStudentProfileFromServer() = studentProfile

    override suspend fun saveStudentProfileToServer(profile: ProfileStudent): ProfileStudent {
        studentProfile = profile
        return studentProfile
    }

    override suspend fun saveProfileAndUpdateRegisteredCourse(profile: ProfileStudent): Pair<ProfileStudent, MutableList<RegisteredCourse>> {
        saveStudentProfileToServer(profile)
        val updatedCourses = mutableListOf(
            RegisteredCourse("C1", "A"),
            RegisteredCourse("C2", "A"),
            RegisteredCourse("C3", "C"),
            RegisteredCourse("C4", "D"),
        )
        return profile to updatedCourses
    }

    override suspend fun fetchRegisteredCourses() = registeredCourse

    override suspend fun addRegisteredCourse(course: RegisteredCourse): MutableList<RegisteredCourse> {
        registeredCourse.add(course)
        return registeredCourse
    }

    override suspend fun removeRegisteredCourse(course: RegisteredCourse): MutableList<RegisteredCourse> {
        registeredCourse.removeIf { it.courseCode == course.courseCode }
        return registeredCourse
    }

    override suspend fun getCourseListFromServer(
        level: String,
        term: String,
        shift: String
    ): MutableList<Course> {
        TODO("Not yet implemented")
    }

    override suspend fun getCRDetailsList(
        level: String,
        term: String,
        shift: String,
        campus: String
    ): List<CRProfile> {
        TODO("Not yet implemented")
    }

    override suspend fun updateFCMToken(token: String): ResponseMessage {
        TODO("Not yet implemented")
    }

    override suspend fun getAllSectionListFromServer(): MutableList<Pair<String, String>> {
        TODO("Not yet implemented")
    }

    override fun getTeacherProfileStream(
        queryString: String,
        limit: Int
    ): Flow<PagingData<ProfileTeacher>> {
        TODO("Not yet implemented")
    }

    override suspend fun completeStudentProfile(profile: ProfileStudent): ResponseMessage {
        TODO("Not yet implemented")
    }

    override suspend fun completeTeacherProfile(profile: ProfileTeacher): ResponseMessage {
        TODO("Not yet implemented")
    }

    override suspend fun getCoursesFromLocal(
        level: Int,
        term: Int,
        shift: String,
        department: String
    ): MutableList<Course> {
        TODO("Not yet implemented")
    }

    override fun getCoursesFromLocalAsLiveData(
        shift: String,
        department: String
    ): LiveData<List<Course>> {
        TODO("Not yet implemented")
    }

    override fun getDistinctSectionsFromLocal(): LiveData<List<String>> =
        MutableLiveData(mutableListOf("A", "B", "X", "Y"))

    override suspend fun getConcatenateCourseCodeAndCourseName(
        shift: String,
        department: String
    ): List<String> {
        TODO("Not yet implemented")
    }

    override suspend fun updateRoutineAndCourseFromDatabase() {
        TODO("Not yet implemented")
    }

    override suspend fun refreshAllCoursesFromServer() {
        TODO("Not yet implemented")
    }

    override suspend fun fetchAllSectionFromServer(): MutableList<String> {
        TODO("Not yet implemented")
    }

    override fun getTeacherRoutineForSpecificDayAsFlow(
        initial: String,
        day: String
    ): Flow<MutableList<ClassDetails>> {
        TODO("Not yet implemented")
    }

    override fun getTeacherWholeRoutineAsFlow(initial: String): Flow<List<ClassDetails>> {
        TODO("Not yet implemented")
    }

    override suspend fun getTeacherRoutine(
        initial: String,
        day: String
    ): MutableList<ClassDetails> {
        TODO("Not yet implemented")
    }

    override fun getStudentRoutineAsFlow(rawQuery: SimpleSQLiteQuery): Flow<MutableList<ClassDetails>> {
        TODO("Not yet implemented")
    }

    override suspend fun getStudentRoutineDayOfWeekSorted(
        courseList: MutableList<String>,
        section: String,
        shift: String,
        campus: String,
        department: String
    ): MutableList<ClassDetails> {
        TODO("Not yet implemented")
    }

    override suspend fun getRoutineWithCourseCode(
        courseCode: String,
        shift: String,
        campus: String,
        department: String
    ): MutableList<ClassDetails> {
        TODO("Not yet implemented")
    }

    override suspend fun getCourseVersion(): String {
        TODO("Not yet implemented")
    }

    override suspend fun getSectionVersion(): String {
        TODO("Not yet implemented")
    }

    override suspend fun getRoutineVersion(): String {
        TODO("Not yet implemented")
    }

    override suspend fun getAppVersion(): String {
        TODO("Not yet implemented")
    }

    override suspend fun getEmptyRooms(
        startTime: Int,
        endTime: Int,
        campus: String,
        day: String
    ): MutableList<ClassDetails> {
        TODO("Not yet implemented")
    }

    override suspend fun toggleNotification(id: Long, notification: Boolean) {
        TODO("Not yet implemented")
    }

    override suspend fun deleteNotificationsAndBookedRooms() {
        TODO("Not yet implemented")
    }

    override suspend fun insertNotification(notification: NotificationModel) {
        TODO("Not yet implemented")
    }

    override fun getNotifications(): LiveData<List<NotificationModel>> {
        TODO("Not yet implemented")
    }

    override fun notificationCount(): LiveData<Long> {
        TODO("Not yet implemented")
    }

    override suspend fun updateReadStatus(pk: Long, read: Boolean) {
        TODO("Not yet implemented")
    }

    override suspend fun markAllNotificationAsRead() {
        TODO("Not yet implemented")
    }

    override suspend fun clearAllNotifications() {
        TODO("Not yet implemented")
    }

    override suspend fun getAvailableRoomForBook(
        startTime: Int,
        endTime: Int,
        campus: String,
        reservationTimeStamp: Long
    ): List<Pair<Long, String>> {
        TODO("Not yet implemented")
    }

    override suspend fun bookRoomTeacher(roomBookingInfo: RoomBookingInfo): ResponseMessage {
        TODO("Not yet implemented")
    }

    override suspend fun bookRoomAdmin(bookingInfo: RoomBookingInfo): ResponseMessage {
        TODO("Not yet implemented")
    }

    override suspend fun cancelRoomBookTeacher(bookID: String): ResponseMessage {
        TODO("Not yet implemented")
    }

    override fun fetchTeacherBookedClassesFromCache(): LiveData<List<BookedRoom>> {
        TODO("Not yet implemented")
    }

    override suspend fun refreshCachedBookedRoomsFromServer() {
        TODO("Not yet implemented")
    }

    override suspend fun uploadRoutine(routineData: MutableMap<String, String>): ResponseMessage {
        TODO("Not yet implemented")
    }

    override suspend fun fetchRoutine(
        startTime: Int,
        endTime: Int,
        campus: String,
        day: String
    ): List<ClassDetails> {
        TODO("Not yet implemented")
    }

    override suspend fun addSingleClass(classDetails: ClassDetails): ResponseMessage {
        TODO("Not yet implemented")
    }

    override suspend fun updateSingleClass(classDetails: ClassDetails): ResponseMessage {
        TODO("Not yet implemented")
    }

    override suspend fun deleteSingleClass(id: String): ResponseMessage {
        TODO("Not yet implemented")
    }

    override suspend fun getBookedRoomsSingleDate(dateTimestamp: Long): List<BookedRoom> {
        TODO("Not yet implemented")
    }

    override suspend fun getBookedRoomsRangedDate(
        startDate: Long,
        endDate: Long
    ): List<BookedRoom> {
        TODO("Not yet implemented")
    }

    override suspend fun cancelRoomBookAdmin(id: Long, mailTitle: String, mailBody: String) {
        TODO("Not yet implemented")
    }

    override suspend fun addCRProfile(profile: CRProfile): ResponseMessage {
        TODO("Not yet implemented")
    }

    override suspend fun updateCRProfile(profile: CRProfile): ResponseMessage {
        TODO("Not yet implemented")
    }

    override suspend fun deleteCRProfile(pk: String): ResponseMessage {
        TODO("Not yet implemented")
    }

    override suspend fun deleteCourse(id: Long): ResponseMessage {
        TODO("Not yet implemented")
    }

    override suspend fun addCourse(course: Course): ResponseMessage {
        TODO("Not yet implemented")
    }

    override suspend fun updateCourse(course: Course): ResponseMessage {
        TODO("Not yet implemented")
    }

    override suspend fun addSection(section: String): ResponseMessage {
        TODO("Not yet implemented")
    }

    override suspend fun deleteSection(id: String): ResponseMessage {
        TODO("Not yet implemented")
    }

    override suspend fun updateRoutineVersion(path: String): ResponseBody {
        TODO("Not yet implemented")
    }

    override suspend fun updateAppVersion(path: String): ResponseBody {
        TODO("Not yet implemented")
    }

    override suspend fun updateCourseVersion(path: String): ResponseBody {
        TODO("Not yet implemented")
    }

    override suspend fun updateSectionVersion(path: String): ResponseBody {
        TODO("Not yet implemented")
    }

    override suspend fun resetBookCount(): ResponseMessage {
        TODO("Not yet implemented")
    }
}