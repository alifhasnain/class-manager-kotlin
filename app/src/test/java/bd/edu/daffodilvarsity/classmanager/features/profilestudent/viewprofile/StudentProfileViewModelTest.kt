package bd.edu.daffodilvarsity.classmanager.features.profilestudent.viewprofile

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.SavedStateHandle
import bd.edu.daffodilvarsity.classmanager.common.models.ProfileStudent
import bd.edu.daffodilvarsity.classmanager.common.models.RegisteredCourse
import bd.edu.daffodilvarsity.classmanager.common.repository.FakeRepository
import bd.edu.daffodilvarsity.classmanager.testutil.TestCoroutineRule
import bd.edu.daffodilvarsity.classmanager.testutil.getOrAwaitValue
import bd.edu.daffodilvarsity.classmanager.testutil.getOrReturnNullIfTimeOut
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.Assert.*
import org.junit.Before
import org.junit.Rule
import org.junit.Test

@ExperimentalCoroutinesApi
class StudentProfileViewModelTest {

    @get:Rule
    val testInstantTaskExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    val testCoroutineRule = TestCoroutineRule()

    private lateinit var viewModel: StudentProfileViewModel

    @Before
    fun setUp() {
        viewModel = StudentProfileViewModel(FakeRepository(), SavedStateHandle())
    }

    @Test
    fun `initialize profile and registered course livedata with a value, livedata initialized`() = testCoroutineRule.runBlockingTest {
        // arrange
        val profile = ProfileStudent("alk", "", "", "", "", "", "", "", "")
        val registeredCourses = mutableListOf(
            RegisteredCourse("code 1", "S"),
            RegisteredCourse("code 2", "S"),
            RegisteredCourse("code 3", "S"),
            RegisteredCourse("code 4", "S"),
        )
        // act
        viewModel.initProfileAndCourses(profile, registeredCourses)
        // assert
        assertEquals(profile, viewModel.profileStudent.getOrAwaitValue())
        assertEquals(registeredCourses, viewModel.registeredCourses.getOrAwaitValue())
    }

    @Test
    fun `fetch profile and registered courses from server, data fetched successfully`() = testCoroutineRule.runBlockingTest {
        // Arrange
        // Act
        viewModel.loadProfileAndCourses()
        // Assert
        assertNotNull(viewModel.profileStudent.getOrReturnNullIfTimeOut())
        assertNotNull(viewModel.registeredCourses.getOrReturnNullIfTimeOut())
    }

    @Test
    fun `add new registered course, course added successfully`() = testCoroutineRule.runBlockingTest {
        // Arrange
        val course = RegisteredCourse("LJLLKAJODJQPZXADAA", "ALJDLKKJLAJP")
        // Act
        viewModel.addCourse(course)
        // Assert
        val registeredCourses = viewModel.registeredCourses.getOrAwaitValue()
        assertTrue(registeredCourses.contains(course))
    }

    @Test
    fun `remove a registered course, course removed successfully`() = testCoroutineRule.runBlockingTest {
        // Arrange
        val course = RegisteredCourse("LJLLKAJODJQPZXADAA", "ALJDLKKJLAJP")
        viewModel.addCourse(course)
        // Act
        viewModel.removeCourse(course)
        // Assert
        assertFalse(viewModel.registeredCourses.getOrAwaitValue().contains(course))
    }

}