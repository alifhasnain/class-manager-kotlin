package bd.edu.daffodilvarsity.classmanager.features.login

import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.*
import bd.edu.daffodilvarsity.classmanager.common.auth.Authenticator
import bd.edu.daffodilvarsity.classmanager.common.extensions.networkErrorMessage
import bd.edu.daffodilvarsity.classmanager.common.extensions.setValueThenNullify
import bd.edu.daffodilvarsity.classmanager.common.models.ProfileStudent
import bd.edu.daffodilvarsity.classmanager.common.models.ProfileTeacher
import bd.edu.daffodilvarsity.classmanager.common.models.RegisteredCourse
import bd.edu.daffodilvarsity.classmanager.common.repository.Repository
import kotlinx.coroutines.launch

class SignInViewModel @ViewModelInject constructor (
    private val authenticator: Authenticator,
    private val repo: Repository,
    @Assisted private val savedStateHandle: SavedStateHandle
) : ViewModel() {

    private val _toast: MutableLiveData<String> = MutableLiveData()
    val toast: LiveData<String> = _toast

    private val _showProgressBar: MutableLiveData<Boolean> = MutableLiveData()
    val showProgressBar: LiveData<Boolean> = _showProgressBar

    private val _isProfileComplete: MutableLiveData<Boolean> = MutableLiveData(true)
    val isProfileComplete: LiveData<Boolean> = _isProfileComplete

    val signInAsStudent: MutableLiveData<Pair<ProfileStudent,MutableList<RegisteredCourse>>> = MutableLiveData()

    val signInAsTeacher: MutableLiveData<ProfileTeacher> = MutableLiveData()

    fun signIn(id: String, password: String, logoutFromOthers: Boolean) = safeScope {

        authenticator.signIn(id, password, logoutFromOthers)

        val isProfileCompleted = authenticator.checkProfileCompletion()

        /*
        * If profile is completed then sign in
        * else complete profile
        * */

        if (isProfileCompleted && authenticator.isStudent()) {
            val profile = repo.fetchStudentProfileFromServer()
            val registeredCourse = repo.fetchRegisteredCourses()

            signInAsStudent.value = profile to registeredCourse

        } else if (isProfileCompleted && !authenticator.isStudent()) {
            signInAsTeacher.value = repo.fetchTeacherProfileFromServer()
        } else {
            _toast.setValueThenNullify("Please complete your profile")
            _isProfileComplete.value = false
        }
    }

    private fun safeScope(block: (suspend () -> Unit)) = viewModelScope.launch {
        try {
            _showProgressBar.value = true
            block.invoke()
        } catch (e: Exception) {
            _toast.setValueThenNullify(e.networkErrorMessage)
        } finally {
            _showProgressBar.value = false
        }
    }

}