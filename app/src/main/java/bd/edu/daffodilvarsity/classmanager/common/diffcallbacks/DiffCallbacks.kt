package bd.edu.daffodilvarsity.classmanager.common.diffcallbacks

import androidx.recyclerview.widget.DiffUtil
import bd.edu.daffodilvarsity.classmanager.common.models.BookedRoom
import bd.edu.daffodilvarsity.classmanager.common.models.ClassDetails

object DiffCallbacks {

    val CLASS_DETAILS_ITEM_DIFF_CALLBACK = object : DiffUtil.ItemCallback<ClassDetails>() {

        override fun areItemsTheSame(oldItem: ClassDetails, newItem: ClassDetails): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: ClassDetails, newItem: ClassDetails): Boolean {
            return oldItem == newItem
        }
    }

    val BOOKED_ROOM_ITEM_DIFF_CALLBACK = object : DiffUtil.ItemCallback<BookedRoom>() {

        override fun areItemsTheSame(oldItem: BookedRoom, newItem: BookedRoom): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: BookedRoom, newItem: BookedRoom): Boolean {
            return oldItem == newItem
        }
    }
}