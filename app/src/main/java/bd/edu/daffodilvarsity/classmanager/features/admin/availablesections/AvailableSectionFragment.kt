package bd.edu.daffodilvarsity.classmanager.features.admin.availablesections

import android.content.DialogInterface
import android.os.Bundle
import android.view.View
import android.widget.EditText
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import bd.edu.daffodilvarsity.classmanager.R
import bd.edu.daffodilvarsity.classmanager.common.extensions.makeToast
import bd.edu.daffodilvarsity.classmanager.common.models.DataBindingFragment
import bd.edu.daffodilvarsity.classmanager.databinding.FragmentAvailableSectionBinding
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch


@AndroidEntryPoint
class AvailableSectionFragment : DataBindingFragment<FragmentAvailableSectionBinding>(R.layout.fragment_available_section) {

    private val adapter by lazy { AvailableSectionRecyclerAdapter() }

    private val viewModel by viewModels<AvailableSectionViewModel>()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initializePropertiesAndListeners()
        initializeRecyclerView()
        initializeObservers()
    }

    private fun addSection() {
        AlertDialog.Builder(requireContext()).apply {
            setTitle("Please enter the section name")
            val input = EditText(context).apply { hint = "Section Name" }
            setView(input)
                .setPositiveButton("OK") { _, _ ->
                    viewModel.addSection(input.text.toString().trim())
                }
                .setNegativeButton("Cancel", null)
        }.create().show()
    }

    private fun deleteSection(id: String, section: String) {
        AlertDialog.Builder(requireContext()).apply {
            setTitle("Please verify")
            setMessage("Are you sure you want to delete Section $section?")
                .setPositiveButton("Yes") { _, _ ->
                    viewModel.deleteSection(id)
                }
                .setNegativeButton("No", null)
        }.create().show()
    }

    private fun initializeObservers() {
        viewModel.sections.observe(viewLifecycleOwner) { adapter.updateItems(it) }
        viewModel.toast.observe(viewLifecycleOwner) { makeToast(it) }
    }

    private fun initializeRecyclerView() {
        binding.recyclerView.adapter = adapter
        adapter.longClickListener = { (id , section) ->
            AlertDialog.Builder(requireContext()).apply {
                setItems(arrayOf("Delete"), DialogInterface.OnClickListener { _, position ->
                    if (position == 0) {
                        deleteSection(id,section)
                    }
                })
            }.create().show()
        }
    }

    private fun initializePropertiesAndListeners() {
        binding.viewModel = viewModel
        binding.addSection.setOnClickListener { addSection() }
        binding.swipeRefresh.setOnRefreshListener {
            lifecycleScope.launch {
                viewModel.fetchAvailableSections()
                delay(1000)
                binding.swipeRefresh.isRefreshing = false
            }
        }
    }

}