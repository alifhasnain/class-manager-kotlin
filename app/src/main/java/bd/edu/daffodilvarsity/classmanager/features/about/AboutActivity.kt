package bd.edu.daffodilvarsity.classmanager.features.about

import android.os.Build
import android.os.Bundle
import android.text.Html
import android.util.TypedValue
import android.view.WindowManager
import android.widget.FrameLayout
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.text.HtmlCompat
import bd.edu.daffodilvarsity.classmanager.BuildConfig
import bd.edu.daffodilvarsity.classmanager.R
import bd.edu.daffodilvarsity.classmanager.databinding.ActivityAboutBinding
import bd.edu.daffodilvarsity.classmanager.utils.sharedprefutils.ThemeSharedPrefHelper
import bd.edu.daffodilvarsity.classmanager.utils.sharedprefutils.ThemeSharedPrefHelper.THEME_CYAN
import bd.edu.daffodilvarsity.classmanager.utils.sharedprefutils.ThemeSharedPrefHelper.THEME_DEFAULT
import bd.edu.daffodilvarsity.classmanager.utils.sharedprefutils.ThemeSharedPrefHelper.THEME_NIGHT
import bd.edu.daffodilvarsity.classmanager.utils.sharedprefutils.ThemeSharedPrefHelper.THEME_TEAL
import com.wada811.viewbinding.viewBinding


class AboutActivity : AppCompatActivity() {

    private val binding by viewBinding { ActivityAboutBinding.bind(it) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setupTheme()
        setContentView(R.layout.activity_about)
        setSupportActionBar(binding.toolbar)
        window.apply {
            val value = TypedValue()
            this.context.theme.resolveAttribute(R.attr.colorPrimaryDark, value, true)
            addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            statusBarColor = value.data
        }

        binding.changelog.setOnClickListener { displayChangeLog() }
        initializeViews()
    }

    @SuppressWarnings("deprecation")
    private fun displayChangeLog() = AlertDialog.Builder(this).apply {
        val params = LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.WRAP_CONTENT,
            LinearLayout.LayoutParams.WRAP_CONTENT
        )
        params.setMargins(30, 50, 30, 0)
        val frameLayout = FrameLayout(context)

        val textView = TextView(context).apply {
            layoutParams = params
            setTextSize(TypedValue.COMPLEX_UNIT_SP, 16f)
        }
        frameLayout.addView(textView)
        /*
        * read changelog file
        * */
        val inputStream = resources.openRawResource(R.raw.changelog)
        val text = inputStream.bufferedReader().use { it.readText() }
        textView.text = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            Html.fromHtml(text, HtmlCompat.FROM_HTML_MODE_LEGACY)
        } else {
            Html.fromHtml(text)
        }
        setView(frameLayout)
        setNegativeButton("Dismiss", null)
    }.create().show()

    private fun initializeViews() {
        binding.version.text = BuildConfig.VERSION_NAME
    }

    private fun setupTheme() {
        when(ThemeSharedPrefHelper.getCurrentTheme(this)) {
            THEME_DEFAULT -> setTheme(R.style.AppTheme_Default)
            THEME_NIGHT -> setTheme(R.style.AppTheme_Night)
            THEME_CYAN -> setTheme(R.style.AppTheme_Cyan)
            THEME_TEAL -> setTheme(R.style.AppTheme_Teal)
        }
    }
}
