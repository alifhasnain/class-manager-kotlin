package bd.edu.daffodilvarsity.classmanager.common.di

import bd.edu.daffodilvarsity.classmanager.common.auth.Authenticator
import bd.edu.daffodilvarsity.classmanager.common.database.ClassManagerDao
import bd.edu.daffodilvarsity.classmanager.common.network.AdminApi
import bd.edu.daffodilvarsity.classmanager.common.network.UserApi
import bd.edu.daffodilvarsity.classmanager.common.repository.Repository
import bd.edu.daffodilvarsity.classmanager.common.repository.RepositoryImpl
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import javax.inject.Singleton

@InstallIn(ApplicationComponent::class)
@Module
object RepositoryModule {

    @Provides
    @Singleton
    fun provideRepository(
        authenticator: Authenticator,
        userClient: UserApi,
        dao: ClassManagerDao,
        adminClient: AdminApi
    ): Repository = RepositoryImpl(authenticator, userClient, dao, adminClient)

}