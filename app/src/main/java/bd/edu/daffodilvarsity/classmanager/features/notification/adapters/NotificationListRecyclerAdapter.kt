package bd.edu.daffodilvarsity.classmanager.features.notification.adapters

import bd.edu.daffodilvarsity.classmanager.R
import bd.edu.daffodilvarsity.classmanager.common.adapter.SingleItemBaseAdapter
import bd.edu.daffodilvarsity.classmanager.common.models.NotificationModel
import timber.log.Timber

class NotificationListRecyclerAdapter: SingleItemBaseAdapter<NotificationModel>() {

    private val items = mutableListOf<NotificationModel>()

    var onCheckChanged: ((Long,Boolean) -> Unit)? = null

    fun submitItems(updatedItems: List<NotificationModel>) {
        items.clear()
        items.addAll(updatedItems)
        notifyDataSetChanged()
    }

    fun onCheckChanged(primaryKey: Long, checked: Boolean) {
        onCheckChanged?.invoke(primaryKey,checked)
        Timber.e("$primaryKey,$checked")
    }

    override fun clickedItem(item: NotificationModel) { }

    override fun getItemForPosition(position: Int): NotificationModel = items[position]

    override fun getLayoutIdForPosition(position: Int): Int = R.layout.list_item_notifications

    override fun getItemCount(): Int = items.size

}