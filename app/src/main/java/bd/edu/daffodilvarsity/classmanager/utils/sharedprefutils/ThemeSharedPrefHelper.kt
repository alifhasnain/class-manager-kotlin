package bd.edu.daffodilvarsity.classmanager.utils.sharedprefutils

import android.content.Context
import android.content.Context.MODE_PRIVATE
import android.content.SharedPreferences

object ThemeSharedPrefHelper {

    private const val THEME_SHARED_PREF = "theme-shared-pref"

    private const val CURRENT_THEME = "current-theme"

    const val THEME_DEFAULT = "day-theme"

    const val THEME_NIGHT = "night-theme"

    const val THEME_CYAN = "theme-cyan"

    const val THEME_TEAL = "theme-teal"

    fun getCurrentTheme(context: Context): String {
        return getSharedPref(
            context
        ).getString(
            CURRENT_THEME,
            THEME_DEFAULT
        )!!
    }

    fun setNewTheme(context: Context, theme: String) {
        getSharedPref(
            context
        )
            .edit().putString(CURRENT_THEME,theme).apply()
    }

    private fun getSharedPref(context: Context): SharedPreferences {
        return context.getSharedPreferences(THEME_SHARED_PREF,MODE_PRIVATE)
    }
}