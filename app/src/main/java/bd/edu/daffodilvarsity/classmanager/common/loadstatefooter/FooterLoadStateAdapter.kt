package bd.edu.daffodilvarsity.classmanager.common.loadstatefooter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.paging.LoadState
import androidx.paging.LoadStateAdapter
import androidx.recyclerview.widget.RecyclerView
import bd.edu.daffodilvarsity.classmanager.R
import bd.edu.daffodilvarsity.classmanager.databinding.FooterRefreshBinding

class FooterLoadStateAdapter(private val retry: () -> Unit): LoadStateAdapter<FooterLoadStateViewHolder>() {

    override fun onBindViewHolder(holder: FooterLoadStateViewHolder, loadState: LoadState) {
        holder.bind(loadState)
    }

    override fun onCreateViewHolder(parent: ViewGroup, loadState: LoadState): FooterLoadStateViewHolder {
        return FooterLoadStateViewHolder.create(parent, retry)
    }

}

class FooterLoadStateViewHolder(
    val binding: FooterRefreshBinding,
    private val retry: () -> Unit
): RecyclerView.ViewHolder(binding.root) {

    init {
        binding.reload.setOnClickListener { retry.invoke() }
    }

    fun bind(loadState: LoadState) {
        binding.progressBar.visibility = if(loadState is LoadState.Loading) { View.VISIBLE } else View.GONE
        binding.reload.visibility = if(loadState !is LoadState.Loading) { View.VISIBLE } else View.GONE
    }

    companion object {
        fun create(parent: ViewGroup, retry: () -> Unit): FooterLoadStateViewHolder {
            val view = LayoutInflater.from(parent.context).inflate(R.layout.footer_refresh,parent,false)
            val binding = FooterRefreshBinding.bind(view)
            return FooterLoadStateViewHolder(binding, retry)
        }
    }
}