package bd.edu.daffodilvarsity.classmanager.features.routine.ui

import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import bd.edu.daffodilvarsity.classmanager.R
import bd.edu.daffodilvarsity.classmanager.common.extensions.enforceSingleScrollDirection
import bd.edu.daffodilvarsity.classmanager.common.extensions.recyclerView
import bd.edu.daffodilvarsity.classmanager.common.models.DataBindingFragment
import bd.edu.daffodilvarsity.classmanager.common.viewpager2transformers.ViewPager2PageTransformation
import bd.edu.daffodilvarsity.classmanager.databinding.FragmentRoutineTabHolderBinding
import bd.edu.daffodilvarsity.classmanager.features.routine.adapters.RoutinePagerAdapter
import bd.edu.daffodilvarsity.classmanager.features.routine.viewmodels.RoutineSharedViewModel
import bd.edu.daffodilvarsity.classmanager.utils.HelperClass
import bd.edu.daffodilvarsity.classmanager.utils.sharedprefutils.ThemeSharedPrefHelper
import bd.edu.daffodilvarsity.classmanager.utils.sharedprefutils.ThemeSharedPrefHelper.THEME_CYAN
import bd.edu.daffodilvarsity.classmanager.utils.sharedprefutils.ThemeSharedPrefHelper.THEME_DEFAULT
import bd.edu.daffodilvarsity.classmanager.utils.sharedprefutils.ThemeSharedPrefHelper.THEME_NIGHT
import bd.edu.daffodilvarsity.classmanager.utils.sharedprefutils.ThemeSharedPrefHelper.THEME_TEAL
import com.google.android.material.tabs.TabLayoutMediator
import com.google.android.material.transition.MaterialSharedAxis
import dagger.hilt.android.AndroidEntryPoint

/**
 * A simple [Fragment] subclass.
 */
@AndroidEntryPoint
class RoutineTabHolderFragment : DataBindingFragment<FragmentRoutineTabHolderBinding>(R.layout.fragment_routine_tab_holder) {

    private val viewModel: RoutineSharedViewModel by activityViewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        enterTransition = MaterialSharedAxis(MaterialSharedAxis.Z, true)
        exitTransition = MaterialSharedAxis(MaterialSharedAxis.Z, true)

        binding.viewmodel = viewModel
        setHasOptionsMenu(true)

        initializeViewPager()
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.fragment_routine_tab_holder,menu)
    }

    override fun onPrepareOptionsMenu(menu: Menu) {
        when(ThemeSharedPrefHelper.getCurrentTheme(requireContext())) {
            THEME_DEFAULT -> menu.findItem(R.id.default_theme).isChecked = true
            THEME_NIGHT -> menu.findItem(R.id.dark_theme).isChecked = true
            THEME_CYAN -> menu.findItem(R.id.cyan_theme).isChecked = true
            THEME_TEAL -> menu.findItem(R.id.teal_theme).isChecked = true
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId) {
            R.id.refresh_routine -> {
                val dialog = AlertDialog.Builder(requireContext())
                    .setTitle("Please confirm")
                    .setMessage("Do you want to update whole routine from server? This will delete your existing active notifications.")
                    .setPositiveButton("proceed") { _, _ ->
                        viewModel.refreshAllClassesAndCoursesFromServer()
                    }
                    .setNegativeButton("cancel",null)
                    .create()
                dialog.show()
            }
            R.id.default_theme -> {
                ThemeSharedPrefHelper.setNewTheme(requireContext(),THEME_DEFAULT)
                activity?.setTheme(R.style.AppTheme_Default)
                activity?.recreate()
            }
            R.id.dark_theme -> {
                ThemeSharedPrefHelper.setNewTheme(requireContext(),THEME_NIGHT)
                activity?.setTheme(R.style.AppTheme_Night)
                activity?.recreate()
            }
            R.id.cyan_theme -> {
                ThemeSharedPrefHelper.setNewTheme(requireContext(),THEME_CYAN)
                activity?.setTheme(R.style.AppTheme_Cyan)
                activity?.recreate()
            }
            R.id.teal_theme -> {
                ThemeSharedPrefHelper.setNewTheme(requireContext(), THEME_TEAL)
                activity?.setTheme(R.style.AppTheme_Teal)
                activity?.recreate()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun initializeViewPager() {

        binding.viewPager.adapter = RoutinePagerAdapter(this)

        TabLayoutMediator(binding.tabLayout,binding.viewPager) { tab, position ->
            tab.text = HelperClass.getDayOfWeek(position)
        }.attach()

        binding.viewPager.apply {
            setCurrentItem(HelperClass.getDayOfWeekIndex(),false)
            offscreenPageLimit = 4
            setPageTransformer(ViewPager2PageTransformation())
        }
        binding.viewPager.recyclerView.enforceSingleScrollDirection()
    }

}
