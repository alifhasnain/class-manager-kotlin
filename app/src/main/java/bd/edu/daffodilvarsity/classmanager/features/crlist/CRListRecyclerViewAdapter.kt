package bd.edu.daffodilvarsity.classmanager.features.crlist

import bd.edu.daffodilvarsity.classmanager.R
import bd.edu.daffodilvarsity.classmanager.common.adapter.SingleItemBaseAdapter
import bd.edu.daffodilvarsity.classmanager.common.models.CRProfile

class CRListRecyclerViewAdapter: SingleItemBaseAdapter<CRProfile>() {

    private val items = mutableListOf<CRProfile>()

    var sendMailClickListener: ((String) -> Unit)? = null

    var dialClickListener: ((String) -> Unit)? = null

    fun submitItems(updatedItems: List<CRProfile>) {
        items.clear()
        items.addAll(updatedItems)
        notifyDataSetChanged()
    }

    fun onSendEmailClick(email: String) = sendMailClickListener?.invoke(email)

    fun onDialClicked(phoneNo: String) = dialClickListener?.invoke(phoneNo)

    override fun clickedItem(item: CRProfile) { }

    override fun getItemForPosition(position: Int): CRProfile = items[position]

    override fun getLayoutIdForPosition(position: Int): Int = R.layout.list_item_cr

    override fun getItemCount(): Int = items.size

}