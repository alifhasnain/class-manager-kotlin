package bd.edu.daffodilvarsity.classmanager.common.database

import androidx.lifecycle.LiveData
import androidx.room.*
import androidx.sqlite.db.SupportSQLiteQuery
import bd.edu.daffodilvarsity.classmanager.common.models.BookedRoom
import bd.edu.daffodilvarsity.classmanager.common.models.ClassDetails
import bd.edu.daffodilvarsity.classmanager.common.models.Course
import bd.edu.daffodilvarsity.classmanager.common.models.NotificationModel
import kotlinx.coroutines.flow.Flow

@Dao
interface ClassManagerDao {

    /*
    * All queries regarding courses
    */
    @Insert
    suspend fun insertCourses(courses : List<Course>)

    @Query("DELETE FROM Course")
    suspend fun deleteAllCourses()

    @Transaction
    suspend fun refreshAllCourses(courses: List<Course>) {
        deleteAllCourses()
        insertCourses(courses)
    }

    @Query("SELECT * FROM Course WHERE level=:level AND term=:term AND shift=:shift AND department=:department")
    suspend fun getCoursesWithLevelTerm(level: Int, term: Int,shift: String, department: String): MutableList<Course>

    @Query("SELECT * FROM Course WHERE shift=:shift AND department=:department ORDER BY level,term")
    fun getCoursesWithShiftAndDepartment(shift: String, department: String): LiveData<List<Course>>

    @Query("SELECT * FROM Course WHERE shift=:shift AND department=:department ORDER BY level ASC,term ASC")
    suspend fun getAllCourses(shift: String, department: String): MutableList<Course>

    /*
    * All queries regarding classes
    */

    @Insert
    suspend fun insertClasses(classes: List<ClassDetails>)

    @Query("SELECT * FROM ClassDetails")
    suspend fun getAllClasses(): List<ClassDetails>

    @Query("DELETE FROM ClassDetails")
    suspend fun deleteAllClasses()

    @Transaction
    suspend fun refreshAllClasses(classes: List<ClassDetails>) {
        deleteAllClasses()
        insertClasses(classes)
    }

    @Query("SELECT DISTINCT section FROM ClassDetails WHERE section<>\"\" ORDER BY section")
    fun getDistinctSections(): LiveData<List<String>>

    @Query("SELECT * FROM ClassDetails WHERE teacherInitial=:initial AND dayOfWeek=:dayOfWeek ORDER BY startTime")
    fun getTeacherRoutine(initial: String,dayOfWeek: String): Flow<MutableList<ClassDetails>>

    @Query("SELECT * FROM ClassDetails WHERE teacherInitial=:initial AND courseCode <> \"\" ORDER BY startTime")
    suspend fun getTeacherWholeRoutineAsList(initial: String): MutableList<ClassDetails>

    @Query("SELECT * FROM ClassDetails WHERE teacherInitial=:initial AND courseCode <> \"\" ORDER BY startTime")
    fun getTeacherWholeRoutineAsFlow(initial: String): Flow<List<ClassDetails>>

    @Query("SELECT * FROM ClassDetails WHERE teacherInitial=:initial AND dayOfWeek=:dayOfWeek ORDER BY startTime")
    suspend fun getTeacherRoutineForSpecificDayAsList(initial: String, dayOfWeek: String): MutableList<ClassDetails>

    @RawQuery(observedEntities = [ClassDetails::class])
    fun getStudentRoutine(rawQuery: SupportSQLiteQuery): Flow<MutableList<ClassDetails>>

    @RawQuery
    suspend fun getStudentRoutineAsList(rawQuery: SupportSQLiteQuery): List<ClassDetails>

    /*
    * This function is necessary where students
    * doesn't have different section for different courses
    * this is only used in custom search where only one section
    * is defined while querying for a specific level term.
    * It will cause bug in normal use cases
    * */

    @Query("SELECT * FROM ClassDetails WHERE courseCode IN (:courseList) AND section=:section AND shift=:shift AND campus=:campus AND department=:department ORDER BY startTime")
    suspend fun getStudentRoutine(courseList: MutableList<String>,section: String, shift: String,campus: String,department: String): MutableList<ClassDetails>

    @Query("SELECT * FROM ClassDetails WHERE courseCode=:courseCode AND shift=:shift AND campus=:campus AND department=:department ORDER BY startTime,section ASC")
    suspend fun getRoutineWithCourseCode(courseCode: String,shift: String,campus: String, department: String): MutableList<ClassDetails>

    @Query("SELECT * FROM ClassDetails WHERE startTime=:startTime AND endTime=:endTime AND campus=:campus AND dayOfWeek=:day AND courseCode = \"\"")
    suspend fun getEmptyRooms(day: String, startTime: Int, endTime: Int, campus: String): MutableList<ClassDetails>

    @Query("UPDATE ClassDetails SET notification=:notification WHERE id=:id")
    suspend fun toggleNotification(id: Long, notification: Boolean)

    @Query("DELETE FROM BookedRoom")
    suspend fun deleteAllBookedClasses()

    @Insert
    suspend fun insertBookedClasses(classes: List<BookedRoom>)

    @Transaction
    suspend fun refreshAllBookedClasses(classes: List<BookedRoom>) {
        deleteAllBookedClasses()
        insertBookedClasses(classes)
    }

    @Query("SELECT * FROM BookedRoom")
    fun getAllBookedClasses() : LiveData<List<BookedRoom>>

    //Queries related to notifications
    @Insert
    fun insertNotification(notification: NotificationModel)

    @Query("SELECT * FROM Notifications ORDER BY issuedAt DESC LIMIT 30")
    fun getNotifications(): LiveData<List<NotificationModel>>

    @Query("SELECT COUNT(read) FROM Notifications WHERE read = 0")
    fun getUnreadNotificationCount(): LiveData<Long>

    @Query("UPDATE NOTIFICATIONS SET read = :read WHERE id = :pk")
    suspend fun updateReadStatus(pk: Long,read: Boolean)

    @Query("UPDATE Notifications SET read = 1")
    suspend fun markAllAsRead()

    @Query("DELETE FROM Notifications")
    suspend fun deleteAllFCMNotifications()

}