package bd.edu.daffodilvarsity.classmanager.features.admin.availablecourses

import android.content.DialogInterface
import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.widget.ArrayAdapter
import androidx.appcompat.app.AlertDialog
import androidx.core.os.bundleOf
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import bd.edu.daffodilvarsity.classmanager.R
import bd.edu.daffodilvarsity.classmanager.common.extensions.fromPOJOToJSON
import bd.edu.daffodilvarsity.classmanager.common.extensions.makeToast
import bd.edu.daffodilvarsity.classmanager.common.models.Course
import bd.edu.daffodilvarsity.classmanager.common.models.DataBindingFragment
import bd.edu.daffodilvarsity.classmanager.databinding.FragmentAvailableCoursesBinding
import bd.edu.daffodilvarsity.classmanager.utils.HelperClass
import com.google.android.material.transition.MaterialSharedAxis
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class AvailableCoursesFragment : DataBindingFragment<FragmentAvailableCoursesBinding>(R.layout.fragment_available_courses) {

    private val navController by lazy { findNavController() }

    private val viewModel by viewModels<AvailableCoursesViewModel>()

    private val adapter by lazy { AvailableCoursesRecyclerAdapter() }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setHasOptionsMenu(true)
        enterTransition = MaterialSharedAxis(MaterialSharedAxis.Z, true)
        exitTransition = MaterialSharedAxis(MaterialSharedAxis.Z, true)
        //init binding variables
        binding.presenter = this
        binding.viewModel = viewModel

        initializeSpinners()
        initializeObservers()
        initializeRecyclerView()

    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        menu.add("Add Course")
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.title == "Add Course") {
            findNavController().navigate(
                R.id.action_availableCourses_to_addOrEditRoomDialog,
                bundleOf("operation" to "add")
            )
        }
        return super.onOptionsItemSelected(item)
    }

    fun fetchCourses() = viewModel.fetchCourses (
        binding.levelSpinner.spinner.selectedItem.toString(),
        binding.termSpinner.spinner.selectedItem.toString(),
        binding.shiftSpinner.spinner.selectedItem.toString(),
    )

    private fun initializeRecyclerView() {
        binding.recyclerView.adapter = adapter
        adapter.onLongClickListener = {
            AlertDialog.Builder(requireContext()).apply {
                setItems(arrayOf("Edit", "Delete"), DialogInterface.OnClickListener { _, position ->
                    if (position == 0) {
                        editCourse(it)
                    } else {
                        deleteCourse(it)
                    }
                })
            }.create().show()
        }
    }

    private fun editCourse(course: Course) {
        val courseJSON = course.fromPOJOToJSON()
        findNavController().navigate(
            R.id.action_availableCourses_to_addOrEditRoomDialog,
            bundleOf("course" to courseJSON, "operation" to "edit")
        )
    }

    private fun deleteCourse(course: Course) {
        AlertDialog.Builder(requireContext()).apply {
            setTitle("Please confirm")
            setMessage("Are you sure you want to delete ${course.courseCode} from L${course.level}T${course.term}")
            setPositiveButton("Yes") { _, _ ->
                viewModel.deleteCourse(course)
            }
            setNegativeButton("No", null)
        }.create().show()
    }

    private fun initializeObservers() {
        viewModel.toast.observe(viewLifecycleOwner) { makeToast(it.content) }
        viewModel.courses.observe(viewLifecycleOwner) { courses -> adapter.updateItems(courses) }

        navController.getBackStackEntry(R.id.availableCourses).savedStateHandle
            .getLiveData<Pair<String, Course>>("data")
            .observe(viewLifecycleOwner) { (operation, course) ->
                if (operation == "add") {
                    viewModel.addCourse(course)
                } else if (operation == "edit") {
                    viewModel.updateCourse(course)
                }
                navController.getBackStackEntry(R.id.availableCourses)
                    .savedStateHandle.remove<Pair<String, Course>>("data")
            }
    }

    private fun initializeSpinners() {

        binding.levelSpinner.title.text = getString(R.string.level)
        binding.termSpinner.title.text = getString(R.string.term)
        binding.shiftSpinner.title.text = getString(R.string.shift)

        binding.levelSpinner.spinner.adapter = ArrayAdapter(
            requireContext(),
            R.layout.spinner_item,
            HelperClass.getLevels()
        ).apply { setDropDownViewResource(R.layout.spinner_dropdown_item) }

        binding.termSpinner.spinner.adapter = ArrayAdapter(
            requireContext(),
            R.layout.spinner_item,
            HelperClass.getTerms()
        ).apply { setDropDownViewResource(R.layout.spinner_dropdown_item) }

        binding.shiftSpinner.spinner.adapter = ArrayAdapter(
            requireContext(),
            R.layout.spinner_item,
            HelperClass.getShifts()
        ).apply { setDropDownViewResource(R.layout.spinner_dropdown_item) }

    }

}