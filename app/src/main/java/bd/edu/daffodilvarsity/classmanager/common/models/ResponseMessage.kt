package bd.edu.daffodilvarsity.classmanager.common.models

import com.squareup.moshi.JsonClass


@JsonClass(generateAdapter = true)
data class ResponseMessage(
    val success: Boolean,
    val message: String
)
