package bd.edu.daffodilvarsity.classmanager.features.routine.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import bd.edu.daffodilvarsity.classmanager.common.diffcallbacks.DiffCallbacks.CLASS_DETAILS_ITEM_DIFF_CALLBACK
import bd.edu.daffodilvarsity.classmanager.common.models.ClassDetails
import bd.edu.daffodilvarsity.classmanager.databinding.ListItemClassesBinding

class ClassesListListAdapter: ListAdapter<ClassDetails, RecyclerView.ViewHolder>(CLASS_DETAILS_ITEM_DIFF_CALLBACK) {

    var onNotificationClicked: ((ClassDetails) -> Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val binding = ListItemClassesBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
        val viewHolder = ViewHolder(binding)
        binding.notification.setOnClickListener {
            onNotificationClicked?.invoke(getItem(viewHolder.absoluteAdapterPosition))
        }
        return viewHolder
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as ViewHolder).bind(getItem(position))
    }

    class ViewHolder(val binding: ListItemClassesBinding): RecyclerView.ViewHolder(binding.root) {
        fun bind(data: ClassDetails) {
            binding.data = data
            binding.executePendingBindings()
        }
    }
}