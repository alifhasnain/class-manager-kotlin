package bd.edu.daffodilvarsity.classmanager.features.roombooking.bookrooms.viewmodel

import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.*
import bd.edu.daffodilvarsity.classmanager.common.extensions.networkErrorMessage
import bd.edu.daffodilvarsity.classmanager.common.extensions.setValueThenNullify
import bd.edu.daffodilvarsity.classmanager.common.models.RoomBookingInfo
import bd.edu.daffodilvarsity.classmanager.common.repository.Repository
import kotlinx.coroutines.launch

class BookRoomViewModel @ViewModelInject constructor(
    private val repo: Repository,
    @Assisted savedStateHandle: SavedStateHandle
): ViewModel() {

    private val _availableRooms: MutableLiveData<List<Pair<Long,String>>> = MutableLiveData()
    val availableRooms: LiveData<List<Pair<Long,String>>> = _availableRooms

    private val _teacherDistinctCourses: MutableLiveData<MutableList<String>> = MutableLiveData()
    val teacherDistinctCourses: LiveData<MutableList<String>> = _teacherDistinctCourses

    private val _teacherDistinctSections: MutableLiveData<MutableList<String>> = MutableLiveData()
    val teacherDistinctSections: LiveData<MutableList<String>> = _teacherDistinctSections

    val showProgressBar: MutableLiveData<Boolean> = MutableLiveData(false)
    val showEmptyRoom: MutableLiveData<Boolean> = MutableLiveData(true)
    val bookSuccess: MutableLiveData<Boolean> = MutableLiveData()

    val toast: MutableLiveData<String> = MutableLiveData()

    fun loadAvailableRoomForBook(startTime: Int, endTime: Int, campus: String, reservationTimeStamp: Long) {
        viewModelScope.launch {
            try {
                showProgressBar.value = true
                _availableRooms.value = mutableListOf()
                showEmptyRoom.value = false

                _availableRooms.value = repo.getAvailableRoomForBook(startTime, endTime, campus, reservationTimeStamp)
                showEmptyRoom.value = availableRooms.value.isNullOrEmpty()

            } catch (e: Exception) {
                toast.setValueThenNullify(e.networkErrorMessage)
            } finally {
                showProgressBar.value = false
            }
        }
    }

    fun bookRoom(roomBookingInfo: RoomBookingInfo) {
        viewModelScope.launch {
            try {
                _availableRooms.value = mutableListOf()
                showEmptyRoom.value = false
                showProgressBar.value = true

                toast.setValueThenNullify("Precessing request please wait")

                val response = repo.bookRoomTeacher(roomBookingInfo)

                if (response.success) {
                    toast.setValueThenNullify(response.message)
                    bookSuccess.setValueThenNullify(true)
                }
            } catch (e: Exception) {
                toast.setValueThenNullify(e.networkErrorMessage)
                bookSuccess.setValueThenNullify(false)
            }
        }
    }

    fun loadDistinctCourseCodesAndNamesByTeacher(initial: String) {
        viewModelScope.launch {
            val allClasses = repo.getTeacherRoutine(initial)
            val distinctCourses = allClasses.distinctBy { "${it.courseCode} : ${it.courseName}" }

            _teacherDistinctCourses.value = distinctCourses.map { "${it.courseCode} : ${it.courseName}" } as MutableList
            _teacherDistinctSections.value = allClasses.distinctBy { it.section }.map { it.section } as MutableList
        }
    }

}