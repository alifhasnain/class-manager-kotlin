package bd.edu.daffodilvarsity.classmanager.features.login

import android.os.Bundle
import android.view.View
import androidx.activity.viewModels
import androidx.lifecycle.lifecycleScope
import bd.edu.daffodilvarsity.classmanager.R
import bd.edu.daffodilvarsity.classmanager.common.auth.Authenticator
import bd.edu.daffodilvarsity.classmanager.common.extensions.clearStackAndStartActivity
import bd.edu.daffodilvarsity.classmanager.common.extensions.hideKeyboard
import bd.edu.daffodilvarsity.classmanager.common.extensions.makeToast
import bd.edu.daffodilvarsity.classmanager.common.extensions.startActivity
import bd.edu.daffodilvarsity.classmanager.common.models.DataBindingAppCompatActivity
import bd.edu.daffodilvarsity.classmanager.databinding.ActivitySignInBinding
import bd.edu.daffodilvarsity.classmanager.features.completeprofile.CompleteProfileStudentActivity
import bd.edu.daffodilvarsity.classmanager.features.completeprofile.CompleteProfileTeacherActivity
import bd.edu.daffodilvarsity.classmanager.features.home.MainActivity
import bd.edu.daffodilvarsity.classmanager.utils.TokenExpirationException
import bd.edu.daffodilvarsity.classmanager.utils.sharedprefutils.AuthSharedPrefHelper
import bd.edu.daffodilvarsity.classmanager.utils.sharedprefutils.ProfileSharedPrefHelper
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch
import timber.log.Timber
import javax.inject.Inject


@AndroidEntryPoint
class SignInActivity : DataBindingAppCompatActivity<ActivitySignInBinding>(R.layout.activity_sign_in) {

    private val viewModel: SignInViewModel by viewModels()

    @Inject lateinit var authenticator: Authenticator

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding.presenter = this

        initializeViewData()

        initializeViewModelObservers()

    }

    override fun onStart() {
        if (authenticator.isSignedIn()) {
            lifecycleScope.launch {
                try {
                    showProgressBar(true)
                    authenticator.renewAccessToken()
                } catch (exp : TokenExpirationException) {
                    Timber.e(exp)
                    makeToast("Session expired.Please sign in again.")
                    AuthSharedPrefHelper.clearAuthSharedPref(this@SignInActivity)
                    ProfileSharedPrefHelper.clearProfileSharedPref(this@SignInActivity)
                } catch (e: Exception) {
                    Timber.e(e)
                } finally {
                    showProgressBar(false)
                }
                if(authenticator.isSignedIn()) {
                    clearStackAndStartActivity<MainActivity>()
                }
            }
        }
        super.onStart()
    }

    override fun onBackPressed() {
        if (binding.progressBarHolder.visibility == View.VISIBLE) {
            binding.progressBarHolder.visibility = View.GONE
        } else {
            super.onBackPressed()
        }
    }

    fun signIn(id: String, password: String, logoutFromOthers: Boolean) {
        hideKeyboard()
        if (!validateID(id) && !validateID(password)) {
            makeToast("please check your id & password")
            return
        }
        //AuthSharedPrefHelper.saveLoginIdAndPassword(this, id, password)
        viewModel.signIn(id, password, logoutFromOthers)

    }

    private fun initializeViewData() {
        binding.email.editText?.setText(AuthSharedPrefHelper.getLoginID(this))
        binding.password.editText?.setText(AuthSharedPrefHelper.getPassword(this))
    }

    private fun initializeViewModelObservers() {

        //For showing toast message
        viewModel.toast.observe(this, {
            makeToast(it)
        })

        //Enable or Disable sign in button
        viewModel.showProgressBar.observe(this, {
            showProgressBar(it)
        })

        //Complete profile
        viewModel.isProfileComplete.observe(this, {
            if (it == false && isStudent()) {
                startActivity<CompleteProfileStudentActivity>()
            } else if (it == false && !isStudent()) {
                startActivity<CompleteProfileTeacherActivity>()
            }
        })

        //SignIn Observers
        viewModel.signInAsStudent.observe(this , {
            val (profile , registeredCourses) = it
            ProfileSharedPrefHelper.saveStudentProfile(this,profile)
            ProfileSharedPrefHelper.saveRegisteredCourses(this,registeredCourses)
            AuthSharedPrefHelper.saveSignedIn(this)
            clearStackAndStartActivity<MainActivity>()
        })

        viewModel.signInAsTeacher.observe(this, {
            ProfileSharedPrefHelper.saveTeacherProfile(this,it)
            AuthSharedPrefHelper.saveSignedIn(this)
            clearStackAndStartActivity<MainActivity>()
        })

    }

    private fun isStudent(): Boolean {
        return AuthSharedPrefHelper.getUserType(this) == AuthSharedPrefHelper.USER_TYPE_STUDENT
    }

    private fun showProgressBar(show: Boolean) {
        if (show) {
            binding.progressBarHolder.visibility = View.VISIBLE
        } else {
            binding.progressBarHolder.visibility = View.GONE
        }
    }

    fun validateID(id: String): Boolean {
        if (id.isEmpty()) {
            binding.email.error = "This field can't be empty"
        } else {
            binding.email.error = null
            return true
        }
        return false
    }

    fun validatePassword(password: String): Boolean {
        if (password.isEmpty()) {
            binding.password.error = "Password can't be empty"
        } else {
            binding.password.error = null
            return true
        }
        return false
    }

}
