package bd.edu.daffodilvarsity.classmanager.utils.sharedprefutils

import android.content.Context
import android.content.Context.MODE_PRIVATE
import android.content.SharedPreferences

object VersionSharedPrefHelper {

    private const val VERSION_SHARED_PREF_TAG = "version-shared-pref-tag"

    private const val ROUTINE_VERSION = "routine-version"
    private const val APP_VERSION = "app-version"
    private const val COURSE_VERSION = "device-course-version"
    private const val SECTION_VERSION = "device-section-version"

    fun getCourseVersion(context: Context): String {
        return getSharedPref(context).getString(COURSE_VERSION, "0")!!
    }

    fun saveCourseVersion(context: Context, version: String) {
        getSharedPref(context).edit().putString(COURSE_VERSION, version).apply()
    }

    fun getSectionVersion(context: Context): String {
        return getSharedPref(context).getString(SECTION_VERSION, "0")!!
    }

    fun saveSectionVersion(context: Context, version: String) {
        getSharedPref(context).edit().putString(SECTION_VERSION, version).apply()
    }

    fun getRoutineVersion(context: Context): String {
        return getSharedPref(context).getString(ROUTINE_VERSION,"0")!!
    }

    fun saveRoutineVersion(context: Context,version: String) {
        getSharedPref(context).edit().apply {
            putString(ROUTINE_VERSION,version)
        }.apply()
    }

    fun getAppVersion(context: Context): String? {
        return try {
            val appVer = context.packageManager.getPackageInfo(context.packageName, 0).versionName
            val prefVer = getSharedPref(context).getString(APP_VERSION,"")
            if (appVer == prefVer) {
                null
            } else {
                appVer
            }
        } catch (e: Exception) {
            null
        }
    }

    fun saveAppVersion(context: Context) {
        getSharedPref(context).edit().apply {
            putString(APP_VERSION, getAppVersion(context) ?: "")
        }.apply()
    }

    fun clearSharedPref(context: Context) = getSharedPref(context).edit().clear().apply()

    private fun getSharedPref(context: Context): SharedPreferences =
        context.getSharedPreferences(VERSION_SHARED_PREF_TAG,MODE_PRIVATE)

}