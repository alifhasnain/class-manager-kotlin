package bd.edu.daffodilvarsity.classmanager.features.crlist

import android.annotation.SuppressLint
import android.content.ActivityNotFoundException
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import android.widget.ArrayAdapter
import androidx.fragment.app.viewModels
import bd.edu.daffodilvarsity.classmanager.R
import bd.edu.daffodilvarsity.classmanager.common.extensions.makeToast
import bd.edu.daffodilvarsity.classmanager.common.models.DataBindingFragment
import bd.edu.daffodilvarsity.classmanager.databinding.FragmentCRListBinding
import bd.edu.daffodilvarsity.classmanager.utils.HelperClass
import com.google.android.material.transition.MaterialSharedAxis
import dagger.hilt.android.AndroidEntryPoint
import timber.log.Timber

@AndroidEntryPoint
class CRListFragment : DataBindingFragment<FragmentCRListBinding>(R.layout.fragment_c_r_list) {

    private val adapter by lazy { CRListRecyclerViewAdapter() }

    private val viewModel: CRListViewModel by viewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initializeAnimations()
        initializeSpinners()
        initializeProperties()
        initializeRecyclerView()
        initializeObservers()
    }

    fun fetchCRProfiles() {
        viewModel.fetchCRList(
            binding.level.spinner.selectedItem.toString(),
            binding.term.spinner.selectedItem.toString(),
            binding.shift.spinner.selectedItem.toString(),
            binding.campus.spinner.selectedItem.toString()
        )
    }

    private fun initializeObservers() {
        viewModel.toast.observe(viewLifecycleOwner) { makeToast(it) }
        viewModel.crProfileList.observe(viewLifecycleOwner) { adapter.submitItems(it) }
    }

    private fun initializeRecyclerView() {

        binding.recyclerView.adapter = adapter

        adapter.sendMailClickListener = { email ->
            val intent = Intent(Intent.ACTION_SENDTO)
            intent.data = Uri.parse("mailto:")
            intent.putExtra(Intent.EXTRA_EMAIL, arrayOf(email))
            try {
                startActivity(Intent.createChooser(intent, "Send mail..."))
            } catch (e: ActivityNotFoundException) {
                Timber.e(e)
                makeToast(e.message)
            }
        }
        adapter.dialClickListener = { contactNo ->
            val intent = Intent(Intent.ACTION_DIAL)
            intent.data = Uri.parse("tel:$contactNo")
            startActivity(intent)
        }
    }

    private fun initializeProperties() {
        binding.presenter = this
        binding.viewModel = viewModel
    }

    private fun initializeAnimations() {
        enterTransition = MaterialSharedAxis(MaterialSharedAxis.Z, true)
        exitTransition = MaterialSharedAxis(MaterialSharedAxis.Z, true)
    }

    @SuppressLint("SetTextI18n")
    private fun initializeSpinners() {
        binding.level.title.text = "Level"
        binding.level.spinner.adapter = ArrayAdapter(
            requireContext(),
            R.layout.spinner_item,
            HelperClass.getLevels()
        ).apply { setDropDownViewResource(R.layout.spinner_dropdown_item) }

        binding.term.title.text = "Term"
        binding.term.spinner.adapter = ArrayAdapter(
            requireContext(),
            R.layout.spinner_item,
            HelperClass.getTerms()
        ).apply { setDropDownViewResource(R.layout.spinner_dropdown_item) }

        binding.shift.title.text = "Shift"
        binding.shift.spinner.adapter = ArrayAdapter(
            requireContext(),
            R.layout.spinner_item,
            HelperClass.getShifts()
        ).apply { setDropDownViewResource(R.layout.spinner_dropdown_item) }

        binding.campus.title.text = "Campus"
        binding.campus.spinner.adapter = ArrayAdapter(
            requireContext(),
            R.layout.spinner_item,
            HelperClass.getCampuses()
        ).apply { setDropDownViewResource(R.layout.spinner_dropdown_item) }
    }
}
