package bd.edu.daffodilvarsity.classmanager.features.admin.miscellaneous

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import bd.edu.daffodilvarsity.classmanager.R
import bd.edu.daffodilvarsity.classmanager.common.extensions.makeToast
import bd.edu.daffodilvarsity.classmanager.common.models.DataBindingFragment
import bd.edu.daffodilvarsity.classmanager.databinding.FragmentMiscellaneousBinding
import com.google.android.material.transition.MaterialSharedAxis
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

@AndroidEntryPoint
class MiscellaneousFragment : DataBindingFragment<FragmentMiscellaneousBinding>(R.layout.fragment_miscellaneous) {

    private val viewModel: MiscellaneousFragmentViewModel by viewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        //Init transitions
        enterTransition = MaterialSharedAxis(MaterialSharedAxis.Z, true)
        exitTransition = MaterialSharedAxis(MaterialSharedAxis.Z, true)
        //Init layout data
        binding.viewModel = viewModel
        binding.presenter = this

        binding.swipeRefresh.setOnRefreshListener {
            lifecycleScope.launch {
                viewModel.refreshAll()
                delay(1500)
                binding.swipeRefresh.isRefreshing = false
            }
        }

        initializeObservers()
    }

    fun resetBookCount() {
        AlertDialog.Builder(requireContext()).apply {
            setTitle("Please verify")
            setMessage("Are you sure you want to reset room book count? This can't be undone.")
            setPositiveButton("Proceed") { _, _ ->
                viewModel.resetBookCount()
            }.setNegativeButton("No", null)
        }.create().show()
    }

    private fun initializeObservers() {
        viewModel.toast.observe(viewLifecycleOwner) { makeToast(it) }
    }

}
