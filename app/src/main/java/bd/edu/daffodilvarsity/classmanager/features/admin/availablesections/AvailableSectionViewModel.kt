package bd.edu.daffodilvarsity.classmanager.features.admin.availablesections

import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.*
import bd.edu.daffodilvarsity.classmanager.common.extensions.networkErrorMessage
import bd.edu.daffodilvarsity.classmanager.common.models.LoadStates
import bd.edu.daffodilvarsity.classmanager.common.repository.Repository
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.launch
import kotlinx.coroutines.supervisorScope

class AvailableSectionViewModel @ViewModelInject constructor(
    private val repo: Repository,
    @Assisted private val savedStateHandle: SavedStateHandle
): ViewModel() {

    private val _uiState = MutableLiveData<LoadStates>(LoadStates.Empty())
    val uiStates: LiveData<LoadStates> = _uiState

    private val _sections = MutableLiveData<List<Pair<String,String>>>()
    val sections: LiveData<List<Pair<String,String>>> = _sections

    private val _toast = MutableLiveData<String>()
    val toast: LiveData<String> = _toast

    init { fetchAvailableSections() }

    fun fetchAvailableSections() = safeScope {
        _sections.value = repo.getAllSectionListFromServer()
        _uiState.value = LoadStates.decideEmptyOrNot(sections.value, "Nothing found")
    }

    fun deleteSection(id: String) = safeScope {

        val handler = CoroutineExceptionHandler { _, e ->
            _toast.value = (e as Exception).networkErrorMessage
        }

        supervisorScope {
            launch(handler) {
                val responseMessage = repo.deleteSection(id)
                _toast.value = responseMessage.message
            }.join()
            fetchAvailableSections()
        }
    }

    fun addSection(section: String) = safeScope {
        val handler = CoroutineExceptionHandler { _, e ->
            _toast.value = (e as Exception).networkErrorMessage
        }
        supervisorScope {
            launch(handler) {
                val responseMessage = repo.addSection(section)
                _toast.value = responseMessage.message
            }.join()
            fetchAvailableSections()
        }
    }

    private fun safeScope(block: (suspend () -> Unit)) = viewModelScope.launch {
        try {
            _uiState.value = LoadStates.Loading
            block()
        } catch (e: Exception) {
            _toast.value = e.networkErrorMessage
            _uiState.value = LoadStates.Error("Failed to load")
        }
    }
}