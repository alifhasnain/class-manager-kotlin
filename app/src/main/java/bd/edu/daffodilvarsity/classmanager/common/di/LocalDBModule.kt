package bd.edu.daffodilvarsity.classmanager.common.di

import android.content.Context
import androidx.room.Room
import bd.edu.daffodilvarsity.classmanager.common.database.ClassManagerDB
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Singleton

@Module
@InstallIn(ApplicationComponent::class)
object LocalDBModule {

    @Singleton
    @Provides
    fun provideDB(@ApplicationContext context: Context) = Room.databaseBuilder(
        context,
        ClassManagerDB::class.java,
        ClassManagerDB.DB_NAME
    ).fallbackToDestructiveMigration().build()

    @Singleton
    @Provides
    fun provideDAO(db: ClassManagerDB) = db.classManagerDao()

}