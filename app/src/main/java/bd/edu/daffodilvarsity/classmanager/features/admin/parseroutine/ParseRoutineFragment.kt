package bd.edu.daffodilvarsity.classmanager.features.admin.parseroutine

import android.net.Uri
import android.os.Bundle
import android.provider.OpenableColumns
import android.view.View
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.os.bundleOf
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import bd.edu.daffodilvarsity.classmanager.R
import bd.edu.daffodilvarsity.classmanager.common.extensions.bindSpinner
import bd.edu.daffodilvarsity.classmanager.common.extensions.makeToast
import bd.edu.daffodilvarsity.classmanager.common.models.DataBindingFragment
import bd.edu.daffodilvarsity.classmanager.databinding.FragmentParseRoutineBinding
import bd.edu.daffodilvarsity.classmanager.utils.HelperClass
import com.google.android.material.transition.MaterialSharedAxis
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import timber.log.Timber
import java.io.File


/**
 * [ParseRoutineFragment] responsible for parsing routine
 */

@AndroidEntryPoint
class ParseRoutineFragment : DataBindingFragment<FragmentParseRoutineBinding>(R.layout.fragment_parse_routine) {

    private var selectedFile: File? = null

    private val campus: String
        get() = binding.campusSpinner.spinner.selectedItem.toString()

    private val department: String
        get() = binding.departmentSpinner.spinner.selectedItem.toString()

    private val shift: String
        get() = binding.shiftSpinner.spinner.selectedItem.toString()

    private val viewModel: ParseRoutineViewModel by viewModels()

    //Temp directory for parsing routine
    private val fileDir by lazy {
        File(context?.cacheDir,"routine_temps")
    }

    private val csvPickerLauncher: ActivityResultLauncher<String> = registerForActivityResult(ActivityResultContracts.GetContent()) { data ->
        lifecycleScope.launch {
            try {
                if (data == null) {
                    makeToast("No file was selected")
                    return@launch
                }
                selectedFile = getFileFromUri(data)
                binding.fileSelector.selectFileText.text = getFileNameFromUri(data)
            } catch (e: Exception) {
                makeToast("Error while parsing")
                Timber.e(e)
            }
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        enterTransition = MaterialSharedAxis(MaterialSharedAxis.Z, true)
        exitTransition = MaterialSharedAxis(MaterialSharedAxis.Z, true)

        initializeSpinners()

        binding.viewmodel = viewModel
        binding.presenter = this

        binding.parseRoutine.setOnClickListener {
            viewModel.parseRoutine(department,campus,shift,selectedFile)
        }

        initViewModelObservers()
    }

    override fun onDestroy() {
        // Delete the cache routine file
        val file = File(fileDir,"routine.csv")
        val deleted = file.delete()
        Timber.e(deleted.toString())
        super.onDestroy()
    }

    fun pickCsvFile() { csvPickerLauncher.launch("text/*") }

    private fun initViewModelObservers() {

        viewModel.toast.observe(viewLifecycleOwner, { makeToast(it) })

        viewModel.classes.observe(viewLifecycleOwner, { parsedClasses ->

            val emptyRooms = parsedClasses.count { it.courseCode.isEmpty() }
            val nonEmptyRooms = parsedClasses.count { it.courseCode.isNotEmpty() }

            val bundle = bundleOf(
                "empty" to emptyRooms.toString(),
                "nonEmpty" to nonEmptyRooms.toString(),
                "total" to (emptyRooms+nonEmptyRooms).toString(),
                "campus" to campus,
                "department" to department,
                "shift" to shift
            )

            val confirmationDialog = ConfirmationDialog()
            confirmationDialog.arguments = bundle
            confirmationDialog.show(childFragmentManager,"confirmation")

            confirmationDialog.okayClickListener = {
                viewModel.uploadRoutine(campus,department,shift)
            }
        })
    }

    private suspend fun getFileFromUri(uri: Uri): File {
        return withContext(Dispatchers.IO) {
            if (!fileDir.exists()) {
                fileDir.mkdir()
            }
            val file = File(fileDir,"routine.csv")
            file.writeBytes(readBytesFromUri(uri)!!)
            return@withContext file
        }
    }

    private suspend fun getFileNameFromUri(uri: Uri): String {
        return withContext(Dispatchers.IO) {
            var fileName: String? = null
            if (uri.scheme.equals("content")) {
                val cursor = context?.contentResolver?.query(uri, null, null, null, null)
                cursor.use {
                    if (it != null && it.moveToFirst()) {
                        fileName = it.getString(it.getColumnIndex(OpenableColumns.DISPLAY_NAME))
                    }
                }
            }

            if (fileName == null) {
                fileName = uri.path
                val cut = fileName!!.lastIndexOf('/')
                if (cut != -1) {
                    fileName = fileName?.substring(cut + 1)
                }
            }

            return@withContext fileName ?: "Unknown file name"
        }
    }

    private fun readBytesFromUri(uri: Uri): ByteArray? = context?.contentResolver?.openInputStream(uri)?.buffered()?.use { it.readBytes() }

    private fun initializeSpinners() {

        binding.departmentSpinner.spinner.bindSpinner(HelperClass.getDepartments())

        binding.campusSpinner.spinner.bindSpinner(HelperClass.getCampuses())

        binding.shiftSpinner.spinner.bindSpinner(HelperClass.getShifts())

    }

}
