package bd.edu.daffodilvarsity.classmanager.features.admin.bookedrooms.singledate

import android.annotation.SuppressLint
import android.app.DatePickerDialog
import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import bd.edu.daffodilvarsity.classmanager.R
import bd.edu.daffodilvarsity.classmanager.common.extensions.makeToast
import bd.edu.daffodilvarsity.classmanager.common.models.DataBindingFragment
import bd.edu.daffodilvarsity.classmanager.databinding.FragmentBookedClassesSingleDateBinding
import bd.edu.daffodilvarsity.classmanager.features.admin.bookedrooms.common.BookedRoomsAdminViewModel
import com.google.android.material.transition.MaterialSharedAxis
import dagger.hilt.android.AndroidEntryPoint
import java.text.SimpleDateFormat
import java.util.*


@AndroidEntryPoint
class BookedClassesSingleDate : DataBindingFragment<FragmentBookedClassesSingleDateBinding>(R.layout.fragment_booked_classes_single_date) {

    private val viewModel: BookedRoomsAdminViewModel by viewModels()

    private val adapter by lazy { BookedClassesSingleDateRecyclerAdapter() }

    private var selectedDate: Calendar? = null

    @SuppressLint("SimpleDateFormat")
    private val dateFormat = SimpleDateFormat("EEE,d MMM, yyyy")

    private val dateSelectListener = DatePickerDialog.OnDateSetListener { _, year, month, dayOfMonth ->
        val date = GregorianCalendar(year, month, dayOfMonth)
        selectedDate = date.clone() as Calendar
        binding.selectedDate.text = dateFormat.format(date.time)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        enterTransition = MaterialSharedAxis(MaterialSharedAxis.Z, true)
        exitTransition = MaterialSharedAxis(MaterialSharedAxis.Z, true)
        binding.presenter = this
        binding.viewModel = viewModel
        initializeRecyclerView()
        initializeObservers()
    }

    fun pickDate() {
        val todayDate = selectedDate ?: Calendar.getInstance()
        DatePickerDialog(
            requireContext(),
            dateSelectListener,
            todayDate.get(Calendar.YEAR),
            todayDate.get(Calendar.MONTH),
            todayDate.get(Calendar.DAY_OF_MONTH)
        ).show()
    }

    fun search() {
        if (selectedDate != null) {
            viewModel.fetchBookedRoomsWithDate(selectedDate!!.timeInMillis)
        } else {
            makeToast("Please select a date first")
        }
    }

    private fun initializeRecyclerView() {
        binding.recyclerView.adapter = adapter
        adapter.onCancelClick = {
            val dialog = CancelBookAdminDialogFragment(it)
            dialog.show(childFragmentManager,"cancel-book=admin")
            dialog.onConfirmListener = { data ->
                viewModel.cancelBook(data.first,data.second,data.third,selectedDate?.timeInMillis ?: 0)
            }
        }
    }

    private fun initializeObservers() {
        viewModel.toast.observe(viewLifecycleOwner) { makeToast(it) }
        viewModel.bookedRoomsInSingleDate.observe(viewLifecycleOwner) { adapter.submitData(it) }
    }

}