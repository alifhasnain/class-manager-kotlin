package bd.edu.daffodilvarsity.classmanager.features.admin.updateroutine

import android.annotation.SuppressLint
import android.app.TimePickerDialog
import android.content.DialogInterface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import bd.edu.daffodilvarsity.classmanager.R
import bd.edu.daffodilvarsity.classmanager.common.extensions.fromJSONToPOJO
import bd.edu.daffodilvarsity.classmanager.common.extensions.makeToast
import bd.edu.daffodilvarsity.classmanager.common.models.ClassDetails
import bd.edu.daffodilvarsity.classmanager.databinding.DialogAddOrUpdateClassBinding
import bd.edu.daffodilvarsity.classmanager.utils.HelperClass
import bd.edu.daffodilvarsity.classmanager.utils.sharedprefutils.SectionSharedPrefHelper
import com.wada811.databinding.dataBinding
import dagger.hilt.android.AndroidEntryPoint
import timber.log.Timber

@AndroidEntryPoint
class AddOrUpdateClassDialog : DialogFragment() {

    private val viewModel: UpdateRoutineViewModel by viewModels()

    private val binding: DialogAddOrUpdateClassBinding by dataBinding()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NORMAL, R.style.FullScreenDialog)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.dialog_add_or_update_class, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.presenter = this
        binding.viewModel = viewModel
        initializeSpinners()
        initializeProperties()
        initializeObservers()
    }

    override fun onDismiss(dialog: DialogInterface) {
        findNavController().previousBackStackEntry?.savedStateHandle?.set("refresh", true)
        super.onDismiss(dialog)
    }

    fun pickStartTime() {
        TimePickerDialog(requireContext(), { _, hourOfDay, minute ->
            binding.startTime.text = HelperClass.getTimeStringFromSecond(hourOfDay * 3600 + minute * 60)
        }, 0, 0, false).show()
    }

    fun pickEndTime() {
        TimePickerDialog(requireContext(), { _, hourOfDay, minute ->
            binding.endTime.text = HelperClass.getTimeStringFromSecond(hourOfDay * 3600 + minute * 60)
        }, 0, 0, false).show()
    }

    private fun addClass() {
        if (verifyInputFields() && verifyStartAndEndTime()) {
            val classDetails = ClassDetails(
                0,
                binding.room.editText!!.text.toString(),
                binding.courseCode.editText!!.text.toString(),
                binding.courseName.editText!!.text.toString(),
                binding.teacherInitial.editText!!.text.toString(),
                HelperClass.getTimeSecondFromString(binding.startTime.text.toString()),
                HelperClass.getTimeSecondFromString(binding.endTime.text.toString()),
                binding.department.spinner.selectedItem.toString(),
                binding.shift.spinner.selectedItem.toString(),
                binding.campus.spinner.selectedItem.toString(),
                binding.day.spinner.selectedItem.toString(),
                if (binding.section.spinner.selectedItem.toString() == "empty") { "" } else { binding.section.spinner.selectedItem.toString() }
            )
            viewModel.addClass(classDetails)
        }
    }

    private fun updateClass() {
        if (verifyInputFields() && verifyStartAndEndTime()) {

            val oldClassDetails = arguments?.getString("data")!!.fromJSONToPOJO<ClassDetails>()

            val classDetails = ClassDetails(
                oldClassDetails.id,
                binding.room.editText!!.text.toString(),
                binding.courseCode.editText!!.text.toString(),
                binding.courseName.editText!!.text.toString(),
                binding.teacherInitial.editText!!.text.toString(),
                HelperClass.getTimeSecondFromString(binding.startTime.text.toString()),
                HelperClass.getTimeSecondFromString(binding.endTime.text.toString()),
                binding.department.spinner.selectedItem.toString(),
                binding.shift.spinner.selectedItem.toString(),
                binding.campus.spinner.selectedItem.toString(),
                binding.day.spinner.selectedItem.toString(),
                if (binding.section.spinner.selectedItem.toString() == "empty") { "" } else { binding.section.spinner.selectedItem.toString() }
            )
            viewModel.updateClass(classDetails)
        }
    }

    private fun initializeObservers() {
        viewModel.toast.observe(viewLifecycleOwner) { makeToast(it) }
    }

    @Suppress("UNCHECKED_CAST")
    private fun initializeProperties() {

        binding.toolbar.setNavigationOnClickListener { dismiss() }

        binding.save.setOnClickListener {
            val action = arguments?.getString("action")
            if (action == "add") {
                addClass()
                makeToast("Adding")
            } else if (action == "update") {
                updateClass()
                makeToast("Updating")
            }
        }

        //Initialize Toolbar title as per our action type
        val operation = arguments?.getString("action")
        if (operation == "add") {
            binding.toolbar.title = "Add Class"
            binding.save.text = "Add Class"
        } else if (operation == "update") {
            binding.toolbar.title = "Update Class"
            binding.save.text = "Update Class"

            val classDetails = arguments?.getString("data")!!.fromJSONToPOJO<ClassDetails>()
            binding.room.editText?.setText(classDetails.room)
            binding.courseCode.editText?.setText(classDetails.courseCode)
            binding.courseName.editText?.setText(classDetails.courseName)
            binding.teacherInitial.editText?.setText(classDetails.teacherInitial)
            binding.startTime.text = HelperClass.getTimeStringFromSecond(classDetails.startTime)
            binding.endTime.text = HelperClass.getTimeStringFromSecond(classDetails.endTime)

            binding.department.spinner.setSelection(
                (binding.department.spinner.adapter as ArrayAdapter<String>).getPosition(classDetails.department)
            )

            binding.section.spinner.setSelection(
                if (classDetails.section.isEmpty()) {
                    0
                } else {
                    (binding.section.spinner.adapter as ArrayAdapter<String>).getPosition(classDetails.section)
                }
            )

            binding.day.spinner.setSelection(
                (binding.day.spinner.adapter as ArrayAdapter<String>).getPosition(classDetails.dayOfWeek)
            )

            binding.campus.spinner.setSelection(
                (binding.campus.spinner.adapter as ArrayAdapter<String>).getPosition(classDetails.campus)
            )

            binding.shift.spinner.setSelection(
                (binding.shift.spinner.adapter as ArrayAdapter<String>).getPosition(classDetails.shift)
            )

        }
    }

    private fun verifyInputFields(): Boolean {
        if (binding.room.editText!!.text.isEmpty()) {
            binding.room.error = "This field can't be empty"
            return false
        } else {
            binding.room.error = null
        }
        if (binding.courseCode.editText!!.text.isEmpty() && binding.courseName.editText!!.text.isNotEmpty()) {
            binding.courseCode.error = "This field can't be empty"
            return false
        } else {
            binding.courseCode.error = null
        }
        if (binding.courseName.editText!!.text.isEmpty() && binding.courseCode.editText!!.text.isNotEmpty()) {
            binding.courseName.error = "This field can't be empty"
            return false
        } else {
            binding.courseName.error = null
        }
        return true
    }

    private fun verifyStartAndEndTime(): Boolean = try {
        HelperClass.getTimeSecondFromString(binding.startTime.text.toString())
        HelperClass.getTimeSecondFromString(binding.endTime.text.toString())
        true
    } catch (e: Exception) {
        Timber.e(e)
        makeToast("Please specify start & end time correctly")
        false
    }

    @SuppressLint("SetTextI18n")
    private fun initializeSpinners() {

        binding.department.title.text = "Department"
        binding.department.spinner.adapter = ArrayAdapter(
            requireContext(),
            R.layout.spinner_item,
            arrayOf("Computer Science & Engineering")
        ).apply { setDropDownViewResource(R.layout.spinner_dropdown_item) }

        binding.section.title.text = "Section"
        binding.section.spinner.adapter = ArrayAdapter(
            requireContext(),
            R.layout.spinner_item,
            arrayOf("empty", *(SectionSharedPrefHelper.getSections(requireContext()).toTypedArray()))
        ).apply { setDropDownViewResource(R.layout.spinner_dropdown_item) }

        binding.shift.title.text = "Shift"
        binding.shift.spinner.adapter = ArrayAdapter(
            requireContext(),
            R.layout.spinner_item,
            HelperClass.getShifts()
        ).apply { setDropDownViewResource(R.layout.spinner_dropdown_item) }

        binding.campus.title.text = "Campus"
        binding.campus.spinner.adapter = ArrayAdapter(
            requireContext(),
            R.layout.spinner_item,
            HelperClass.getCampuses()
        ).apply { setDropDownViewResource(R.layout.spinner_dropdown_item) }

        binding.day.title.text = "Day"
        binding.day.spinner.adapter = ArrayAdapter(
            requireContext(),
            R.layout.spinner_item,
            HelperClass.getSevenDaysOfWeek()
        ).apply { setDropDownViewResource(R.layout.spinner_dropdown_item) }

    }

}