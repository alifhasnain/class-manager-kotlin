package bd.edu.daffodilvarsity.classmanager.common.auth

import android.content.Context
import bd.edu.daffodilvarsity.classmanager.common.network.UserApi
import bd.edu.daffodilvarsity.classmanager.utils.sharedprefutils.AuthSharedPrefHelper
import com.auth0.android.jwt.JWT
import okhttp3.ResponseBody
import org.json.JSONObject

class Authenticator (
    private val context: Context,
    private val userClient: UserApi
) {

    var authStateListener: ((Boolean) -> Unit)? = null

    suspend fun signIn(id: String, password: String, logoutFromOthers: Boolean) {
        val responseBody: ResponseBody = if (!id.contains("-")) {
            userClient.loginFaculty(id, password, logoutFromOthers)
        } else {
            userClient.loginStudent(id, password, logoutFromOthers)
        }

        val json = JSONObject(responseBody.string())
        val refreshToken = json.getString("refresh_token")
        AuthSharedPrefHelper.saveRefreshToken(context, refreshToken)
        val accessToken = json.getString("access_token")
        AuthSharedPrefHelper.saveAccessToken(context, accessToken)
    }

    suspend fun checkProfileCompletion(): Boolean {
        val responseBody = userClient.checkProfileCompletion(
            "Bearer " + AuthSharedPrefHelper.getAccessToken(context)
        )

        val json = JSONObject(responseBody.string())
        return json.getBoolean("completed")
    }

    suspend fun getAccessToken(): String {
        val accessToken = AuthSharedPrefHelper.getAccessToken(context)
        return if (!isJwtExpired(accessToken)) {
            accessToken
        } else {
            renewAccessToken()
            AuthSharedPrefHelper.getAccessToken(context)
        }
    }

    suspend fun renewAccessToken() {
        val tokenResponseBody = userClient.renewAccessToken(
            "Bearer " + AuthSharedPrefHelper.getRefreshToken(context)
        )
        val jsonToken = JSONObject(tokenResponseBody.string())
        val accessToken = jsonToken.getString("access_token")
        AuthSharedPrefHelper.saveAccessToken(context, accessToken)
    }

    fun getUserType(): String {
        return AuthSharedPrefHelper.getUserType(context)
    }

    fun isSignedIn():Boolean {
        return AuthSharedPrefHelper.isSignedIn(context)
    }

    suspend fun signOut(refreshToken: String) = userClient.signOut("Bearer $refreshToken")

    fun isStudent(): Boolean {
        return getUserType() == AuthSharedPrefHelper.USER_TYPE_STUDENT
    }

    private fun isJwtExpired(token: String): Boolean {
        val jwt = JWT(token)
        return jwt.isExpired(1)
    }

}
