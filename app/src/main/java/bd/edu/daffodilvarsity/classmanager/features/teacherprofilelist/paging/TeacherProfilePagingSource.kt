package bd.edu.daffodilvarsity.classmanager.features.teacherprofilelist.paging

import androidx.paging.PagingSource
import bd.edu.daffodilvarsity.classmanager.common.auth.Authenticator
import bd.edu.daffodilvarsity.classmanager.common.models.ProfileTeacher
import bd.edu.daffodilvarsity.classmanager.common.network.UserApi
import timber.log.Timber

class TeacherProfilePagingSource(
    private val queryString: String?,
    private val authenticator: Authenticator,
    private val userClient: UserApi
): PagingSource<Int, ProfileTeacher>() {

    private val authToken: (suspend () -> String) = { "Bearer " + authenticator.getAccessToken() }

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, ProfileTeacher> {
        try {
            val position = params.key

            return if (position == null) {
                val profiles = userClient.fetchTeacherProfileList(authToken(), queryString, params.loadSize, position)
                val nextPage: Int? = if (profiles.isNotEmpty()) { 1 } else { null }

                LoadResult.Page(data = profiles, null, nextKey = nextPage)
            } else {
                val profiles = userClient.fetchTeacherProfileList(authToken(), queryString, params.loadSize, params.key)
                val nextPage = if (profiles.isNotEmpty()) { position + 1 } else { null }

                LoadResult.Page(profiles, position, nextPage)
            }
        } catch (e: Exception) {
            Timber.e(e)
            return LoadResult.Error(e)
        }
    }
}