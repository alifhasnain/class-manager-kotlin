package bd.edu.daffodilvarsity.classmanager.common.adapter

import android.annotation.SuppressLint
import android.view.View
import android.widget.ImageButton
import android.widget.LinearLayout
import android.widget.TextView
import androidx.databinding.BindingAdapter
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import bd.edu.daffodilvarsity.classmanager.R
import bd.edu.daffodilvarsity.classmanager.common.models.LoadStates
import bd.edu.daffodilvarsity.classmanager.utils.HelperClass
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import java.text.SimpleDateFormat
import java.util.*


@SuppressLint("SimpleDateFormat")
private val dateFormatter = SimpleDateFormat("EEE, d MMM, yyyy")
@SuppressLint("SimpleDateFormat")
private val dateTimeFormatter = SimpleDateFormat("EEE, d MMM, yyyy h:mm a")

@BindingAdapter("refreshingBinding")
fun refreshingBinding(view: SwipeRefreshLayout, refreshing: Boolean) {
    if (refreshing) {
        CoroutineScope(Dispatchers.Main).launch {
            delay(1500)
            view.isRefreshing = false
        }
    }
}

@BindingAdapter("bindNotification")
fun bindNotification(imageView: ImageButton, active: Boolean) {
    if (active) {
        imageView.setImageResource(R.drawable.ic_notification_active)
    } else {
        imageView.setImageResource(R.drawable.ic_notifications_off)
    }
}

@BindingAdapter("bindNotificationIndicator")
fun bindNotificationIndicator(view: View, active: Boolean) {
    if (active) {
        view.setBackgroundResource(R.drawable.notification_active)
    } else {
        view.setBackgroundResource(R.drawable.notification_inactive)
    }
}

@BindingAdapter("bindDepartment")
fun bindDepartment(view: TextView, department: String) {
    if(department == "Computer Science & Engineering") {
        view.text = "CSE"
    } else {
        view.text = ""
    }
}

@BindingAdapter("visibility")
fun visibility(view: View,visible: Boolean) {
    if(visible) {
        view.visibility = View.VISIBLE
    } else {
        view.visibility = View.GONE
    }
}

@BindingAdapter("bindLoadingState")
fun uiStateWhileLoading(view: View, state: LoadStates?) {
    state?.let {
        view.visibility = if (state is LoadStates.Loading) { View.VISIBLE } else { View.GONE }
    }
}

@BindingAdapter("bindErrorState")
fun uiStateWhenError(view: LinearLayout, state: LoadStates?) {
    state?.let {
        view.visibility = if(state is LoadStates.Error) {
            val msg = view.getChildAt(1) as TextView
            msg.text = state.msg
            View.VISIBLE
        } else { View.GONE }
    }
}

@BindingAdapter("bindEmptyState")
fun uiStateWhenEmpty(view: LinearLayout, state: LoadStates?) {
    state?.let {
        if(state is LoadStates.Empty) {
            val msg = view.getChildAt(1) as TextView
            msg.text = state.msg
            view.visibility = View.VISIBLE
        } else {
            view.visibility = View.GONE
        }
    }
}

@BindingAdapter("bindNonEmptyState")
fun uiStateWhenNotEmpty(view: View, state: LoadStates?) {
    state?.let {
        view.visibility = if (state is LoadStates.NotEmpty) { View.VISIBLE } else { View.GONE }
    }
}

@BindingAdapter("bindDisableState")
fun bindButtonDisableState(view: View, state: LoadStates) {
    view.isEnabled = state !is LoadStates.Loading
}

@SuppressLint("SetTextI18n")
@BindingAdapter("bindStartTime","bindEndTime")
fun bindStartAndEndTime(view: TextView, startTime: Int, endTime: Int) {
    view.text = "${HelperClass.getTimeStringFromSecond(startTime)}-${HelperClass.getTimeStringFromSecond(endTime)}"
}

@BindingAdapter("bindDate")
fun bindDate(view: TextView, date: Date) {
    view.text = dateFormatter.format(date)
}

@BindingAdapter("bindDateTime")
fun bindDateTime(view: TextView, date: Date) {
    view.text = dateTimeFormatter.format(date)
}
