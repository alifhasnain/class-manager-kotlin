package bd.edu.daffodilvarsity.classmanager.features.admin.parseroutine

import bd.edu.daffodilvarsity.classmanager.common.models.ClassDetails
import de.siegmar.fastcsv.reader.CsvReader
import de.siegmar.fastcsv.reader.CsvRow
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import timber.log.Timber
import java.io.File
import java.nio.charset.StandardCharsets


object ParseHelper {

    suspend fun readRoutineCSEDayMC(file: File, department: String, campus: String, shift: String): MutableList<ClassDetails> = withContext(Dispatchers.IO) {
        fun parserFunction(startDay: String, endDay: String): MutableList<ClassDetails> {

            val classesInDay: MutableList<ClassDetails> = ArrayList()

            fun splitCourseCode(str: String): String = try {
                str.substring(0, str.indexOf("("))
            } catch (e: StringIndexOutOfBoundsException) {
                Timber.e(e, "For String : $str")
                str
            }

            fun splitSection(string: String): MutableList<String> = try {
                val section = string.substring(string.indexOf("(") + 1, string.indexOf(")"))
                val splittedSection = section.split("+")
                if (splittedSection.size == 1) {
                    mutableListOf(section)
                } else {
                    splittedSection.map { it.trim() } as MutableList<String>
                }
            } catch (e: StringIndexOutOfBoundsException) {
                Timber.e("For String : $string")
                mutableListOf("")
            }

            fun getClassDetailsFromRow(
                r: CsvRow,
                i1: Int,
                i2: Int,
                i3: Int,
                day: String,
                startTime: Int,
                endTime: Int
            ): MutableList<ClassDetails> {
                val classes = mutableListOf<ClassDetails>()
                val sections = splitSection(r.getField(i2).trim())
                for (section in sections) {
                    val singleClass = ClassDetails(
                        0,
                        r.getField(i1).trim(),
                        splitCourseCode(r.getField(i2).trim()),
                        "",
                        r.getField(i3).trim(),
                        startTime,
                        endTime,
                        department,
                        shift,
                        campus,
                        day,
                        section
                    )
                    if (singleClass.room.isNotEmpty()) {
                        classes.add(singleClass)
                    }
                }
                return classes
            }

            val csvReader = CsvReader()
            csvReader.setSkipEmptyRows(true)
            val parser = csvReader.parse(file, StandardCharsets.UTF_8)
            var row: CsvRow?

            while (kotlin.run { row = parser.nextRow(); row != null }) {

                if (row!!.getField(0) != startDay) {
                    continue
                }

                if (row!!.getField(0) == startDay) {
                    parser.nextRow()
                    parser.nextRow()
                    break
                }
            }

            while (kotlin.run { row = parser.nextRow(); row != null }) {
                if (row!!.getField(0) == endDay || row!!.getField(0) == "Electrical Lab") {
                    parser.nextRow()
                    break
                }

                if (startDay != "Friday") {
                    val cd1: MutableList<ClassDetails> = getClassDetailsFromRow(row!!, 0, 1, 2, startDay, 30600, 36000)
                    val cd2: MutableList<ClassDetails> = getClassDetailsFromRow(row!!, 3, 4, 5, startDay, 36000, 41400)
                    val cd3: MutableList<ClassDetails> = getClassDetailsFromRow(row!!, 6, 7, 8, startDay, 41400, 46800)
                    val cd4: MutableList<ClassDetails> = getClassDetailsFromRow(row!!, 9, 10, 11, startDay, 46800, 52200)
                    val cd5: MutableList<ClassDetails> = getClassDetailsFromRow(row!!, 12, 13, 14, startDay, 52200, 57600)
                    val cd6: MutableList<ClassDetails> = getClassDetailsFromRow(row!!, 15, 16, 17, startDay, 57600, 63000)

                    classesInDay.addAll(cd1)
                    classesInDay.addAll(cd2)
                    classesInDay.addAll(cd3)
                    classesInDay.addAll(cd4)
                    classesInDay.addAll(cd5)
                    classesInDay.addAll(cd6)

                    // Only filter those where room no is mentioned
                    classesInDay.filter { it.room.isNotEmpty() }
                } else if (startDay == "Friday") {
                    val cd1: MutableList<ClassDetails> = getClassDetailsFromRow(row!!, 0, 1, 2, startDay, 32400, 37800)
                    val cd2: MutableList<ClassDetails> = getClassDetailsFromRow(row!!, 3, 4, 5, startDay, 37800, 43200)
                    val cd3: MutableList<ClassDetails> = getClassDetailsFromRow(row!!, 7, 8, 9, startDay, 52200, 57600)
                    val cd4: MutableList<ClassDetails> = getClassDetailsFromRow(row!!, 10, 11, 12, startDay, 57600, 63000)

                    classesInDay.addAll(cd1)
                    classesInDay.addAll(cd2)
                    classesInDay.addAll(cd3)
                    classesInDay.addAll(cd4)

                    // Only filter those where room no is mentioned
                    classesInDay.filter { it.room.isNotEmpty() }
                }

            }

            while (kotlin.run { row = parser.nextRow(); row != null }) {
                if (row!!.getField(0) == endDay) {
                    break
                }

                if (startDay != "Friday") {
                    val e1 = getClassDetailsFromRow(row!!, 0, 1, 2, startDay, 32400, 39600)
                    val e2 = getClassDetailsFromRow(row!!, 0, 3, 4, startDay, 39600, 46800)
                    val e3 = getClassDetailsFromRow(row!!, 0, 5, 6, startDay, 46800, 54000)
                    val e4 = getClassDetailsFromRow(row!!, 0, 7, 8, startDay, 54000, 61200)

                    classesInDay.addAll(e1)
                    classesInDay.addAll(e2)
                    classesInDay.addAll(e3)
                    classesInDay.addAll(e4)

                    classesInDay.filter { it.room.isNotEmpty() }
                }
            }
            return classesInDay
        }

        val routine: MutableList<ClassDetails> = ArrayList()
        routine.addAll(parserFunction("Saturday", "Sunday"))
        routine.addAll(parserFunction("Sunday", "Monday"))
        routine.addAll(parserFunction("Monday", "Tuesday"))
        routine.addAll(parserFunction("Tuesday", "Wednesday"))
        routine.addAll(parserFunction("Wednesday", "Thursday"))
        routine.addAll(parserFunction("Thursday", "Friday"))
        routine.addAll(parserFunction("Friday", "some random text"))

        return@withContext routine
    }

    suspend fun readRoutineCSEEveMC(file: File , department: String , campus: String , shift: String): MutableList<ClassDetails> {
        return withContext(Dispatchers.IO) {
            fun parserFunction(startDay: String, endDay: String): MutableList<ClassDetails> {

                val classesInWeek: MutableList<ClassDetails> = ArrayList()

                fun splitCourseCode(str: String): String = try {
                    str.substring(0, str.indexOf("("))
                } catch (e: StringIndexOutOfBoundsException) {
                    Timber.e(e,"For String : $str")
                    str
                }

                fun splitSection(string: String): String {
                    return try {
                        string.substring(string.indexOf("(") + 1, string.indexOf(")"))
                    } catch (e: StringIndexOutOfBoundsException) {
                        Timber.e("For String : $string")
                        ""
                    }
                }

                fun getClassDetailsFromRow(r: CsvRow,i1: Int , i2: Int, i3: Int, i4: Int, day: String,startTime: Int , endTime: Int): ClassDetails {
                    return ClassDetails(
                        0,
                        r.getField(i1).trim(),
                        splitCourseCode(r.getField(i2).trim()),
                        r.getField(i3).trim(),
                        r.getField(i4).trim(),
                        startTime,
                        endTime,
                        department,
                        shift,
                        campus,
                        day,
                        splitSection(r.getField(i2).trim())
                    )
                }

                val csvReader = CsvReader()
                csvReader.setSkipEmptyRows(true)
                val parser = csvReader.parse(file, StandardCharsets.UTF_8)
                var row: CsvRow?

                var startCounting = false
                while (kotlin.run { row = parser.nextRow(); row != null }) {
                    if (row!!.getField(0) == startDay) {
                        startCounting = true
                    } else if(row!!.getField(0)==endDay) {
                        break
                    }

                    if (startCounting) {
                        val cd1 = getClassDetailsFromRow(row!!,1,2,3,5,startDay,64800,70200)
                        val cd2 = getClassDetailsFromRow(row!!,1,2,3,5,startDay,70200,75600)

                        classesInWeek.addAll(arrayOf(cd1,cd2).filter {
                            cd1.room.isNotEmpty()
                        })
                    }
                }
                return classesInWeek
            }

            val routine: MutableList<ClassDetails> = ArrayList()
            routine.addAll(parserFunction("Saturday","Sunday"))
            routine.addAll(parserFunction("Sunday","Monday"))
            routine.addAll(parserFunction("Monday","Tuesday"))
            routine.addAll(parserFunction("Tuesday","Wednesday"))
            routine.addAll(parserFunction("Wednesday","Thursday"))
            routine.addAll(parserFunction("Thursday","Friday"))

            return@withContext routine
        }
    }
}