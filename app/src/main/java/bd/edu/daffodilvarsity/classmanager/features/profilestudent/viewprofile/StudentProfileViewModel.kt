package bd.edu.daffodilvarsity.classmanager.features.profilestudent.viewprofile

import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.*
import bd.edu.daffodilvarsity.classmanager.common.extensions.networkErrorMessage
import bd.edu.daffodilvarsity.classmanager.common.extensions.setValueThenNullify
import bd.edu.daffodilvarsity.classmanager.common.models.ProfileStudent
import bd.edu.daffodilvarsity.classmanager.common.models.RegisteredCourse
import bd.edu.daffodilvarsity.classmanager.common.repository.Repository
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.launch
import kotlinx.coroutines.supervisorScope

class StudentProfileViewModel @ViewModelInject constructor(
    private val repo: Repository,
    @Assisted private val savedStateHandle: SavedStateHandle
): ViewModel() {

    private val _profileStudent = MutableLiveData<ProfileStudent>()
    val profileStudent: LiveData<ProfileStudent> = _profileStudent

    private val _registeredCourses = MutableLiveData<MutableList<RegisteredCourse>>()
    val registeredCourses: LiveData<MutableList<RegisteredCourse>> = _registeredCourses

    val allCourses: LiveData<List<String>> = Transformations.switchMap(profileStudent) { profile ->
        repo.getCoursesFromLocalAsLiveData(profile.shift,profile.department).map {
            it.map { course -> "${course.courseCode} : ${course.courseName}" }
        }
    }

    val allSections: LiveData<List<String>> = repo.getDistinctSectionsFromLocal()

    private val _toast = MutableLiveData<String>()
    val toast: LiveData<String> = _toast

    private val _isRefreshing = MutableLiveData<Boolean>()
    val isRefreshing: LiveData<Boolean> = _isRefreshing

    init {
        loadProfileAndCourses()
    }

    fun initProfileAndCourses(profileStudent: ProfileStudent,courses: MutableList<RegisteredCourse>) {
        _profileStudent.value = profileStudent
        _registeredCourses.value = courses
    }

    fun loadProfileAndCourses() = safeScope {
        val handler = CoroutineExceptionHandler { _, exception ->
            _toast.setValueThenNullify((exception as Exception).networkErrorMessage)
        }
        supervisorScope {
            launch(handler) { _profileStudent.value = repo.fetchStudentProfileFromServer() }
            launch(handler) { _registeredCourses.value = repo.fetchRegisteredCourses() }
        }
    }

    fun addCourse(course: RegisteredCourse) = safeScope {
        //add a single course and get latest updated courses
        _registeredCourses.value = repo.addRegisteredCourse(course)
    }

    fun removeCourse(course: RegisteredCourse) = safeScope {
        //Remove a single course & get latest updated courses
        _registeredCourses.value = repo.removeRegisteredCourse(course)
    }

    private fun safeScope(block: (suspend () -> Unit)) = viewModelScope.launch {
        try {
            _isRefreshing.value = true
            block()
        } catch (e: Exception) {
            _toast.setValueThenNullify(e.networkErrorMessage)
        } finally {
            _isRefreshing.value = false
        }
    }
}