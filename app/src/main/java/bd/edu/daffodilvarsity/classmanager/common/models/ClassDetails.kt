package bd.edu.daffodilvarsity.classmanager.common.models

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
@Entity
data class ClassDetails
(
    @PrimaryKey val id: Long = 0,
    val room: String,
    @field:Json(name = "course_code") val courseCode: String,
    @field:Json(name = "course_name") val courseName: String = "",
    @field:Json(name = "teacher_initial") val teacherInitial: String,
    @field:Json(name = "start_time") val startTime: Int,
    @field:Json(name = "end_time") val endTime: Int,
    val department: String,
    val shift: String,
    val campus: String,
    val dayOfWeek: String,
    val section: String,
    val notification: Boolean = false
)