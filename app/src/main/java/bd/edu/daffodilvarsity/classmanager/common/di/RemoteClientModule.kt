package bd.edu.daffodilvarsity.classmanager.common.di

import bd.edu.daffodilvarsity.classmanager.BuildConfig
import bd.edu.daffodilvarsity.classmanager.common.network.AdminApi
import bd.edu.daffodilvarsity.classmanager.common.network.ResponseErrorInterceptor
import bd.edu.daffodilvarsity.classmanager.common.network.UserApi
import bd.edu.daffodilvarsity.classmanager.utils.Routes
import com.squareup.moshi.Moshi
import com.squareup.moshi.adapters.Rfc3339DateJsonAdapter
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import java.util.*
import java.util.concurrent.TimeUnit
import javax.inject.Qualifier
import javax.inject.Singleton

@Module
@InstallIn(ApplicationComponent::class)
object RemoteClientModule {

    @Qualifier
    @Retention(AnnotationRetention.BINARY)
    annotation class ErrorInterceptor

    @Qualifier
    @Retention(AnnotationRetention.BINARY)
    annotation class LoggingInterceptor

    @Provides
    fun provideMoshi(): Moshi = Moshi.Builder().apply {
        add(Date::class.java, Rfc3339DateJsonAdapter().nullSafe())
    }.build()

    @ErrorInterceptor
    @Provides
    fun provideErrorInterceptor(): Interceptor = ResponseErrorInterceptor()

    @LoggingInterceptor
    @Provides
    fun provideLoggingInterceptor(): Interceptor = HttpLoggingInterceptor().apply {
        level = HttpLoggingInterceptor.Level.BODY
    }

    @Provides
    fun provideHttpClient(
        @ErrorInterceptor errorInterceptor: Interceptor,
        @LoggingInterceptor loggingInterceptor: Interceptor
    ): OkHttpClient = OkHttpClient.Builder().apply {
        connectTimeout(15, TimeUnit.SECONDS)
        readTimeout(15, TimeUnit.SECONDS)
        writeTimeout(15, TimeUnit.SECONDS)
        addInterceptor(errorInterceptor)
        if (BuildConfig.DEBUG) addInterceptor(loggingInterceptor)
    }.build()

    @Provides
    fun provideRetrofitBuilder(httpClient: OkHttpClient, moshi: Moshi): Retrofit.Builder = Retrofit.Builder().apply {
        client(httpClient)
        addConverterFactory(MoshiConverterFactory.create(moshi))
    }

    @Singleton
    @Provides
    fun provideUserRetrofitClient(retrofitBuilder: Retrofit.Builder): UserApi = retrofitBuilder.apply {
        baseUrl(Routes.USER_ROUTE)
    }.build().create(UserApi::class.java)

    @Singleton
    @Provides
    fun provideAdminRetrofitClient(retrofitBuilder: Retrofit.Builder): AdminApi = retrofitBuilder.apply {
        baseUrl(Routes.ADMIN_ROUTE)
    }.build().create(AdminApi::class.java)

}