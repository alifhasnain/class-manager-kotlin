package bd.edu.daffodilvarsity.classmanager.features.profilestudent.editprofile

import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.*
import bd.edu.daffodilvarsity.classmanager.common.models.ProfileStudent
import bd.edu.daffodilvarsity.classmanager.common.models.RegisteredCourse
import bd.edu.daffodilvarsity.classmanager.common.repository.Repository
import kotlinx.coroutines.launch
import okio.IOException
import timber.log.Timber

class EditProfileStudentViewModel @ViewModelInject constructor(
    private val repo: Repository,
    @Assisted private val savedStateHandle: SavedStateHandle
): ViewModel() {

    private val _profile: MutableLiveData<ProfileStudent> = MutableLiveData()
    val profile: LiveData<ProfileStudent> = _profile

    private val _registeredCourse = MutableLiveData<MutableList<RegisteredCourse>>()
    val registeredCourse: LiveData<MutableList<RegisteredCourse>> = _registeredCourse

    private val _toast = MutableLiveData<String>()
    val toast: LiveData<String> = _toast

    private val _progressBarVisibility = MutableLiveData(false)
    val progressBarVisibility: LiveData<Boolean> = _progressBarVisibility

    private val _saveButtonEnabled = MutableLiveData(true)
    val saveButtonEnabled: LiveData<Boolean> = _saveButtonEnabled

    fun initializeProfile(profile: ProfileStudent?) { _profile.value = profile }

    fun saveProfile(profile: ProfileStudent) = safeScope {
        _profile.value = repo.saveStudentProfileToServer(profile)
        _toast.value = "saved successfully"
    }

    fun saveProfileAndUpdateCourses(profile: ProfileStudent) = safeScope {
        val (updatedProfile,registeredCourses) = repo.saveProfileAndUpdateRegisteredCourse(profile)
        _profile.value = updatedProfile
        _registeredCourse.value = registeredCourses
        _toast.value = "saved successfully"
    }

    private fun safeScope(block: (suspend () -> Unit)) = viewModelScope.launch {
        try {
            _progressBarVisibility.value = true
            _saveButtonEnabled.value = false
            block()
        } catch (io: IOException) {
            Timber.e(io)
            _toast.value = io.message
        } catch (e: Exception) {
            Timber.e(e)
            _toast.value = "Failed to connect. Please check your connection and try again"
        } finally {
            _progressBarVisibility.value = false
            _saveButtonEnabled.value = true
        }
    }

}