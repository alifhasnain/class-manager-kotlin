package bd.edu.daffodilvarsity.classmanager.common.models

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class ProfileTeacher
(
    @field:Json(name = "employee_id") val employeeID: String,
    val name: String,
    val email: String,
    val department: String,
    @field:Json(name = "teacher_initial") val initial: String,
    val designation: String,
    @field:Json(name = "contact_no") val contactNo: String,
    @field:Json(name = "book_count") val bookCount : Int = 0
)