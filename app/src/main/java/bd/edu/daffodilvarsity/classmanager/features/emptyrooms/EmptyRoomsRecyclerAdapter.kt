package bd.edu.daffodilvarsity.classmanager.features.emptyrooms

import androidx.recyclerview.widget.AsyncListDiffer
import bd.edu.daffodilvarsity.classmanager.R
import bd.edu.daffodilvarsity.classmanager.common.adapter.SingleItemBaseAdapter
import bd.edu.daffodilvarsity.classmanager.common.diffcallbacks.DiffCallbacks.CLASS_DETAILS_ITEM_DIFF_CALLBACK
import bd.edu.daffodilvarsity.classmanager.common.models.ClassDetails

class EmptyRoomsRecyclerAdapter: SingleItemBaseAdapter<ClassDetails>() {

    private val differ = AsyncListDiffer(this, CLASS_DETAILS_ITEM_DIFF_CALLBACK)

    private var classes: List<ClassDetails>
        get() = differ.currentList
        set(value) = differ.submitList(value)

    fun submitItems(updatedClasses: MutableList<ClassDetails>) { classes = updatedClasses}

    override fun clickedItem(item: ClassDetails) { }

    override fun getItemForPosition(position: Int): ClassDetails = classes[position]

    override fun getLayoutIdForPosition(position: Int): Int = R.layout.list_item_empty_rooms

    override fun getItemCount(): Int = classes.size

}