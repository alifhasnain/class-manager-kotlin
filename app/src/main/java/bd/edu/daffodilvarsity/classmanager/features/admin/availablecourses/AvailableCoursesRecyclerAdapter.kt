package bd.edu.daffodilvarsity.classmanager.features.admin.availablecourses

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import bd.edu.daffodilvarsity.classmanager.common.models.Course
import bd.edu.daffodilvarsity.classmanager.databinding.ListItemAvailableCoursesBinding

class AvailableCoursesRecyclerAdapter: RecyclerView.Adapter<AvailableCoursesRecyclerAdapter.ViewHolder>() {

    private val courses = mutableListOf<Course>()

    var onLongClickListener: ((Course) -> Unit)? = null

    fun updateItems(updatedCourses: List<Course>) {
        courses.clear()
        courses.addAll(updatedCourses)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = ListItemAvailableCoursesBinding.inflate (
            LayoutInflater.from(parent.context),
            parent,
            false
        )
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(courses[position], this)
    }

    override fun getItemCount(): Int {
        return courses.size
    }

    fun onLongClick(course: Course): Boolean {
        onLongClickListener?.invoke(course)
        return true
    }

    class ViewHolder(private val binding: ListItemAvailableCoursesBinding): RecyclerView.ViewHolder(binding.root) {
        fun bind(course: Course, adapter: AvailableCoursesRecyclerAdapter) {
            binding.data = course
            binding.adapter = adapter
            binding.executePendingBindings()
        }
    }
}