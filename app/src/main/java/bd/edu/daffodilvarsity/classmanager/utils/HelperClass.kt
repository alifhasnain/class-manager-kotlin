package bd.edu.daffodilvarsity.classmanager.utils

import bd.edu.daffodilvarsity.classmanager.common.models.ClassDetails
import java.text.DecimalFormat
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.LinkedHashMap

object HelperClass {

    const val CSE_DEPARTMENT = "Computer Science & Engineering"

    fun getSectionList() : ArrayList<String> {
        return arrayListOf("A","B","C","D","E","F","G","H","I","J","K","L",
            "M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z",
            "R1","R2","R3","R4",
            "O1","O2","O3","O4","O5","O6","O7","O8","O9","O10","O11","O12","O13","O14","O15","O16","O17","O18",
        )
    }

    fun getCampuses(): MutableList<String> {
        return arrayListOf("Main Campus" , "Permanent Campus")
    }

    fun getShifts(): MutableList<String> {
        return arrayListOf("Day","Evening")
    }

    fun getDepartments(): MutableList<String> {
        return arrayListOf("Computer Science & Engineering")
    }

    fun getDepartmentShortFrom(department: String): String {
        return when(department) {
            CSE_DEPARTMENT -> "CSE"
            else -> ""
        }
    }

    fun getLevels(): MutableList<String> {
        return arrayListOf("1","2","3","4")
    }

    fun getTerms(): MutableList<String> {
        return arrayListOf("1","2","3")
    }

    fun getSevenDaysOfWeek(): MutableList<String> {
        return mutableListOf("Saturday","Sunday","Monday","Tuesday","Wednesday","Thursday","Friday")
    }

    fun getDayOfWeek(index: Int): String {
        return when(index) {
            0 -> "Saturday"
            1 -> "Sunday"
            2 -> "Monday"
            3 -> "Tuesday"
            4 -> "Wednesday"
            5 -> "Thursday"
            6 -> "Friday"
            else -> ""
        }
    }

    fun getDayOfWeekIndex(): Int {
        val day = Calendar.getInstance().let {
            it.get(Calendar.DAY_OF_WEEK)
        }
        return when(day) {
            Calendar.SATURDAY -> 0
            Calendar.SUNDAY -> 1
            Calendar.MONDAY -> 2
            Calendar.TUESDAY -> 3
            Calendar.WEDNESDAY -> 4
            Calendar.THURSDAY -> 5
            Calendar.FRIDAY -> 6
            else -> 0
        }
    }

    fun getSecondPair(time: String): Pair<Int,Int> {
        val times = time.split("-")
        return getTimeSecondFromString(times[0]) to getTimeSecondFromString(times[1])
    }

    fun getClassTimesMainCampus(): LinkedHashMap<String, Pair<Int, Int>> {
        val times: LinkedHashMap<String,Pair<Int,Int>> = LinkedHashMap()
        times["08:30AM-10:00AM"] = getSecondPair("08:30AM-10:00AM")
        times["10:00AM-11:30AM"] = getSecondPair("10:00AM-11:30AM")
        times["11:30AM-01:00PM"] = getSecondPair("11:30AM-01:00PM")
        times["01:00PM-02:30PM"] = getSecondPair("01:00PM-02:30PM")
        times["02:30PM-04:00PM"] = getSecondPair("02:30PM-04:00PM")
        times["04:00PM-05:30PM"] = getSecondPair("04:00PM-05:30PM")
        times["06:00PM-07:30PM"] = getSecondPair("06:00PM-07:30PM")
        times["07:30PM-09:00PM"] = getSecondPair("07:30PM-09:00PM")

        return times
    }

    fun sortClassesByWeek(classes: MutableList<ClassDetails>): MutableList<ClassDetails> {
        fun getEmptyClassDetails(day: String): ClassDetails {
            return ClassDetails(0,"","","","",0,0,"","","",day,"")
        }
        val sortedClasses: MutableList<ClassDetails> = mutableListOf()

        val groupedClassesByWeek = classes.groupBy { it.dayOfWeek }

        groupedClassesByWeek["Saturday"]?.let {
            sortedClasses.add(getEmptyClassDetails("Saturday"))
            sortedClasses.addAll(it)
        }

        groupedClassesByWeek["Sunday"]?.let {
            sortedClasses.add(getEmptyClassDetails("Sunday"))
            sortedClasses.addAll(it)
        }

        groupedClassesByWeek["Monday"]?.let {
            sortedClasses.add(getEmptyClassDetails("Monday"))
            sortedClasses.addAll(it)
        }

        groupedClassesByWeek["Tuesday"]?.let {
            sortedClasses.add(getEmptyClassDetails("Tuesday"))
            sortedClasses.addAll(it)
        }

        groupedClassesByWeek["Wednesday"]?.let {
            sortedClasses.add(getEmptyClassDetails("Wednesday"))
            sortedClasses.addAll(it)
        }

        groupedClassesByWeek["Thursday"]?.let {
            sortedClasses.add(getEmptyClassDetails("Thursday"))
            sortedClasses.addAll(it)
        }

        groupedClassesByWeek["Friday"]?.let {
            sortedClasses.add(getEmptyClassDetails("Friday"))
            sortedClasses.addAll(it)
        }
        return sortedClasses
    }

    fun getTimeStringFromSecond(sec: Int): String {
        val period: String
        var newTime = if (sec >= 43200) {
            period = "PM"
            sec - 43200
        } else {
            period = "AM"
            sec
        }
        val hour = if((newTime / 3600) > 0) { newTime / 3600 } else { 12 }
        newTime %= 3600
        val minute = newTime / 60
        val formatter = DecimalFormat("00")
        return "${formatter.format(hour)}:${formatter.format(minute)}$period"
    }

    fun getTimeSecondFromString(time: String): Int {
        val midnightTime = Calendar.getInstance()
        midnightTime.set(Calendar.HOUR_OF_DAY,0)
        midnightTime.set(Calendar.MINUTE,0)
        midnightTime.set(Calendar.SECOND,0)
        midnightTime.set(Calendar.MILLISECOND,0)

        val formatter = SimpleDateFormat("hh:mmaa")
        val date = Calendar.getInstance()
        date.time = formatter.parse(time)

        val currentTime = Calendar.getInstance()
        currentTime.set(Calendar.HOUR,date.get(Calendar.HOUR))
        currentTime.set(Calendar.AM_PM,date.get(Calendar.AM_PM))
        currentTime.set(Calendar.MINUTE,date.get(Calendar.MINUTE))
        currentTime.set(Calendar.SECOND,0)
        currentTime.set(Calendar.MILLISECOND,0)

        return ((currentTime.timeInMillis - midnightTime.timeInMillis) / 1000).toInt()
    }
}