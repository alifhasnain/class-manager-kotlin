package bd.edu.daffodilvarsity.classmanager.common.workers

import android.content.Context
import androidx.hilt.Assisted
import androidx.hilt.work.WorkerInject
import androidx.work.CoroutineWorker
import androidx.work.WorkerParameters
import bd.edu.daffodilvarsity.classmanager.common.auth.Authenticator
import kotlinx.coroutines.coroutineScope
import timber.log.Timber

class CleanUpAfterLogout @WorkerInject constructor (
    @Assisted context: Context,
    @Assisted params: WorkerParameters,
    private val authenticator: Authenticator
): CoroutineWorker(context,params) {

    override suspend fun doWork(): Result = coroutineScope {
        try {
            val refreshToken = inputData.getString("refreshToken")
            authenticator.signOut(refreshToken ?: "")
        } catch (e: Exception) {
            Timber.e(e)
            return@coroutineScope Result.retry()
        }
        Result.success()
    }
}