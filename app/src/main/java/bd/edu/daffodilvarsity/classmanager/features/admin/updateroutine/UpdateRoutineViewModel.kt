package bd.edu.daffodilvarsity.classmanager.features.admin.updateroutine

import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.*
import bd.edu.daffodilvarsity.classmanager.common.extensions.networkErrorMessage
import bd.edu.daffodilvarsity.classmanager.common.models.ClassDetails
import bd.edu.daffodilvarsity.classmanager.common.models.LoadStates
import bd.edu.daffodilvarsity.classmanager.common.models.Quadruple
import bd.edu.daffodilvarsity.classmanager.common.repository.Repository
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.launch
import kotlinx.coroutines.supervisorScope

class UpdateRoutineViewModel @ViewModelInject constructor(
    private val repo: Repository,
    @Assisted private val savedStateHandle: SavedStateHandle
) : ViewModel() {

    private val _uiState = MutableLiveData<LoadStates>(LoadStates.Empty("Empty"))
    val uiState : LiveData<LoadStates> = _uiState

    private var inputs: Quadruple<Int, Int, String, String>? = null

    private val _classes = MutableLiveData<List<ClassDetails>>()
    val classes: LiveData<List<ClassDetails>> = _classes

    private val _toast = MutableLiveData<String>()
    val toast: LiveData<String> = _toast

    private val handler = CoroutineExceptionHandler { _, e ->
        _toast.value = (e as Exception).networkErrorMessage
    }

    fun fetchClasses(startTime: Int, endTime: Int, day: String, campus: String) = safeScope {
        inputs = Quadruple(startTime, endTime, day, campus)
        _classes.value = repo.fetchRoutine(startTime, endTime, campus, day)
        _uiState.value = LoadStates.decideEmptyOrNot(classes.value, "Nothing found")
    }

    fun addClass(classDetails: ClassDetails) = safeScope {
        val responseMessage = repo.addSingleClass(classDetails)
        _toast.value = responseMessage.message
        _uiState.value = LoadStates.NotEmpty
    }

    fun updateClass(classDetails: ClassDetails) = safeScope {
        val responseMessage = repo.updateSingleClass(classDetails)
        _toast.value = responseMessage.message
        _uiState.value = LoadStates.NotEmpty
    }

    fun deleteClass(id: String) = safeScope {
        supervisorScope {
            launch(handler) {
                val responseMessage = repo.deleteSingleClass(id)
                _toast.value = responseMessage.message
            }.join()
            fetchClassesFromCachedQueryData()
        }
    }

    fun fetchClassesFromCachedQueryData() = safeScope {
        inputs?.let {(startTime, endTime, day, campus) ->
            _classes.value = repo.fetchRoutine(startTime, endTime, campus, day)
            _uiState.value = LoadStates.decideEmptyOrNot(classes.value, "Nothing found")
        }
    }

    private fun safeScope(block: (suspend () -> Unit)) = viewModelScope.launch {
        try {
            _uiState.value = LoadStates.Loading
            block()
        } catch (e: Exception) {
            _toast.value = e.networkErrorMessage
            _uiState.value = LoadStates.Error("Error while loading")
        }
    }

}