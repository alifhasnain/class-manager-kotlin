package bd.edu.daffodilvarsity.classmanager.features.admin.bookroom

import android.os.Bundle
import android.view.View
import android.widget.ArrayAdapter
import androidx.fragment.app.DialogFragment
import androidx.navigation.fragment.findNavController
import bd.edu.daffodilvarsity.classmanager.R
import bd.edu.daffodilvarsity.classmanager.common.extensions.fromPOJOToJSON
import bd.edu.daffodilvarsity.classmanager.common.models.RoomBookingInfo
import bd.edu.daffodilvarsity.classmanager.databinding.DialogFinishBookAdminBinding
import bd.edu.daffodilvarsity.classmanager.utils.HelperClass
import bd.edu.daffodilvarsity.classmanager.utils.sharedprefutils.SectionSharedPrefHelper
import com.wada811.databinding.dataBinding


class FinishBookAdminDialog : DialogFragment(R.layout.dialog_finish_book_admin) {

    private val reservationDateTimestamp: Long
        get() = arguments?.getLong("reservationDateTimestamp") ?: 0

    private val reservationDate: String
        get() = arguments?.getString("reservationDate") ?: ""

    private val room: String
        get() = arguments?.getString("room") ?: ""

    private val time: String
        get() = arguments?.getString("time") ?: ""

    private val pk: String
        get() = arguments?.getString("pk") ?: ""

    private val binding by dataBinding<DialogFinishBookAdminBinding>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NORMAL, R.style.FullScreenDialog)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initializeProperties()
        initializeSpinners()
    }

    fun proceedRoomBook() {
        val courseCode = binding.courseCode.editText!!.text.toString().trim()
        val courseName = binding.courseName.editText!!.text.toString().trim()
        val bookingInfo = RoomBookingInfo(
            reservationDateTimestamp,
            pk,
            courseCode,
            courseName,
            binding.teacherDetail.editText!!.text.toString().trim(),
            binding.sections.spinner.selectedItem.toString(),
            binding.department.spinner.selectedItem.toString(),
            binding.shift.spinner.selectedItem.toString(),
        )
        findNavController().previousBackStackEntry?.savedStateHandle?.set("data", bookingInfo.fromPOJOToJSON())
        dismiss()
    }

    private fun initializeProperties() {
        binding.presenter = this

        binding.toolbar.title = "Book Room"
        binding.toolbar.setNavigationOnClickListener { dismiss() }

        binding.room.title.text = "Room"
        binding.reservationDate.title.text = "Reservation Date"
        binding.time.title.text = "Time"

        binding.reservationDate.description.text = reservationDate
        binding.room.description.text = room
        binding.time.description.text = time

    }

    @Suppress("CAST_NEVER_SUCCEEDS")
    private fun initializeSpinners() {
        binding.sections.title.text = "Section"
        binding.sections.spinner.adapter = ArrayAdapter(
            requireContext(),
            R.layout.spinner_item,
            arrayOf("Undefined", *SectionSharedPrefHelper.getSections(requireContext()).toTypedArray())
        ).apply { setDropDownViewResource(R.layout.spinner_dropdown_item) }

        binding.shift.title.text = "Shift"
        binding.shift.spinner.adapter = ArrayAdapter(
            requireContext(),
            R.layout.spinner_item,
            HelperClass.getShifts()
        ).apply { setDropDownViewResource(R.layout.spinner_dropdown_item) }

        binding.department.title.text = "Department"
        binding.department.spinner.adapter = ArrayAdapter(
            requireContext(),
            R.layout.spinner_item,
            HelperClass.getDepartments()
        ).apply { setDropDownViewResource(R.layout.spinner_dropdown_item) }

    }

}