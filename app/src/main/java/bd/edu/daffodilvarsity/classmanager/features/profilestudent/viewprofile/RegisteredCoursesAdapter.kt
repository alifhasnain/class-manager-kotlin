package bd.edu.daffodilvarsity.classmanager.features.profilestudent.viewprofile

import bd.edu.daffodilvarsity.classmanager.R
import bd.edu.daffodilvarsity.classmanager.common.adapter.SingleItemBaseAdapter
import bd.edu.daffodilvarsity.classmanager.common.models.RegisteredCourse

class RegisteredCoursesAdapter(private val courses: MutableList<RegisteredCourse>): SingleItemBaseAdapter<RegisteredCourse>() {

    var onItemClick: ((RegisteredCourse) -> Unit)? = null

    fun setItems(data: MutableList<RegisteredCourse>) {
        courses.clear()
        courses.addAll(data)
        notifyDataSetChanged()
    }

    override fun getItemForPosition(position: Int): RegisteredCourse {
        return courses[position]
    }

    override fun getLayoutIdForPosition(position: Int): Int {
        return R.layout.list_item_registered_courses
    }

    override fun getItemCount(): Int {
        return courses.size
    }

    override fun clickedItem(item: RegisteredCourse) {
        onItemClick?.invoke(item)
    }

}