package bd.edu.daffodilvarsity.classmanager.features.admin.parseroutine

import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.*
import bd.edu.daffodilvarsity.classmanager.common.extensions.fromListToJSON
import bd.edu.daffodilvarsity.classmanager.common.extensions.networkErrorMessage
import bd.edu.daffodilvarsity.classmanager.common.extensions.setValueThenNullify
import bd.edu.daffodilvarsity.classmanager.common.models.ClassDetails
import bd.edu.daffodilvarsity.classmanager.common.repository.Repository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.io.File

class ParseRoutineViewModel @ViewModelInject constructor(
    private val repo: Repository,
    @Assisted private val savedStateHandle: SavedStateHandle
): ViewModel() {

    private val _progressBarVisibility = MutableLiveData(false)
    val progressBarVisibility: LiveData<Boolean> = _progressBarVisibility

    private val _parseEnabled = MutableLiveData(true)
    val parseEnabled: LiveData<Boolean> = _parseEnabled

    private val _toast = MutableLiveData<String>()
    val toast: LiveData<String> = _toast

    private val _classes = MutableLiveData<MutableList<ClassDetails>>()
    val classes: LiveData<MutableList<ClassDetails>> = _classes

    fun parseRoutine(department: String , campus: String, shift: String, file: File?) {
        if (file == null) {
            _toast.setValueThenNullify("No file is selected")
            return
        }
        safeScope {
            if (department == "Computer Science & Engineering" && campus == "Main Campus" && shift == "Day") {
                _classes.value = ParseHelper.readRoutineCSEDayMC(file, department, campus, shift)
            } else if (department == "Computer Science & Engineering" && campus == "Main Campus" && shift == "Evening") {
                _classes.value = ParseHelper.readRoutineCSEEveMC(file, department, campus, shift)
            }
        }
    }

    fun uploadRoutine(campus: String ,department: String, shift: String) {
        safeScope {
            val classesJson = getJsonClassList()
            val dataToSend = mutableMapOf<String,String>()
            dataToSend["campus"] = campus
            dataToSend["department"] = department
            dataToSend["shift"] = shift
            dataToSend["classes"] = classesJson
            val responseMessage = repo.uploadRoutine(dataToSend)
            _toast.setValueThenNullify(responseMessage.message)
        }
    }

    private suspend fun getJsonClassList(): String = withContext(Dispatchers.Default) {
        classes.value?.fromListToJSON() ?: ""
    }


    private fun safeScope(block: (suspend ()-> Unit)) {
        viewModelScope.launch {
            try {
                _progressBarVisibility.value = true
                _parseEnabled.value = false
                block()
            } catch (e: Exception) {
                _toast.setValueThenNullify(e.networkErrorMessage)
            } finally {
                _progressBarVisibility.value = false
                _parseEnabled.value = true
            }
        }
    }
}