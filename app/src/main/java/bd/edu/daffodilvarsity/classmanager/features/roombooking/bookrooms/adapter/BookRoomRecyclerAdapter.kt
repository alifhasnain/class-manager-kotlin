package bd.edu.daffodilvarsity.classmanager.features.roombooking.bookrooms.adapter

import bd.edu.daffodilvarsity.classmanager.R
import bd.edu.daffodilvarsity.classmanager.common.adapter.SingleItemBaseAdapter

class BookRoomRecyclerAdapter: SingleItemBaseAdapter<Pair<Long,String>>() {

    private val rooms: MutableList<Pair<Long,String>> = mutableListOf()

    var onBookClickListener: ((Pair<Long, String>) -> Unit)? = null

    fun updateItems(rooms: List<Pair<Long,String>>) {
        this.rooms.clear()
        this.rooms.addAll(rooms)
        notifyDataSetChanged()
    }

    override fun clickedItem(item: Pair<Long, String>) {
        onBookClickListener?.invoke(item)
    }

    override fun getItemForPosition(position: Int): Pair<Long, String> {
        return rooms[position]
    }

    override fun getLayoutIdForPosition(position: Int): Int {
        return R.layout.list_item_book_room
    }

    override fun getItemCount(): Int {
        return rooms.size
    }

}