package bd.edu.daffodilvarsity.classmanager.features.admin.bookedrooms.common

import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.*
import bd.edu.daffodilvarsity.classmanager.common.extensions.networkErrorMessage
import bd.edu.daffodilvarsity.classmanager.common.extensions.setValueThenNullify
import bd.edu.daffodilvarsity.classmanager.common.models.BookedRoom
import bd.edu.daffodilvarsity.classmanager.common.models.LoadStates
import bd.edu.daffodilvarsity.classmanager.common.repository.Repository
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.launch
import kotlinx.coroutines.supervisorScope

class BookedRoomsAdminViewModel@ViewModelInject constructor(
    private val repo: Repository,
    @Assisted private val savedStateHandle: SavedStateHandle
): ViewModel() {

    private val _uiStates: MutableLiveData<LoadStates> = MutableLiveData(LoadStates.Empty("Nothing found"))
    val uiStates: LiveData<LoadStates> = _uiStates

    private val _bookedRoomsInSingleDate: MutableLiveData<List<BookedRoom>> = MutableLiveData()
    val bookedRoomsInSingleDate: LiveData<List<BookedRoom>> = _bookedRoomsInSingleDate

    private val _bookedRoomsInRangedDate: MutableLiveData<List<BookedRoom>> = MutableLiveData()
    val bookedRoomsInRangedDate: LiveData<List<BookedRoom>> = _bookedRoomsInRangedDate

    val toast: MutableLiveData<String> = MutableLiveData()

    fun fetchBookedRoomsWithDate(timestamp: Long) = safeScope {
        _bookedRoomsInSingleDate.value = repo.getBookedRoomsSingleDate(timestamp)
        _uiStates.value = LoadStates.decideEmptyOrNot(_bookedRoomsInSingleDate.value,"Nothing found")
    }

    fun fetchBookedRoomsWithRange(startDate: Long, endDate: Long) = safeScope {
        _bookedRoomsInRangedDate.value = repo.getBookedRoomsRangedDate(startDate,endDate)
        _uiStates.value = LoadStates.decideEmptyOrNot(_bookedRoomsInRangedDate.value,"Nothing found")
    }

    suspend fun fetchAndReturnBookedRoomsWithRange(startDate: Long, endDate: Long): List<BookedRoom> {
        return try {
            _uiStates.value = LoadStates.Loading
            val bookedRooms = repo.getBookedRoomsRangedDate(startDate, endDate)
            _bookedRoomsInRangedDate.value = bookedRooms
            _uiStates.value = LoadStates.decideEmptyOrNot(bookedRooms,"Nothing found")
            bookedRooms
        } catch (e: Exception) {
            _uiStates.value = LoadStates.Error()
            toast.value = e.networkErrorMessage
            emptyList()
        }
    }

    fun cancelBook(id: Long,mailTitle: String, mailBody: String, timestamp: Long)  = safeScope {
        val handler = CoroutineExceptionHandler { _,e ->
            toast.setValueThenNullify((e as Exception).networkErrorMessage)
        }
        supervisorScope {

            launch(handler) {
                repo.cancelRoomBookAdmin(id,mailTitle,mailBody)
                toast.setValueThenNullify("Cancellation success")
            }.join()

            launch(handler) {
                _bookedRoomsInSingleDate.value = repo.getBookedRoomsSingleDate(timestamp)
                _uiStates.value = LoadStates.decideEmptyOrNot(_bookedRoomsInSingleDate.value,"Nothing found")
            }

        }
    }

    private fun safeScope(block: (suspend () -> Unit)) = viewModelScope.launch {
        try {
            _uiStates.value = LoadStates.Loading
            block()
        } catch (e: Exception) {
            toast.value = e.networkErrorMessage
            _uiStates.value = LoadStates.Error("Error while loading")
        }
    }
}