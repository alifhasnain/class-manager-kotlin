package bd.edu.daffodilvarsity.classmanager.features.emptyrooms

import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.*
import bd.edu.daffodilvarsity.classmanager.common.models.ClassDetails
import bd.edu.daffodilvarsity.classmanager.common.repository.Repository
import bd.edu.daffodilvarsity.classmanager.utils.HelperClass
import kotlinx.coroutines.launch

class EmptyRoomsViewModel @ViewModelInject constructor (
    private val repo: Repository,
    @Assisted private val savedStateHandle: SavedStateHandle
): ViewModel() {

    private val _classes: MutableLiveData<MutableList<ClassDetails>> = MutableLiveData()
    val classes: LiveData<MutableList<ClassDetails>> = _classes

    fun searchEmptyRooms(day: String, time: String, campus: String) {
        viewModelScope.launch {
            val (startTime,endTime) = HelperClass.getSecondPair(time)
            _classes.value = repo.getEmptyRooms(startTime,endTime,campus,day)
        }
    }
}