package bd.edu.daffodilvarsity.classmanager.common.repository

import androidx.lifecycle.LiveData
import androidx.paging.PagingData
import androidx.sqlite.db.SimpleSQLiteQuery
import bd.edu.daffodilvarsity.classmanager.common.models.*
import kotlinx.coroutines.flow.Flow
import okhttp3.ResponseBody

interface Repository {

    /*
    * Functions related with USER (Teacher/Student)
    * */

    // Common

    suspend fun getCourseListFromServer(level: String, term: String, shift: String): MutableList<Course>

    suspend fun updateFCMToken(token: String): ResponseMessage

    suspend fun getAllSectionListFromServer(): MutableList<Pair<String, String>>
    /*
    * For paginating teacher
    * profile list
    * */
    fun getTeacherProfileStream(
        queryString: String,
        limit: Int
    ): Flow<PagingData<ProfileTeacher>>

    // Teachers

    suspend fun completeTeacherProfile(profile: ProfileTeacher): ResponseMessage

    suspend fun fetchTeacherProfileFromServer(): ProfileTeacher

    suspend fun saveTeacherProfileToServer(profile: ProfileTeacher): ProfileTeacher

    suspend fun getCRDetailsList(level: String, term: String, shift: String, campus: String): List<CRProfile>

    // Students

    suspend fun completeStudentProfile(profile: ProfileStudent): ResponseMessage

    suspend fun fetchStudentProfileFromServer(): ProfileStudent

    suspend fun saveStudentProfileToServer(profile: ProfileStudent): ProfileStudent

    suspend fun saveProfileAndUpdateRegisteredCourse(profile: ProfileStudent): Pair<ProfileStudent,MutableList<RegisteredCourse>>

    suspend fun fetchRegisteredCourses(): MutableList<RegisteredCourse>

    suspend fun addRegisteredCourse(course: RegisteredCourse): MutableList<RegisteredCourse>

    suspend fun removeRegisteredCourse(course: RegisteredCourse): MutableList<RegisteredCourse>


    /*
    * Function related to Routine
    * */

    suspend fun getCoursesFromLocal(
        level: Int,
        term: Int,
        shift: String,
        department: String
    ): MutableList<Course>

    fun getCoursesFromLocalAsLiveData(
        shift: String,
        department: String
    ): LiveData<List<Course>>

    fun getDistinctSectionsFromLocal(): LiveData<List<String>>

    suspend fun getConcatenateCourseCodeAndCourseName(
        shift: String,
        department: String
    ): List<String>

    suspend fun updateRoutineAndCourseFromDatabase()

    suspend fun refreshAllCoursesFromServer()

    suspend fun fetchAllSectionFromServer(): MutableList<String>

    fun getTeacherRoutineForSpecificDayAsFlow(initial: String, day: String): Flow<MutableList<ClassDetails>>

    fun getTeacherWholeRoutineAsFlow(initial: String): Flow<List<ClassDetails>>

    suspend fun getTeacherRoutine(
        initial: String,
        day: String = "All"
    ): MutableList<ClassDetails>

    fun getStudentRoutineAsFlow(rawQuery: SimpleSQLiteQuery): Flow<MutableList<ClassDetails>>

    suspend fun getStudentRoutineDayOfWeekSorted(
        courseList: MutableList<String>,
        section: String,
        shift: String,
        campus: String,
        department: String
    ): MutableList<ClassDetails>

    suspend fun getRoutineWithCourseCode(
        courseCode: String,
        shift: String,
        campus: String,
        department: String
    ): MutableList<ClassDetails>

    suspend fun getCourseVersion(): String

    suspend fun getSectionVersion(): String

    suspend fun getRoutineVersion(): String

    suspend fun getAppVersion(): String

    suspend fun getEmptyRooms(
        startTime: Int,
        endTime: Int,
        campus: String,
        day: String
    ): MutableList<ClassDetails>

    suspend fun toggleNotification(id: Long, notification: Boolean)

    /*
    * Function related to Notification (For Students)
    * */

    suspend fun deleteNotificationsAndBookedRooms()

    suspend fun insertNotification(notification: NotificationModel)

    fun getNotifications(): LiveData<List<NotificationModel>>

    fun notificationCount(): LiveData<Long>

    suspend fun updateReadStatus(pk: Long, read: Boolean)

    suspend fun markAllNotificationAsRead()

    suspend fun clearAllNotifications()

    /*
    * Functions Related to Room Booking
    * */

    suspend fun getAvailableRoomForBook(
        startTime: Int,
        endTime: Int,
        campus: String,
        reservationTimeStamp: Long
    ): List<Pair<Long,String>>

    suspend fun bookRoomTeacher(roomBookingInfo: RoomBookingInfo): ResponseMessage

    suspend fun bookRoomAdmin(bookingInfo: RoomBookingInfo): ResponseMessage

    suspend fun cancelRoomBookTeacher(bookID: String): ResponseMessage

    fun fetchTeacherBookedClassesFromCache(): LiveData<List<BookedRoom>>

    suspend fun refreshCachedBookedRoomsFromServer()

    /*
    * Function that to Admin can perform
    * */

    suspend fun uploadRoutine(routineData: MutableMap<String, String>): ResponseMessage

    suspend fun fetchRoutine(startTime: Int, endTime: Int, campus: String, day: String): List<ClassDetails>

    suspend fun addSingleClass(classDetails: ClassDetails): ResponseMessage

    suspend fun updateSingleClass(classDetails: ClassDetails): ResponseMessage

    suspend fun deleteSingleClass(id: String): ResponseMessage

    suspend fun getBookedRoomsSingleDate(dateTimestamp: Long): List<BookedRoom>

    suspend fun getBookedRoomsRangedDate(startDate: Long, endDate: Long): List<BookedRoom>

    suspend fun cancelRoomBookAdmin(id: Long, mailTitle: String, mailBody: String)

    suspend fun addCRProfile(profile: CRProfile): ResponseMessage

    suspend fun updateCRProfile(profile: CRProfile): ResponseMessage

    suspend fun deleteCRProfile(pk: String): ResponseMessage

    suspend fun deleteCourse(id: Long): ResponseMessage

    suspend fun addCourse(course: Course): ResponseMessage

    suspend fun updateCourse(course: Course): ResponseMessage

    suspend fun addSection(section: String): ResponseMessage

    suspend fun deleteSection(id: String): ResponseMessage

    suspend fun updateRoutineVersion(path: String): ResponseBody

    suspend fun updateAppVersion(path: String): ResponseBody

    suspend fun updateCourseVersion(path: String): ResponseBody

    suspend fun updateSectionVersion(path: String): ResponseBody

    suspend fun resetBookCount(): ResponseMessage

}