package bd.edu.daffodilvarsity.classmanager.features.completeprofile.common

import android.view.View
import android.widget.EditText
import ernestoyaquello.com.verticalstepperform.Step
/*
* This is an Uneditable step
* which is only used to show pre-defined
* use data
*/
class UneditableStep(stepName : String, val data : String) : Step<String>(stepName) {

    private val userNameEditText : EditText by lazy {
        EditText(context).also {
            it.isSingleLine = true
            it.setText(data)
            it.isEnabled = false
        }
    }

    override fun createStepContentLayout(): View {
        return userNameEditText
    }

    override fun restoreStepData(data: String?) {
        userNameEditText.setText(data)
    }

    override fun isStepDataValid(stepData: String?): IsDataValid {
        return IsDataValid(true)
    }

    override fun getStepDataAsHumanReadableString(): String {
        return userNameEditText.text.toString()
    }

    override fun getStepData(): String {
        return userNameEditText.text.toString()
    }

    override fun onStepOpened(animated: Boolean) {
        // This will be called automatically whenever the step gets opened.
    }

    override fun onStepClosed(animated: Boolean) {
        // This will be called automatically whenever the step gets closed.
    }

    override fun onStepMarkedAsCompleted(animated: Boolean) {
        // This will be called automatically whenever the step is marked as completed.
    }

    override fun onStepMarkedAsUncompleted(animated: Boolean) {
        // This will be called automatically whenever the step is marked as uncompleted.
    }


}