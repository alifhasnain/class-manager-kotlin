package bd.edu.daffodilvarsity.classmanager.features.customsearch.viewmodel

import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.*
import bd.edu.daffodilvarsity.classmanager.common.models.ClassDetails
import bd.edu.daffodilvarsity.classmanager.common.repository.Repository
import bd.edu.daffodilvarsity.classmanager.utils.HelperClass
import kotlinx.coroutines.launch
import timber.log.Timber
import java.util.*

class CustomRoutineSearchViewModel @ViewModelInject constructor (
    private val repo: Repository,
    @Assisted private val savedStateHandle: SavedStateHandle
): ViewModel() {

    private val _classes: MutableLiveData<MutableList<ClassDetails>> = MutableLiveData()
    val classes: LiveData<MutableList<ClassDetails>> = _classes

    fun loadClassesWithInitial(initial: String, day: String) = safeScope {
        _classes.value = repo.getTeacherRoutine(initial.toUpperCase(Locale.ROOT), day)
    }

    fun loadStudentRoutine(level: String, term: String,shift: String, section: String, campus: String) = safeScope {
        val updatedLevel = level.split(" ")[1].toInt()
        val updatedTerm = term.split(" ")[1].toInt()
        val courses = repo.getCoursesFromLocal(updatedLevel,updatedTerm,shift,HelperClass.CSE_DEPARTMENT)
        _classes.value = repo.getStudentRoutineDayOfWeekSorted(
            courses.map { it.courseCode } as MutableList<String>,
            section,
            shift,campus,HelperClass.CSE_DEPARTMENT
        )
    }

    fun loadRoutineWithCourseCode(courseCodeWithCourseName: String, shift: String, campus: String) = safeScope {
        val courseCode = courseCodeWithCourseName.split(":")[0].trim()
        _classes.value = repo.getRoutineWithCourseCode(courseCode, shift, campus, HelperClass.CSE_DEPARTMENT)
    }


    suspend fun getCourseCodeAndName(shift: String, department: String = "Computer Science & Engineering"): List<String> {
        return repo.getConcatenateCourseCodeAndCourseName(shift,department)
    }

    private fun safeScope(block: (suspend () -> Unit)) = viewModelScope.launch {
        try {
            block()
        } catch (e: Exception) {
            Timber.e(e)
        }
    }


}