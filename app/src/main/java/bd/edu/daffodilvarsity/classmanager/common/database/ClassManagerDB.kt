package bd.edu.daffodilvarsity.classmanager.common.database

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import bd.edu.daffodilvarsity.classmanager.common.models.BookedRoom
import bd.edu.daffodilvarsity.classmanager.common.models.ClassDetails
import bd.edu.daffodilvarsity.classmanager.common.models.Course
import bd.edu.daffodilvarsity.classmanager.common.models.NotificationModel

@Database(entities = [Course::class,ClassDetails::class,BookedRoom::class,NotificationModel::class], version = 9, exportSchema = false)
@TypeConverters(DateTypeConverter::class)
abstract class ClassManagerDB : RoomDatabase() {

    abstract fun classManagerDao(): ClassManagerDao

    companion object {

        const val DB_NAME = "class-manager-db"

        /*
        * Removed this singleton implementation
        * after adding dependency injection with hilt
        * */

/*        @Volatile
        private var INSTANCE: ClassManagerDB? = null

        fun getInstance(context: Context): ClassManagerDB {
            return INSTANCE ?: synchronized(this) {
                INSTANCE ?: createDatabase(context).also { INSTANCE = it }
            }
        }

        private fun createDatabase(context: Context): ClassManagerDB {
            return Room.databaseBuilder(
                context,
                ClassManagerDB::class.java,
                DB_NAME
            ).fallbackToDestructiveMigration().build()
        }*/
    }

}