package bd.edu.daffodilvarsity.classmanager.features.teacherprofilelist.ui

import android.content.DialogInterface
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.lifecycleScope
import androidx.paging.LoadState
import bd.edu.daffodilvarsity.classmanager.R
import bd.edu.daffodilvarsity.classmanager.common.extensions.makeToast
import bd.edu.daffodilvarsity.classmanager.common.extensions.openDialerWithNumber
import bd.edu.daffodilvarsity.classmanager.common.extensions.sendEmail
import bd.edu.daffodilvarsity.classmanager.common.loadstatefooter.FooterLoadStateAdapter
import bd.edu.daffodilvarsity.classmanager.common.models.LoadStates
import bd.edu.daffodilvarsity.classmanager.common.models.ViewBindingFragment
import bd.edu.daffodilvarsity.classmanager.databinding.FragmentTeacherProfileListBinding
import bd.edu.daffodilvarsity.classmanager.features.teacherprofilelist.adapter.TeacherProfileListRecyclerAdapter
import bd.edu.daffodilvarsity.classmanager.features.teacherprofilelist.viewmodel.TeacherProfileListViewModel
import com.google.android.material.transition.MaterialSharedAxis
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import timber.log.Timber

/**
 * A simple [Fragment] subclass.
 */
@AndroidEntryPoint
class TeacherProfileListFragment : ViewBindingFragment<FragmentTeacherProfileListBinding>(R.layout.fragment_teacher_profile_list, { FragmentTeacherProfileListBinding.bind(it) }) {

    val uiStates: MutableLiveData<LoadStates> = MutableLiveData(LoadStates.Empty("Nothing found"))

    private val viewModel: TeacherProfileListViewModel by viewModels()

    private val adapter by lazy { TeacherProfileListRecyclerAdapter() }

    private var teacherProfileFetchJob: Job? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        enterTransition = MaterialSharedAxis(MaterialSharedAxis.Z, true)
        exitTransition = MaterialSharedAxis(MaterialSharedAxis.Z, true)
        binding.presenter = this
        binding.lifecycleOwner = viewLifecycleOwner

        binding.queryText.editText!!.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                teacherProfileFetchJob?.cancel()
                teacherProfileFetchJob = lifecycleScope.launch {
                    delay(500)
                    viewModel.fetchTeacherProfiles(binding.queryText.editText!!.text.toString())?.collectLatest {
                        adapter.submitData(it)
                    }
                }
            }
            override fun afterTextChanged(p0: Editable?) {}
        })

        binding.stateError.retry.setOnClickListener { adapter.refresh() }

        initializeRecyclerView()

        fetchTeacherProfiles()
    }

    private fun fetchTeacherProfiles() {
        teacherProfileFetchJob?.cancel()
        teacherProfileFetchJob = lifecycleScope.launch {
            viewModel.fetchTeacherProfiles(binding.queryText.editText!!.text.toString())?.collectLatest {
                adapter.submitData(it)
            }
        }
    }

    private fun initializeRecyclerView() {

        binding.recyclerView.adapter = adapter.withLoadStateHeaderAndFooter(
            header = FooterLoadStateAdapter { adapter.retry() },
            footer = FooterLoadStateAdapter { adapter.retry() }
        )

        adapter.onLongClickListener = { profile ->
            AlertDialog.Builder(requireContext()).apply {
                setItems(arrayOf("Dial : ${profile.contactNo}", "Email : ${profile.email}"), DialogInterface.OnClickListener { _, position ->
                    if(position == 0) {
                        context.openDialerWithNumber(profile.contactNo)
                    } else {
                        context.sendEmail(arrayOf(profile.email), null, null)
                    }
                })
            }.create().show()
        }

        adapter.addLoadStateListener { loadState ->
            if (loadState.refresh !is LoadState.NotLoading) {
                // If we are refreshing data from server then we have to hide
                // the list and show progressbar or in case of an error we have
                // to show the retry button
                binding.recyclerView.visibility = View.GONE
                if (loadState.refresh is LoadState.Loading) {
                    Timber.e("Loading")
                }
                if (loadState.refresh is LoadState.Error) {
                    Timber.e("Error")
                }
                (if (loadState.refresh is LoadState.Loading) { LoadStates.Loading } else null)?.let { uiStates.value = it }
                (if (loadState.refresh is LoadState.Error) { LoadStates.Error() } else null)?.let { uiStates.value = it }
            } else {
                // If not loading data then show the list
                binding.recyclerView.visibility = View.VISIBLE
                Timber.e("Adapter item count : ${adapter.itemCount}")
                uiStates.value = if (adapter.itemCount <= 0) LoadStates.Empty("Nothing found") else {
                    LoadStates.NotEmpty
                }
            }

            val errorState = when {
                loadState.append is LoadState.Error -> loadState.append as LoadState.Error
                loadState.prepend is LoadState.Error -> loadState.append as LoadState.Error
                else -> null
            }
            errorState?.let { makeToast(errorState.error.message) }
        }
    }

}
