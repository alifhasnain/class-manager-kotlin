package bd.edu.daffodilvarsity.classmanager.utils.sharedprefutils

import android.content.Context
import android.content.SharedPreferences
import bd.edu.daffodilvarsity.classmanager.common.models.ProfileStudent
import bd.edu.daffodilvarsity.classmanager.common.models.ProfileTeacher
import bd.edu.daffodilvarsity.classmanager.common.models.RegisteredCourse
import com.squareup.moshi.JsonAdapter

import com.squareup.moshi.Moshi
import com.squareup.moshi.Types




object ProfileSharedPrefHelper {

    private const val PROFILE_SHARED_PREF_TAG = "profile-shared-pref"

    private const val PROFILE_STUDENT = "student-profile"

    private const val PROFILE_TEACHER = "teacher-profile"

    private const val REGISTERED_COURSES = "registered-course-list"

    fun getStudentProfile(context: Context): ProfileStudent {
        val profileJson = getProfileSharedPref(
            context
        )
            .getString(PROFILE_STUDENT, "")
        val jsonAdapter = Moshi.Builder().build().adapter(ProfileStudent::class.java)
        return jsonAdapter.fromJson(profileJson) ?: ProfileStudent("", "", "", "", "", "", "", "", "")
    }

    fun saveStudentProfile(context: Context, profile: ProfileStudent) {
        val jsonAdapter = Moshi.Builder().build().adapter(ProfileStudent::class.java)
        val jsonProfile = jsonAdapter.toJson(profile)
        getProfileSharedPref(
            context
        ).edit().apply {
            putString(PROFILE_STUDENT, jsonProfile)
        }.apply()
    }

    fun getTeacherProfile(context: Context): ProfileTeacher {
        val profileJson = getProfileSharedPref(context).getString(PROFILE_TEACHER, "")
        val jsonAdapter = Moshi.Builder().build().adapter(ProfileTeacher::class.java)
        return jsonAdapter.fromJson(profileJson) ?: ProfileTeacher("","","","","","","",0)
    }

    fun saveTeacherProfile(context: Context, profileTeacher: ProfileTeacher) {
        val jsonAdapter = Moshi.Builder().build().adapter(ProfileTeacher::class.java)
        val jsonProfile = jsonAdapter.toJson(profileTeacher)
        getProfileSharedPref(
            context
        ).edit().apply {
            putString(PROFILE_TEACHER, jsonProfile)
        }.apply()
    }

    fun saveRegisteredCourses(context: Context, courses : MutableList<RegisteredCourse>) {
        val type = Types.newParameterizedType(MutableList::class.java,RegisteredCourse::class.java)
        val jsonAdapter: JsonAdapter<MutableList<RegisteredCourse>> = Moshi.Builder().build().adapter(type)

        val jsonString = jsonAdapter.toJson(courses)

        getProfileSharedPref(
            context
        )
            .edit().putString(REGISTERED_COURSES,jsonString).apply()
    }

    fun getRegisteredCourses(context: Context) : MutableList<RegisteredCourse> {
        val jsonString = getProfileSharedPref(
            context
        )
            .getString(REGISTERED_COURSES,"")

        val type = Types.newParameterizedType(MutableList::class.java,RegisteredCourse::class.java)
        val jsonAdapter: JsonAdapter<MutableList<RegisteredCourse>> = Moshi.Builder().build().adapter(type)

        return jsonAdapter.fromJson(jsonString!!) ?: mutableListOf()
    }

    fun clearProfileSharedPref(context: Context) = getProfileSharedPref(context).edit().clear().apply()

    private fun getProfileSharedPref(context: Context): SharedPreferences {
        return context.getSharedPreferences(
            PROFILE_SHARED_PREF_TAG,
            Context.MODE_PRIVATE
        )
    }
}