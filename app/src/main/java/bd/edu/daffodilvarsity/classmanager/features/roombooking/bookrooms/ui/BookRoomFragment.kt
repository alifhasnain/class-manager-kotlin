package bd.edu.daffodilvarsity.classmanager.features.roombooking.bookrooms.ui

import android.annotation.SuppressLint
import android.app.DatePickerDialog
import android.os.Bundle
import android.view.View
import android.widget.ArrayAdapter
import android.widget.DatePicker
import androidx.core.os.bundleOf
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import bd.edu.daffodilvarsity.classmanager.R
import bd.edu.daffodilvarsity.classmanager.common.extensions.fromJSONToPOJO
import bd.edu.daffodilvarsity.classmanager.common.extensions.makeToast
import bd.edu.daffodilvarsity.classmanager.common.models.DataBindingFragment
import bd.edu.daffodilvarsity.classmanager.common.models.RoomBookingInfo
import bd.edu.daffodilvarsity.classmanager.databinding.FragmentBookRoomBinding
import bd.edu.daffodilvarsity.classmanager.features.roombooking.bookrooms.adapter.BookRoomRecyclerAdapter
import bd.edu.daffodilvarsity.classmanager.features.roombooking.bookrooms.viewmodel.BookRoomViewModel
import bd.edu.daffodilvarsity.classmanager.utils.HelperClass
import com.google.android.material.transition.MaterialSharedAxis
import dagger.hilt.android.AndroidEntryPoint
import java.text.SimpleDateFormat
import java.util.*

@AndroidEntryPoint
class BookRoomFragment : DataBindingFragment<FragmentBookRoomBinding>(R.layout.fragment_book_room), DatePickerDialog.OnDateSetListener {

    private lateinit var selectedDate: Calendar

    private lateinit var finalDate: Calendar

    @SuppressLint("SimpleDateFormat")
    private val dateFormatter = SimpleDateFormat("EEE, d MMM, yyyy")

    private val viewModel: BookRoomViewModel by viewModels()

    private val adapter by lazy { BookRoomRecyclerAdapter() }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        enterTransition = MaterialSharedAxis(MaterialSharedAxis.Z, true)
        exitTransition = MaterialSharedAxis(MaterialSharedAxis.Z, true)
        binding.presenter = this
        binding.viewmodel = viewModel

        setupPrimaryDate()
        initializeSpinners()
        initializeRecyclerView()
        initializeObservers()

    }

    override fun onDateSet(view: DatePicker?, year: Int, month: Int, dayOfMonth: Int) {
        selectedDate.set(year,month,dayOfMonth)
        binding.date.text = dateFormatter.format(selectedDate.time)
    }

    fun searchAvailableRooms(selectedTime: String,campus: String) {
        val (startTime,endTime) = HelperClass.getSecondPair(selectedTime)
        finalDate = selectedDate
        viewModel.loadAvailableRoomForBook(startTime,endTime,campus,finalDate.timeInMillis)
    }

    fun pickDate() {
        val minDate = Calendar.getInstance().apply { add(Calendar.DATE,1) }.timeInMillis
        val maxDate = Calendar.getInstance().apply { add(Calendar.DATE,7) }.timeInMillis

        val year = selectedDate.get(Calendar.YEAR)
        val month = selectedDate.get(Calendar.MONTH)
        val dayOfMonth = selectedDate.get(Calendar.DAY_OF_MONTH)

        DatePickerDialog(requireContext(),this,year, month, dayOfMonth).apply {
            datePicker.minDate = minDate
            datePicker.maxDate = maxDate
        }.show()

    }

    private fun bookRoom(pk: Long,room: String) {

        findNavController().navigate(
            R.id.action_bookRoomFragment_to_finishBookDialogFragment,
            bundleOf(
                "reservationDateTimestamp" to finalDate.timeInMillis,
                "pk" to pk.toString(),
                "reservationDate" to dateFormatter.format(finalDate.time),
                "room" to room,
                "time" to binding.time.selectedItem.toString()
            )
        )

    }

    private fun initializeRecyclerView() {
        binding.recyclerView.layoutManager = GridLayoutManager(requireContext(),2)
        binding.recyclerView.adapter = adapter
        adapter.onBookClickListener = { (pk,room) ->
            bookRoom(pk,room)
        }
    }

    private fun initializeObservers() {

        findNavController().getBackStackEntry(R.id.bookRoomFragment).savedStateHandle
            .getLiveData<String>("data")
            .observe(viewLifecycleOwner) {
                val bookingInfo = it.fromJSONToPOJO<RoomBookingInfo>()
                viewModel.bookRoom(bookingInfo)
                // remove data so it doesn't trigger twice
                findNavController().getBackStackEntry(R.id.bookRoomFragment).savedStateHandle.remove<RoomBookingInfo>("data")
            }

        viewModel.toast.observe(viewLifecycleOwner) { makeToast(it) }

        viewModel.availableRooms.observe(viewLifecycleOwner) { adapter.updateItems(it) }

        viewModel.bookSuccess.observe(viewLifecycleOwner) {
            it?.let { searchAvailableRooms(binding.time.selectedItem.toString(),binding.campus.selectedItem.toString()) }
        }
    }

    private fun setupPrimaryDate() {
        val tomorrow = Calendar.getInstance().apply { add(Calendar.DATE,1) }
        val year = tomorrow.get(Calendar.YEAR)
        val month = tomorrow.get(Calendar.MONTH)
        val dayOfMonth = tomorrow.get(Calendar.DAY_OF_MONTH)

        selectedDate = GregorianCalendar(year, month, dayOfMonth)
        finalDate = (selectedDate as GregorianCalendar).clone() as Calendar
        binding.date.text = dateFormatter.format(selectedDate.time)
    }

    private fun initializeSpinners() {
        binding.time.apply {
            adapter = ArrayAdapter(
                context,
                R.layout.spinner_item,
                HelperClass.getClassTimesMainCampus().keys.toMutableList()
            ).apply { setDropDownViewResource(R.layout.spinner_dropdown_item) }
        }
        binding.campus.apply {
            adapter = ArrayAdapter(
                context,
                R.layout.spinner_item,
                HelperClass.getCampuses()
            ).apply { setDropDownViewResource(R.layout.spinner_dropdown_item) }
        }
    }

}
