package bd.edu.daffodilvarsity.classmanager.features.customsearch.courscoderoutine

import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import bd.edu.daffodilvarsity.classmanager.R
import bd.edu.daffodilvarsity.classmanager.common.models.DataBindingFragment
import bd.edu.daffodilvarsity.classmanager.databinding.FragmentCourseCodeSearchBinding
import bd.edu.daffodilvarsity.classmanager.features.customsearch.adapters.ClassesRecyclerAdapter
import bd.edu.daffodilvarsity.classmanager.features.customsearch.viewmodel.CustomRoutineSearchViewModel
import bd.edu.daffodilvarsity.classmanager.utils.HelperClass
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch

/**
 * A simple [Fragment] subclass.
 */
@AndroidEntryPoint
class CourseCodeSearchFragment : DataBindingFragment<FragmentCourseCodeSearchBinding>(R.layout.fragment_course_code_search) {

    private val adapter = ClassesRecyclerAdapter()

    private val viewModel: CustomRoutineSearchViewModel by viewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.viewmodel = viewModel

        initializeSpinners()

        initializeRecyclerView()
        initializeObservers()
    }

    private fun initializeObservers() {
        viewModel.classes.observe(viewLifecycleOwner, { adapter.submitItems(it) })
    }

    private fun initializeRecyclerView() {
        binding.recyclerView.layoutManager = LinearLayoutManager(context)
        binding.recyclerView.adapter = adapter
        adapter.stateRestorationPolicy = RecyclerView.Adapter.StateRestorationPolicy.PREVENT_WHEN_EMPTY
    }

    private fun initializeSpinners() {

        binding.campus.apply {
            adapter = ArrayAdapter(
                context,
                R.layout.spinner_item,
                HelperClass.getCampuses()
            ).apply { setDropDownViewResource(R.layout.spinner_dropdown_item) }
        }

        binding.shift.apply {
            adapter = ArrayAdapter(
                context,
                R.layout.spinner_item,
                HelperClass.getShifts()
            ).apply { setDropDownViewResource(R.layout.spinner_dropdown_item) }
        }

        binding.courses.apply {
            adapter = ArrayAdapter(
                context,
                R.layout.spinner_item,
                mutableListOf<String>()
            ).apply { setDropDownViewResource(R.layout.spinner_dropdown_item) }
        }

        binding.shift.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {

            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                lifecycleScope.launch {
                    val courses = viewModel.getCourseCodeAndName(binding.shift.selectedItem.toString())
                    val adapter = binding.courses.adapter as ArrayAdapter<String>
                    adapter.clear()
                    adapter.addAll(courses)
                    adapter.notifyDataSetChanged()
                }
            }
        }
    }
}
