package bd.edu.daffodilvarsity.classmanager.features.roombooking.bookedrooms.ui

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import bd.edu.daffodilvarsity.classmanager.R
import bd.edu.daffodilvarsity.classmanager.common.extensions.makeToast
import bd.edu.daffodilvarsity.classmanager.common.models.DataBindingFragment
import bd.edu.daffodilvarsity.classmanager.databinding.FragmentBookedRoomsBinding
import bd.edu.daffodilvarsity.classmanager.features.roombooking.bookedrooms.adapter.BookedRoomsRecyclerAdapter
import bd.edu.daffodilvarsity.classmanager.features.roombooking.bookedrooms.viewmodel.BookedRoomsViewModel
import com.google.android.material.transition.MaterialSharedAxis
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class BookedRoomsFragment : DataBindingFragment<FragmentBookedRoomsBinding>(R.layout.fragment_booked_rooms) {

    private val viewModel: BookedRoomsViewModel by viewModels()

    private val adapter by lazy {
        BookedRoomsRecyclerAdapter()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        enterTransition = MaterialSharedAxis(MaterialSharedAxis.Z, true)
        exitTransition = MaterialSharedAxis(MaterialSharedAxis.Z, true)
        binding.viewmodel = viewModel

        initializeRecyclerView()
        initializeObservers()

    }

    private fun initializeRecyclerView() {
        binding.recyclerView.layoutManager = LinearLayoutManager(requireContext())
        binding.recyclerView.adapter = adapter
        adapter.onCancelClicked = {
            AlertDialog.Builder(requireContext()).apply {
                setTitle("Please confirm")
                setMessage("Are you sure you want to cancel this booking?")
                setPositiveButton("Yes") { _, _ -> viewModel.cancelBooking(it.id.toString()) }
                setNegativeButton("No",null)
            }.show()
        }
    }

    private fun initializeObservers() {
        viewModel.bookedRoom.observe(viewLifecycleOwner) { bookedClasses -> adapter.updateItems(bookedClasses) }
        viewModel.toast.observe(viewLifecycleOwner, { makeToast(it) })
    }
}
