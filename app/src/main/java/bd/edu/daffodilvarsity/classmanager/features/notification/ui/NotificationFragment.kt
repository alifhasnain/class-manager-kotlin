package bd.edu.daffodilvarsity.classmanager.features.notification.ui

import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import bd.edu.daffodilvarsity.classmanager.R
import bd.edu.daffodilvarsity.classmanager.common.extensions.makeToast
import bd.edu.daffodilvarsity.classmanager.common.models.DataBindingFragment
import bd.edu.daffodilvarsity.classmanager.common.models.LoadStates
import bd.edu.daffodilvarsity.classmanager.databinding.FragmentNotificationBinding
import bd.edu.daffodilvarsity.classmanager.features.notification.adapters.NotificationListRecyclerAdapter
import bd.edu.daffodilvarsity.classmanager.features.notification.viewmodel.NotificationViewModel
import com.google.android.material.transition.MaterialSharedAxis
import dagger.hilt.android.AndroidEntryPoint

/**
 * A simple [Fragment] subclass.
 */
@AndroidEntryPoint
class NotificationFragment : DataBindingFragment<FragmentNotificationBinding>(R.layout.fragment_notification) {

    private val adapter by lazy { NotificationListRecyclerAdapter() }

    private val viewModel by viewModels<NotificationViewModel>()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setHasOptionsMenu(true)
        enterTransition = MaterialSharedAxis(MaterialSharedAxis.Z, true)
        exitTransition = MaterialSharedAxis(MaterialSharedAxis.Z, true)
        initializeProperties()
        initializeObservers()
        initializeRecyclerView()
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        menu.add("Mark all as read")
        menu.add("Clear All")
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.title == "Mark all as read") {
            viewModel.markAllAsRead()
        } else if (item.title == "Clear All") {
            viewModel.clearAllNotification()
        }
        return super.onOptionsItemSelected(item)
    }

    private fun initializeRecyclerView() {
        binding.recyclerView.adapter = adapter
        adapter.onCheckChanged = { pk,checked ->
            viewModel.updateCheckStatus(pk, checked)
        }
    }

    private fun initializeProperties() {
        binding.viewModel = viewModel
    }

    private fun initializeObservers() {

        viewModel.toast.observe(viewLifecycleOwner) { makeToast(it.content) }

        viewModel.notifications.observe(viewLifecycleOwner) { notifications ->
            adapter.submitItems(notifications)
            if (notifications.isNotEmpty()) {
                viewModel.uiState.value = LoadStates.NotEmpty
            } else {
                viewModel.uiState.value = LoadStates.Empty("Nothing found")
            }
        }
    }

}
