package bd.edu.daffodilvarsity.classmanager.features.completeprofile.teacher

import android.text.Editable
import android.text.InputType
import android.text.TextWatcher
import android.view.View
import android.widget.EditText
import ernestoyaquello.com.verticalstepperform.Step

/*
* This is a customizable step
* which is used for taking
* teacher initial & teacher's phone number
* as input
*/
class CustomizableStep(private val stepTitle: String) : Step<String>(stepTitle) {

    private val input by lazy {
        EditText(context).apply {
            isSingleLine = true
            hint = stepTitle
            if (stepTitle == "Initial") {
                inputType = InputType.TYPE_CLASS_TEXT
            } else if (stepTitle == "Contact No") {
                inputType = InputType.TYPE_CLASS_PHONE
            }
        }
    }

    override fun createStepContentLayout(): View {

        input.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                markAsCompletedOrUncompleted(true)
            }

            override fun afterTextChanged(p0: Editable?) {

            }
        })
        return input
    }

    override fun getStepData(): String {
        return input.text.toString()
    }

    override fun getStepDataAsHumanReadableString(): String {
        return input.text.toString()
    }

    override fun isStepDataValid(stepData: String): IsDataValid {
        return if (stepTitle == "Initial") {
            if (stepData.length < 15 && stepData.isNotEmpty()) {
                IsDataValid(true)
            } else if(stepData.isEmpty()){
                IsDataValid(false,"Initial can't be empty")
            } else {
                IsDataValid(false,"Initial can't be that large")
            }
        } else {
            if (stepData.isNotEmpty()) {
                IsDataValid(true)
            } else {
                IsDataValid(false,"Contact can't be empty")
            }
        }
    }

    override fun onStepMarkedAsCompleted(animated: Boolean) {

    }

    override fun onStepMarkedAsUncompleted(animated: Boolean) {

    }

    override fun onStepOpened(animated: Boolean) {

    }

    override fun onStepClosed(animated: Boolean) {

    }

    override fun restoreStepData(data: String?) {
        input.setText(data)
    }

}