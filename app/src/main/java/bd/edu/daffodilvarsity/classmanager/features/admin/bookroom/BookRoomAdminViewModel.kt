package bd.edu.daffodilvarsity.classmanager.features.admin.bookroom

import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.*
import bd.edu.daffodilvarsity.classmanager.common.extensions.networkErrorMessage
import bd.edu.daffodilvarsity.classmanager.common.extensions.setValueThenNullify
import bd.edu.daffodilvarsity.classmanager.common.models.LoadStates
import bd.edu.daffodilvarsity.classmanager.common.models.Quadruple
import bd.edu.daffodilvarsity.classmanager.common.models.RoomBookingInfo
import bd.edu.daffodilvarsity.classmanager.common.repository.Repository
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.launch
import kotlinx.coroutines.supervisorScope

class BookRoomAdminViewModel @ViewModelInject constructor(
    private val repo: Repository,
    @Assisted private val savedStateHandle: SavedStateHandle
): ViewModel() {

    private val _availableRooms = MutableLiveData<List<Pair<Long,String>>>()
    val availableRoom: LiveData<List<Pair<Long,String>>> = _availableRooms

    private val _uiState = MutableLiveData<LoadStates>(LoadStates.Empty("Nothing Found"))
    val uiState: LiveData<LoadStates> = _uiState

    var cachedQueryData: Quadruple<Int,Int,String,Long>? = null

    val toast = MutableLiveData<String>()

    fun fetchAvailableRooms(startTime: Int, endTime: Int, campus: String, reservationTimeStamp: Long) = safeScope {
        _availableRooms.value = repo.getAvailableRoomForBook(startTime, endTime, campus, reservationTimeStamp)
        _uiState.value = LoadStates.decideEmptyOrNot(availableRoom.value, "Nothing found")
        cachedQueryData = Quadruple(startTime,endTime,campus,reservationTimeStamp)
    }

    fun bookRoom(roomBookingInfo: RoomBookingInfo) = safeScope {
        val handler = CoroutineExceptionHandler { _, throwable ->
            toast.setValueThenNullify((throwable as Exception).networkErrorMessage)
        }
        supervisorScope {
            launch(handler) {
                val response = repo.bookRoomTeacher(roomBookingInfo)
                toast.setValueThenNullify(response.message)
            }.join()
            cachedQueryData?.let {
                _availableRooms.value = repo.getAvailableRoomForBook(it.first, it.second, it.third, it.fourth)
                _uiState.value = LoadStates.decideEmptyOrNot(availableRoom.value, "Nothing found")
            }
        }
    }

    private fun safeScope(block: (suspend () -> Unit)) = viewModelScope.launch {
        try {
            _uiState.value = LoadStates.Loading
            block()
        } catch (e: Exception) {
            _uiState.value = LoadStates.Error("Error while loading")
            toast.setValueThenNullify(e.networkErrorMessage)
        }
    }

}