package bd.edu.daffodilvarsity.classmanager.features.admin.crlist

import bd.edu.daffodilvarsity.classmanager.R
import bd.edu.daffodilvarsity.classmanager.common.adapter.SingleItemBaseAdapter
import bd.edu.daffodilvarsity.classmanager.common.models.CRProfile

class UpdateCRDetailsRecyclerAdapter: SingleItemBaseAdapter<CRProfile>() {

    private val crProfiles = mutableListOf<CRProfile>()

    var longClickListener: ((CRProfile) -> Unit)? = null

    var sendMailClickListener: ((String) -> Unit)? = null

    var dialClickListener: ((String) -> Unit)? = null

    fun submitItems(updatedProfiles: List<CRProfile>) {
        crProfiles.clear()
        crProfiles.addAll(updatedProfiles)
        notifyDataSetChanged()
    }

    fun onLongClick(data: CRProfile): Boolean {
        longClickListener?.invoke(data)
        return true
    }

    fun onSendEmailClick(email: String) = sendMailClickListener?.invoke(email)

    fun onDialClicked(phoneNo: String) = dialClickListener?.invoke(phoneNo)

    override fun clickedItem(item: CRProfile) {

    }

    override fun getItemForPosition(position: Int): CRProfile = crProfiles[position]

    override fun getLayoutIdForPosition(position: Int): Int = R.layout.list_item_update_cr_details

    override fun getItemCount(): Int = crProfiles.size

}