package bd.edu.daffodilvarsity.classmanager.common.models

import com.squareup.moshi.JsonClass


@JsonClass(generateAdapter = true)
data class ProfileStudent
(
    val id: String,
    val name: String,
    val email: String,
    val section: String,
    val level: String,
    val term: String,
    val department: String,
    val campus: String,
    val shift: String
)