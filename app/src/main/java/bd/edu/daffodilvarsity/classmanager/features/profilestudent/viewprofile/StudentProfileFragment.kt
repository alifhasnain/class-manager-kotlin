package bd.edu.daffodilvarsity.classmanager.features.profilestudent.viewprofile

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import bd.edu.daffodilvarsity.classmanager.R
import bd.edu.daffodilvarsity.classmanager.common.extensions.makeToast
import bd.edu.daffodilvarsity.classmanager.common.models.DataBindingFragment
import bd.edu.daffodilvarsity.classmanager.common.models.ProfileStudent
import bd.edu.daffodilvarsity.classmanager.databinding.FragmentStudentProfileBinding
import bd.edu.daffodilvarsity.classmanager.features.routine.viewmodels.RoutineSharedViewModel
import bd.edu.daffodilvarsity.classmanager.utils.sharedprefutils.ProfileSharedPrefHelper
import com.google.android.material.transition.MaterialSharedAxis
import dagger.hilt.android.AndroidEntryPoint

/**
 * [StudentProfileFragment]
 */
@AndroidEntryPoint
class StudentProfileFragment : DataBindingFragment<FragmentStudentProfileBinding>(R.layout.fragment_student_profile) {

    private val courses = mutableListOf<String>()
    private val sections = mutableListOf<String>()

    private val sharedViewModel: RoutineSharedViewModel by activityViewModels()
    private val viewModel: StudentProfileViewModel by viewModels()

    private val adapter by lazy {
        RegisteredCoursesAdapter(mutableListOf())
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        enterTransition = MaterialSharedAxis(MaterialSharedAxis.Z, true)
        exitTransition = MaterialSharedAxis(MaterialSharedAxis.Z, true)
        setHasOptionsMenu(true)

        initializeViewTitles()

        binding.viewmodel = viewModel
        binding.presenter = this

        viewModel.initProfileAndCourses(
            ProfileSharedPrefHelper.getStudentProfile(requireContext()),
            ProfileSharedPrefHelper.getRegisteredCourses(requireContext())
        )

        initializeRecyclerView()
        initializeViewModelObservers()
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.fragment_student_profile, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if(item.itemId == R.id.edit_profile) {
            view?.findNavController()?.navigate(R.id.studentProfileFragment_to_editProfileStudent)
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        binding.courseListRecyclerView.adapter = null
    }

    fun addNewCourse() {
        val dialog = CourseAndSectionSelectorDialog(courses,sections)
        dialog.onClickListener = { course -> viewModel.addCourse(course) }
        dialog.show(childFragmentManager,"COURSE_SECTION_SELECTOR")
    }

    private fun initializeRecyclerView() {
        binding.courseListRecyclerView.layoutManager = LinearLayoutManager(requireContext())
        binding.courseListRecyclerView.adapter = adapter

        adapter.onItemClick = { course->
            viewModel.removeCourse(course)
        }
    }

    private fun initializeViewModelObservers() {
        viewModel.toast.observe(viewLifecycleOwner, {
            makeToast(it)
        })

        viewModel.profileStudent.observe(viewLifecycleOwner, { profile ->
            ProfileSharedPrefHelper.saveStudentProfile(requireContext(),profile)
            val courses = ProfileSharedPrefHelper.getRegisteredCourses(requireContext())
            sharedViewModel.loadStudentClasses(courses, profile.shift, profile.campus, profile.department)
            initializeViewDescriptions(profile)
        })

        viewModel.registeredCourses.observe(viewLifecycleOwner, {
            ProfileSharedPrefHelper.saveRegisteredCourses(requireContext(),it)
            val profile = ProfileSharedPrefHelper.getStudentProfile(requireContext())
            sharedViewModel.loadStudentClasses(it, profile.shift, profile.campus, profile.department)
            adapter.setItems(it)
        })

        viewModel.allCourses.observe(viewLifecycleOwner, { courses ->
            this.courses.clear()
            this.courses.addAll(courses)
        })

        viewModel.allSections.observe(viewLifecycleOwner, {
            sections.clear()
            sections.addAll(it)
        })
    }

    private fun initializeViewDescriptions(profile: ProfileStudent) {
        binding.name.description.text = profile.name
        binding.studentId.description.text = profile.id
        binding.email.description.text = profile.email
        binding.campus.description.text = profile.campus
        binding.department.description.text = profile.department
        binding.section.description.text = profile.section
        binding.level.description.text = profile.level
        binding.term.description.text = profile.term
        binding.shift.description.text = profile.shift
    }

    @SuppressLint("SetTextI18n")
    private fun initializeViewTitles() {
        binding.name.title.text = "Name"
        binding.studentId.title.text = "Student ID"
        binding.email.title.text = "Email"
        binding.campus.title.text = "Campus"
        binding.department.title.text = "Department"
        binding.section.title.text = "Section"
        binding.level.title.text = "Level"
        binding.term.title.text = "Term"
        binding.shift.title.text = "Shift"
    }

}
