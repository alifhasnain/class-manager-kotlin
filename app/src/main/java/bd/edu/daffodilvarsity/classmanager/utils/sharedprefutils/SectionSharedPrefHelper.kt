package bd.edu.daffodilvarsity.classmanager.utils.sharedprefutils

import android.content.Context
import android.content.Context.MODE_PRIVATE
import bd.edu.daffodilvarsity.classmanager.common.extensions.fromJSONToList
import bd.edu.daffodilvarsity.classmanager.common.extensions.fromListToJSON
import bd.edu.daffodilvarsity.classmanager.utils.HelperClass

object SectionSharedPrefHelper {

    private const val SECTION_LIST = "section-list"

    fun saveSections(context: Context, sections: List<String>) {
        val jsonSections = sections.fromListToJSON()
        getSharedPref(context).edit().putString(SECTION_LIST, jsonSections).apply()
    }

    fun getSections(context: Context): List<String> {
        val jsonSections = getSharedPref(context).getString(SECTION_LIST, "")
        return jsonSections?.fromJSONToList() ?: HelperClass.getSectionList()
    }

    private fun getSharedPref(context: Context) = context.getSharedPreferences("section-shared-pref", MODE_PRIVATE)

}