package bd.edu.daffodilvarsity.classmanager.features.emptyrooms

import android.os.Bundle
import android.view.View
import android.widget.ArrayAdapter
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.GridLayoutManager
import bd.edu.daffodilvarsity.classmanager.R
import bd.edu.daffodilvarsity.classmanager.common.models.DataBindingFragment
import bd.edu.daffodilvarsity.classmanager.databinding.FragmentEmptyRoomsBinding
import bd.edu.daffodilvarsity.classmanager.utils.HelperClass
import com.google.android.material.transition.MaterialSharedAxis
import dagger.hilt.android.AndroidEntryPoint

/**
 * A simple [Fragment] subclass.
 */
@AndroidEntryPoint
class EmptyRoomsFragment : DataBindingFragment<FragmentEmptyRoomsBinding>(R.layout.fragment_empty_rooms) {

    private val viewModel: EmptyRoomsViewModel by viewModels()

    private val adapter by lazy {
        EmptyRoomsRecyclerAdapter()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        enterTransition = MaterialSharedAxis(MaterialSharedAxis.Z, true)
        exitTransition = MaterialSharedAxis(MaterialSharedAxis.Z, true)
        binding.presenter = this
        binding.viewmodel = viewModel

        initializeRecyclerView()

        initializeSpinners()
        initializeObservers()
    }

    fun loadEmptyRooms() {
        viewModel.searchEmptyRooms(
            binding.day.selectedItem.toString(),
            binding.time.selectedItem.toString(),
            binding.campus.selectedItem.toString()
        )
    }

    private fun initializeRecyclerView() {
        binding.recyclerView.layoutManager = GridLayoutManager(requireContext(),2)
        binding.recyclerView.adapter = adapter
    }

    private fun initializeObservers() {

        viewModel.classes.observe(viewLifecycleOwner, { adapter.submitItems(it) })

    }

    private fun initializeSpinners() {
        binding.campus.apply {
            adapter = ArrayAdapter(
                context,
                R.layout.spinner_item,
                HelperClass.getCampuses()
            ).apply {
                setDropDownViewResource(R.layout.spinner_dropdown_item)
            }
        }
        binding.day.apply {
            adapter = ArrayAdapter(
                context,
                R.layout.spinner_item,
                HelperClass.getSevenDaysOfWeek()
            ).apply {
                setDropDownViewResource(R.layout.spinner_dropdown_item)
            }
        }
        binding.time.apply {
            adapter = ArrayAdapter(
                context,
                R.layout.spinner_item,
                HelperClass.getClassTimesMainCampus().keys.toMutableList()
            ).apply {
                setDropDownViewResource(R.layout.spinner_dropdown_item)
            }
        }
    }

}
