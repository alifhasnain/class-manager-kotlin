package bd.edu.daffodilvarsity.classmanager.features.completeprofile

import android.os.Bundle
import android.view.View
import androidx.lifecycle.lifecycleScope
import bd.edu.daffodilvarsity.classmanager.R
import bd.edu.daffodilvarsity.classmanager.common.auth.Authenticator
import bd.edu.daffodilvarsity.classmanager.common.extensions.clearStackAndStartActivity
import bd.edu.daffodilvarsity.classmanager.common.extensions.hideKeyboard
import bd.edu.daffodilvarsity.classmanager.common.extensions.makeToast
import bd.edu.daffodilvarsity.classmanager.common.extensions.runWithoutException
import bd.edu.daffodilvarsity.classmanager.common.models.ProfileTeacher
import bd.edu.daffodilvarsity.classmanager.common.models.ViewBindingAppCompatActivity
import bd.edu.daffodilvarsity.classmanager.common.repository.Repository
import bd.edu.daffodilvarsity.classmanager.databinding.ActivityCompleteProfileTeacherBinding
import bd.edu.daffodilvarsity.classmanager.features.completeprofile.common.UneditableStep
import bd.edu.daffodilvarsity.classmanager.features.completeprofile.teacher.CustomizableStep
import bd.edu.daffodilvarsity.classmanager.features.home.MainActivity
import bd.edu.daffodilvarsity.classmanager.utils.sharedprefutils.AuthSharedPrefHelper
import bd.edu.daffodilvarsity.classmanager.utils.sharedprefutils.ProfileSharedPrefHelper
import bd.edu.daffodilvarsity.classmanager.utils.sharedprefutils.SectionSharedPrefHelper
import bd.edu.daffodilvarsity.classmanager.utils.sharedprefutils.VersionSharedPrefHelper
import com.auth0.android.jwt.JWT
import dagger.hilt.android.AndroidEntryPoint
import ernestoyaquello.com.verticalstepperform.Step
import ernestoyaquello.com.verticalstepperform.listener.StepperFormListener
import kotlinx.coroutines.async
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.launch
import okio.IOException
import timber.log.Timber
import javax.inject.Inject

@AndroidEntryPoint
class CompleteProfileTeacherActivity : ViewBindingAppCompatActivity<ActivityCompleteProfileTeacherBinding>(R.layout.activity_complete_profile_teacher,ActivityCompleteProfileTeacherBinding::bind) , StepperFormListener {

    @Inject
    lateinit var repo: Repository

    @Inject
    lateinit var authenticator: Authenticator

    private lateinit var steps: List<Step<String>>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (savedInstanceState == null) { safeScope { initializeStepperForm() } }

    }

    override fun onSaveInstanceState(savedInstanceState: Bundle) {

        if (::steps.isInitialized) {
            savedInstanceState.putString("initial",steps[5].stepData)
            savedInstanceState.putString("contact_no",steps[6].stepData)
        }

        super.onSaveInstanceState(savedInstanceState)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) = safeScope {

        initializeStepperForm()

        if (savedInstanceState.containsKey("initial")) {
            steps[5].restoreStepData(savedInstanceState.getString("initial"))
        }

        if (savedInstanceState.containsKey("contact_no")) {
            steps[6].restoreStepData(savedInstanceState.getString("contact_no"))
        }

        super.onRestoreInstanceState(savedInstanceState)
    }

    override fun onCompletedForm() = safeScope {

        makeToast("Saving data please wait")

        runWithoutException { hideKeyboard() }

        val teacher = ProfileTeacher(
            steps[0].stepData,
            steps[1].stepData,
            steps[2].stepData,
            steps[3].stepData,
            steps[5].stepData,
            steps[4].stepData,
            steps[6].stepData
        )

        repo.refreshAllCoursesFromServer()

        val courseVersion = coroutineScope { async { repo.getCourseVersion() } }

        val sectionVersion = coroutineScope { async { repo.getSectionVersion() } }

        val sections = coroutineScope { async { repo.fetchAllSectionFromServer() } }

        val responseMessage = repo.completeTeacherProfile(teacher)
        makeToast(responseMessage.message)

        if (responseMessage.success) {

            VersionSharedPrefHelper.saveCourseVersion(this, courseVersion.await())
            VersionSharedPrefHelper.saveSectionVersion(this, sectionVersion.await())
            SectionSharedPrefHelper.saveSections(this, sections.await())

            AuthSharedPrefHelper.saveSignedIn(this)
            ProfileSharedPrefHelper.saveTeacherProfile(this,teacher)

            makeToast(responseMessage.message)

            clearStackAndStartActivity<MainActivity>()
        }

    }

    override fun onCancelledForm() {

    }

    private suspend fun initializeStepperForm() {

        val teacherMap = getTeacherData()

        steps = listOf(
            UneditableStep("Employee ID", teacherMap["employee_id"] ?: error("")),
            UneditableStep("Name",teacherMap["name"] ?: error("")),
            UneditableStep("Email",teacherMap["email"] ?: error("")),
            UneditableStep("Department",teacherMap["department"] ?: error("")),
            UneditableStep("Designation",teacherMap["designation"] ?: error("")),
            CustomizableStep("Initial"),
            CustomizableStep("Contact No")
        )

        binding.stepperForm.setup(this, steps).init()
        binding.stepperForm.visibility = View.VISIBLE

        binding.stepperForm.markStepAsCompleted(0, true)
        binding.stepperForm.markStepAsCompleted(1, true)
        binding.stepperForm.markStepAsCompleted(2, true)
        binding.stepperForm.markStepAsCompleted(3, true)
        binding.stepperForm.markStepAsCompleted(4, true)
        binding.stepperForm.goToStep(5, true)
    }

    private suspend fun getTeacherData(): Map<String, String> {
        val jwt = JWT(authenticator.getAccessToken())
        return jwt.claims.entries.associate {
            it.key to it.value.asString()!!
        }
    }

    private fun safeScope(block : (suspend() -> Unit)) {
        lifecycleScope.launch {
            try {
                block.invoke()
            }
            catch (io : IOException){
                makeToast(io.message ?: "Error occurred")
                binding.stepperForm.cancelFormCompletionOrCancellationAttempt()
                Timber.e(io)
            } catch (e: Exception) {
                binding.stepperForm.cancelFormCompletionOrCancellationAttempt()
                makeToast(e.message ?: "")
                Timber.e(e)
            }
        }
    }

}
