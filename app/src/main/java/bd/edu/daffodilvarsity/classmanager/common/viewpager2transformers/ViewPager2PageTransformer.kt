package bd.edu.daffodilvarsity.classmanager.common.viewpager2transformers

import android.view.View
import androidx.viewpager2.widget.ViewPager2

class ViewPager2PageTransformation : ViewPager2.PageTransformer {
    override fun transformPage(page: View, position: Float) {
        page.apply {
            val r = 1 - Math.abs(position)
            page.alpha = 0.25f + r
            page.scaleY = 0.75f + r * 0.25f
        }
    }
}