package bd.edu.daffodilvarsity.classmanager.common.workers

import android.app.AlarmManager
import android.app.PendingIntent
import android.content.Context
import android.content.Context.ALARM_SERVICE
import android.content.Intent
import androidx.hilt.Assisted
import androidx.hilt.work.WorkerInject
import androidx.sqlite.db.SimpleSQLiteQuery
import androidx.work.CoroutineWorker
import androidx.work.WorkerParameters
import bd.edu.daffodilvarsity.classmanager.common.broadcastreceivers.NotificationReceiver
import bd.edu.daffodilvarsity.classmanager.common.database.ClassManagerDao
import bd.edu.daffodilvarsity.classmanager.common.models.ClassDetails
import bd.edu.daffodilvarsity.classmanager.common.models.RegisteredCourse
import bd.edu.daffodilvarsity.classmanager.utils.HelperClass
import bd.edu.daffodilvarsity.classmanager.utils.sharedprefutils.AuthSharedPrefHelper
import bd.edu.daffodilvarsity.classmanager.utils.sharedprefutils.ProfileSharedPrefHelper
import kotlinx.coroutines.coroutineScope
import java.util.*

const val REMINDER_NOTIFICATION = 1213
class ClassNotificationWorker @WorkerInject constructor (
    @Assisted context: Context,
    @Assisted params: WorkerParameters,
    private val dao: ClassManagerDao
) : CoroutineWorker(context, params) {

    override suspend fun doWork(): Result = coroutineScope {

        val userType = AuthSharedPrefHelper.getUserType(applicationContext)

        if (userType == AuthSharedPrefHelper.USER_TYPE_STUDENT) {

            val profile = ProfileSharedPrefHelper.getStudentProfile(applicationContext)
            val registeredCourses = ProfileSharedPrefHelper.getRegisteredCourses(applicationContext)

            val queryString = generateRoutineQueryStringForStudents(registeredCourses, profile.shift, profile.campus, profile.department, getDayOfWeek())
            val studentClasses = dao.getStudentRoutineAsList(SimpleSQLiteQuery(queryString))

            generateTimeForClassReminder(studentClasses)
            
        } else if (userType == AuthSharedPrefHelper.USER_TYPE_TEACHER || userType == AuthSharedPrefHelper.USER_TYPE_ADMIN) {

            val profile = ProfileSharedPrefHelper.getTeacherProfile(applicationContext)
            val teacherClasses = dao.getTeacherRoutineForSpecificDayAsList(profile.initial, getDayOfWeek())

            generateTimeForClassReminder(teacherClasses)
        }

        Result.success()
    }

    private fun generateTimeForClassReminder(classes: List<ClassDetails>) {
        var notificationMsg = ""
        val currentTime = Calendar.getInstance().timeInMillis
        var lowestTime = Calendar.getInstance().apply { add(Calendar.DATE,1) }.timeInMillis

        val todayWithoutHours = Calendar.getInstance().apply {
            set(Calendar.HOUR_OF_DAY,0)
            set(Calendar.MINUTE,0)
            set(Calendar.SECOND,0)
            set(Calendar.MILLISECOND,0)
        }

        for (i in classes) {
            val comparableTime = todayWithoutHours.timeInMillis + (i.startTime * 1000)
            if (i.notification && comparableTime > currentTime && comparableTime < lowestTime) {
                lowestTime = comparableTime
                notificationMsg = "Your class will held at ${HelperClass.getTimeStringFromSecond(i.startTime)} in ${i.room}. This was a reminder."
            }
        }

        if (lowestTime - System.currentTimeMillis() > 14400000) {
            //If remaining time for class is greater that 4 hour
            cancelIfAnyAlarmExists()
        } else if((lowestTime - System.currentTimeMillis()) > 900000) {
            //If current remaining time is greater than 15 min
            scheduleAlarm(notificationMsg, lowestTime - 899000)
        }
    }

    /*
    * Send broadcast to AlarmManager for notification
    */
    private fun scheduleAlarm(msg: String, timeForAlarm: Long) {
        val intent = Intent(applicationContext, NotificationReceiver::class.java)
        intent.putExtra("message", msg)

        val pendingIntent = PendingIntent.getBroadcast(applicationContext, REMINDER_NOTIFICATION, intent, PendingIntent.FLAG_UPDATE_CURRENT)

        val alarmManager = applicationContext.getSystemService(ALARM_SERVICE) as AlarmManager
        alarmManager.setExact(AlarmManager.RTC_WAKEUP, timeForAlarm, pendingIntent)
    }

    private fun cancelIfAnyAlarmExists() {
        val intent = Intent(applicationContext, NotificationReceiver::class.java)
        val pendingIntent = PendingIntent.getBroadcast(applicationContext, REMINDER_NOTIFICATION, intent, PendingIntent.FLAG_UPDATE_CURRENT)

        val alarmManager = applicationContext.getSystemService(ALARM_SERVICE) as AlarmManager
        alarmManager.cancel(pendingIntent)
    }

    private fun generateRoutineQueryStringForStudents(
        registeredCourse: MutableList<RegisteredCourse>,
        shift: String,
        campus: String,
        department: String,
        day: String
    ): String {
        val queryBuilder = StringBuilder("select * from ClassDetails where (")
        registeredCourse.forEachIndexed { index, item ->
            queryBuilder.append("""(courseCode = "${item.courseCode}" and section = "${item.section}")""")
            if(index!=registeredCourse.size-1) {
                queryBuilder.append(" OR ")
            }
        }
        queryBuilder.append(""") AND shift = "$shift" AND campus = "$campus" AND department = "$department" AND dayOfWeek = "$day" order by startTime""")
        return queryBuilder.toString()
    }

    private fun getDayOfWeek(): String {
        val today = Calendar.getInstance()
        return when (today.get(Calendar.DAY_OF_WEEK)) {
            Calendar.SATURDAY -> "Saturday"
            Calendar.SUNDAY -> "Sunday"
            Calendar.MONDAY -> "Monday"
            Calendar.TUESDAY -> "Tuesday"
            Calendar.WEDNESDAY -> "Wednesday"
            Calendar.THURSDAY -> "Thursday"
            else -> "Friday"
        }
    }

}