package bd.edu.daffodilvarsity.classmanager.features.application

import android.app.Application
import android.app.NotificationChannel
import android.app.NotificationManager
import android.os.Build
import androidx.hilt.work.HiltWorkerFactory
import androidx.work.Configuration
import androidx.work.ExistingPeriodicWorkPolicy
import androidx.work.PeriodicWorkRequestBuilder
import androidx.work.WorkManager
import bd.edu.daffodilvarsity.classmanager.BuildConfig
import bd.edu.daffodilvarsity.classmanager.common.workers.ClassNotificationWorker
import dagger.hilt.android.HiltAndroidApp
import timber.log.Timber
import java.util.concurrent.TimeUnit
import javax.inject.Inject

const val ROUTINE_REMINDER_CHANNEL_ID = "routine_reminder_notification_channel"
const val DOWNLOAD_PROGRESS_CHANNEL_ID = "routine_download_progress"
const val FCM_NOTIFICATION_CHANNEL_ID = "fcm_notification_channel"
const val CLASS_NOTIFICATION = "class-reminder"

@HiltAndroidApp
class App : Application(), Configuration.Provider {

    @Inject lateinit var workFactory: HiltWorkerFactory

    override fun onCreate() {
        super.onCreate()
        //Plant Timber for Logging in Debug Build
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }

        createNotificationChannels()
        initializeClassReminderWorker()

    }

    override fun getWorkManagerConfiguration() = Configuration.Builder().apply {
        setWorkerFactory(workFactory)
    }.build()

    /*
    * Launches work request which is used to notify
    * about their class before 15 minute of class time
    * */
    private fun initializeClassReminderWorker() {
        val workRequest = PeriodicWorkRequestBuilder<ClassNotificationWorker>(55, TimeUnit.MINUTES).build()
        WorkManager.getInstance(this).enqueueUniquePeriodicWork(CLASS_NOTIFICATION,ExistingPeriodicWorkPolicy.KEEP, workRequest)
    }

    private fun createNotificationChannels() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel1 = NotificationChannel(
                ROUTINE_REMINDER_CHANNEL_ID,
                "Routine Reminder Channel",
                NotificationManager.IMPORTANCE_HIGH
            )
            channel1.description = "This channel shows the notification of daily routine"

            val channel2 = NotificationChannel(
                DOWNLOAD_PROGRESS_CHANNEL_ID,
                "Download Progress Channel",
                NotificationManager.IMPORTANCE_HIGH
            )
            channel2.description = "This channel shows the download progress of routine"
            channel2.setSound(null, null)

            val channel3 = NotificationChannel(
                FCM_NOTIFICATION_CHANNEL_ID,
                "Firebase Cloud Messaging Channel",
                NotificationManager.IMPORTANCE_HIGH
            )
            channel3.description =
                "This channel shows notification received from Firebase Cloud Messaging."

            val notificationManager = getSystemService(NotificationManager::class.java)
            notificationManager?.createNotificationChannels(listOf(channel1, channel2, channel3))

        }
    }
}