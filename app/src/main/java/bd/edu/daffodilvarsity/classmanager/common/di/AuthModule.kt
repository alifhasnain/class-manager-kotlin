package bd.edu.daffodilvarsity.classmanager.common.di

import android.content.Context
import bd.edu.daffodilvarsity.classmanager.common.auth.Authenticator
import bd.edu.daffodilvarsity.classmanager.common.network.UserApi
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Singleton

@InstallIn(ApplicationComponent::class)
@Module
object AuthModule {

    @Provides
    @Singleton
    fun provideAuthenticator(
        @ApplicationContext context: Context,
        userClient: UserApi
    ): Authenticator = Authenticator(context, userClient)
}