package bd.edu.daffodilvarsity.classmanager.features.completeprofile.student

import android.view.View
import android.view.ViewGroup
import android.widget.NumberPicker
import bd.edu.daffodilvarsity.classmanager.utils.sharedprefutils.SectionSharedPrefHelper
import ernestoyaquello.com.verticalstepperform.Step

/*
* Section Input Step For Students
*/
class SectionStep : Step<String>("Section") {

    private val sectionPicker: NumberPicker by lazy {
        NumberPicker(context).apply {
            val sectionList = SectionSharedPrefHelper.getSections(context)
            minValue = 0
            maxValue = sectionList.size - 1
            displayedValues = sectionList.toTypedArray()

            val params = ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
            )
            layoutParams = params
        }
    }

    override fun createStepContentLayout(): View {
        sectionPicker.setOnValueChangedListener { _, _, _ ->
            markAsCompletedOrUncompleted(true)
        }
        return sectionPicker
    }

    override fun isStepDataValid(stepData: String?): IsDataValid {
        val sectionList = SectionSharedPrefHelper.getSections(context)
        return if(sectionList.contains(stepData)) {
            IsDataValid(true)
        } else {
            IsDataValid(false)
        }
    }

    override fun getStepData(): String {
        return sectionPicker.displayedValues[sectionPicker.value]
    }

    override fun getStepDataAsHumanReadableString(): String {
        return sectionPicker.displayedValues[sectionPicker.value]
    }

    override fun onStepMarkedAsUncompleted(animated: Boolean) {

    }

    override fun onStepMarkedAsCompleted(animated: Boolean) {

    }

    override fun onStepOpened(animated: Boolean) {

    }

    override fun onStepClosed(animated: Boolean) {

    }

    override fun restoreStepData(data: String?) {
        sectionPicker.value = sectionPicker.displayedValues.indexOf(data)
    }
}