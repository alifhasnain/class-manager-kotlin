package bd.edu.daffodilvarsity.classmanager.common.network

import bd.edu.daffodilvarsity.classmanager.common.models.*
import okhttp3.ResponseBody
import retrofit2.http.*

interface AdminApi {

    @POST("upload-routine")
    suspend fun uploadRoutine(
        @Header("Authorization") accessToken: String,
        @Body classes: MutableMap<String,String>
    ): ResponseMessage

    @GET("routine")
    suspend fun routine(
        @Header("Authorization") accessToken: String,
        @Query("start_time") startTime: Int,
        @Query("end_time") endTime: Int,
        @Query("campus") campus: String,
        @Query("day") day: String
    ): List<ClassDetails>

    @POST("add-class")
    suspend fun addClass(
        @Header("Authorization") accessToken: String,
        @Body classDetails: ClassDetails
    ): ResponseMessage

    @POST("update-class")
    suspend fun updateSingleClass(
        @Header("Authorization") accessToken: String,
        @Body classDetails: ClassDetails
    ): ResponseMessage

    @POST("delete-class")
    suspend fun deleteSingleClass(
        @Header("Authorization") accessToken: String,
        @Query("id") id: String
    ): ResponseMessage

    @POST("book-room")
    suspend fun bookRoomAdmin(
        @Header("Authorization") accessToken: String,
        @Body bookingInfo: RoomBookingInfo
    ): ResponseMessage

    @GET("booked-rooms")
    suspend fun getBookedRoomsAtSingleDay(
        @Header("Authorization") accessToken: String,
        @Query("date") dateTimestamp: Long
    ): List<BookedRoom>

    @GET("booked-rooms")
    suspend fun getBookedRoomsWithRangedDate(
        @Header("Authorization") accessToken: String,
        @Query("start_date") startTime: Long,
        @Query("end_date") endTime: Long
    ): List<BookedRoom>

    @POST("add-cr")
    suspend fun addCRProfile(
        @Header("Authorization") accessToken: String,
        @Body profile: CRProfile
    ): ResponseMessage

    @POST("update-cr")
    suspend fun updateCRProfile(
        @Header("Authorization") accessToken: String,
        @Body profile: CRProfile
    ): ResponseMessage

    @POST("delete-cr")
    suspend fun deleteCRProfile(
        @Header("Authorization") accessToken: String,
        @Query("pk") pk: String
    ): ResponseMessage

    @POST("delete-course")
    suspend fun deleteCourse(
        @Header("Authorization") accessToken: String,
        @Query("id") id: Long
    ): ResponseMessage

    @POST("add-course")
    suspend fun addSingleCourse(
        @Header("Authorization") accessToken: String,
        @Body course: Course
    ): ResponseMessage

    @POST("update-course")
    suspend fun updateSingleCourse(
        @Header("Authorization") accessToken: String,
        @Body course: Course
    ): ResponseMessage

    @POST("add-section")
    suspend fun addSection(
        @Header("Authorization") accessToken: String,
        @Query("section") section: String
    ): ResponseMessage

    @POST("delete-section")
    suspend fun deleteSection(
        @Header("Authorization") accessToken: String,
        @Query("id") id: String
    ): ResponseMessage

    @POST("routine-version/{path}")
    suspend fun updateRoutineVersion(
        @Header("Authorization") accessToken: String,
        @Path("path") path: String
    ): ResponseBody

    @POST("app-version/{path}")
    suspend fun updateAppVersion(
        @Header("Authorization") accessToken: String,
        @Path("path") path: String
    ): ResponseBody

    @POST("course-version/{path}")
    suspend fun updateCourseVersion(
        @Header("Authorization") accessToken: String,
        @Path("path") path: String
    ): ResponseBody

    @POST("section-version/{path}")
    suspend fun updateSectionVersion(
        @Header("Authorization") accessToken: String,
        @Path("path") path: String
    ): ResponseBody

    @FormUrlEncoded
    @POST("cancel-book")
    suspend fun cancelBook(
        @Header("Authorization") accessToken: String,
        @Field("id") id: Long,
        @Field("title") title: String,
        @Field("message") message: String
    )

    @POST("reset-book-count")
    suspend fun resetBookCount(@Header("Authorization") accessToken: String): ResponseMessage

}