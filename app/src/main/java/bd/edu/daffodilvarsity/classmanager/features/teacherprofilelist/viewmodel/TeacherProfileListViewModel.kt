package bd.edu.daffodilvarsity.classmanager.features.teacherprofilelist.viewmodel

import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.PagingData
import androidx.paging.cachedIn
import bd.edu.daffodilvarsity.classmanager.common.models.ProfileTeacher
import bd.edu.daffodilvarsity.classmanager.common.repository.Repository
import kotlinx.coroutines.flow.Flow

class TeacherProfileListViewModel @ViewModelInject constructor(
    private val repo: Repository,
    @Assisted savedStateHandle: SavedStateHandle
): ViewModel() {

    private var lastQueryString: String? = null

    private var teacherProfiles: Flow<PagingData<ProfileTeacher>>? = null

    fun fetchTeacherProfiles(queryString: String): Flow<PagingData<ProfileTeacher>>? {
        val lastResult = teacherProfiles
        return if (queryString == lastQueryString && lastResult != null) {
            lastResult
        } else {
            lastQueryString = queryString
            teacherProfiles = repo.getTeacherProfileStream(queryString, 10).cachedIn(viewModelScope)
            return teacherProfiles
        }
    }
}