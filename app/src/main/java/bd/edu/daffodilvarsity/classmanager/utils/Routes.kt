package bd.edu.daffodilvarsity.classmanager.utils

object Routes {
    private const val BASE_URL = "https://alifhasnain.xyz/classmanager"
    //private const val BASE_URL = "http://10.0.2.2:3000"
    //private const val BASE_URL = "http://192.168.1.112:3000"
    const val USER_ROUTE = "$BASE_URL/user/"
    const val ADMIN_ROUTE = "$BASE_URL/admin/"
}