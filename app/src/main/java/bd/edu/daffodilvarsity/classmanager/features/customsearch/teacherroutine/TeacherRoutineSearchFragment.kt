package bd.edu.daffodilvarsity.classmanager.features.customsearch.teacherroutine

import android.os.Bundle
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.ArrayAdapter
import androidx.core.content.ContextCompat.getSystemService
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import bd.edu.daffodilvarsity.classmanager.R
import bd.edu.daffodilvarsity.classmanager.common.extensions.runWithoutException
import bd.edu.daffodilvarsity.classmanager.common.models.DataBindingFragment
import bd.edu.daffodilvarsity.classmanager.databinding.FragmentTeacherRoutineSearchBinding
import bd.edu.daffodilvarsity.classmanager.features.customsearch.adapters.ClassesRecyclerAdapter
import bd.edu.daffodilvarsity.classmanager.features.customsearch.viewmodel.CustomRoutineSearchViewModel
import bd.edu.daffodilvarsity.classmanager.utils.HelperClass
import dagger.hilt.android.AndroidEntryPoint


@AndroidEntryPoint
class TeacherRoutineSearchFragment : DataBindingFragment<FragmentTeacherRoutineSearchBinding>(R.layout.fragment_teacher_routine_search) {

    private val adapter = ClassesRecyclerAdapter()

    private val viewModel: CustomRoutineSearchViewModel by viewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.presenter = this
        binding.viewmodel = viewModel

        initializeSpinners()

        initializeRecyclerView()
        initializeObservers()

    }

    fun loadTeacherClasses(initial: String, day: String) {
        viewModel.loadClassesWithInitial(initial.trim(),day)
        hideKeyboard()
    }

    private fun initializeRecyclerView() {
        binding.recyclerView.layoutManager = LinearLayoutManager(requireContext())
        binding.recyclerView.adapter = adapter
    }

    private fun initializeObservers() {
        viewModel.classes.observe(viewLifecycleOwner) { classes -> adapter.submitItems(classes) }
    }

    private fun hideKeyboard() = runWithoutException {
        val view: View? = activity?.currentFocus
        if (view != null) {
            val imm: InputMethodManager? = getSystemService(requireContext(),InputMethodManager::class.java)
            imm?.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }

    private fun initializeSpinners() {
        binding.dayOfWeeks.apply {
            adapter = ArrayAdapter(
                context,
                R.layout.spinner_item,
                HelperClass.getSevenDaysOfWeek().also {
                    it.add(0,"All")
                }
            ).apply {
                setDropDownViewResource(R.layout.spinner_dropdown_item)
            }
        }
    }

}
