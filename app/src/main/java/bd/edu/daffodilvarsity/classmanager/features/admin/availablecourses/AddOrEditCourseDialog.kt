package bd.edu.daffodilvarsity.classmanager.features.admin.availablecourses

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import androidx.fragment.app.DialogFragment
import androidx.navigation.fragment.findNavController
import bd.edu.daffodilvarsity.classmanager.R
import bd.edu.daffodilvarsity.classmanager.common.extensions.fromJSONToPOJO
import bd.edu.daffodilvarsity.classmanager.common.extensions.ifEmptySetError
import bd.edu.daffodilvarsity.classmanager.common.models.Course
import bd.edu.daffodilvarsity.classmanager.databinding.DialogAddOrEditCourseBinding
import bd.edu.daffodilvarsity.classmanager.utils.HelperClass
import com.wada811.viewbinding.viewBinding

class AddOrEditCourseDialog : DialogFragment() {

    private val binding by viewBinding { DialogAddOrEditCourseBinding.bind(it) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NORMAL, R.style.FullScreenDialog)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.dialog_add_or_edit_course, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initializeSpinners()

        binding.toolbar.setNavigationOnClickListener { dismiss() }
        binding.save.setOnClickListener { saveCourse() }

        val operation = arguments?.getString("operation")
        if (operation == "add") {
            binding.toolbar.title = "Add Course"
        } else if (operation == "edit") {
            binding.toolbar.title = "Edit Course"

            val course = arguments?.getString("course")!!.fromJSONToPOJO<Course>()
            binding.level.editText?.setText(course.level.toString())
            binding.term.editText?.setText(course.term.toString())
            binding.courseCode.editText?.setText(course.courseCode)
            binding.courseName.editText?.setText(course.courseName)
            if (course.shift == "Day") { binding.shift.spinner.setSelection(0) } else { binding.shift.spinner.setSelection(1) }
        }
    }

    private fun saveCourse() {
        if (!checkInputs()) { return }
        val operation = arguments?.getString("operation")
        if (operation == "add") {
            val updatedCourse = Course(
                0,
                binding.level.editText?.text.toString().toInt(),
                binding.term.editText?.text.toString().toInt(),
                binding.courseCode.editText?.text.toString(),
                binding.courseName.editText?.text.toString(),
                binding.shift.spinner.selectedItem.toString(),
                HelperClass.CSE_DEPARTMENT
            )
            findNavController().previousBackStackEntry?.savedStateHandle?.set("data", "add" to updatedCourse)
            findNavController().popBackStack()
        } else if (operation == "edit") {
            val course = arguments?.getString("course")!!.fromJSONToPOJO<Course>()
            val updatedCourse = Course(
                course.id,
                binding.level.editText?.text.toString().toInt(),
                binding.term.editText?.text.toString().toInt(),
                binding.courseCode.editText?.text.toString(),
                binding.courseName.editText?.text.toString(),
                binding.shift.spinner.selectedItem.toString(),
                HelperClass.CSE_DEPARTMENT
            )
            findNavController().previousBackStackEntry?.savedStateHandle?.set("data", "edit" to updatedCourse)
            findNavController().popBackStack()
        }
    }

    private fun checkInputs() =
        binding.level.ifEmptySetError() && binding.term.ifEmptySetError() && binding.courseCode.ifEmptySetError() && binding.courseName.ifEmptySetError()

    private fun initializeSpinners() {
        binding.shift.title.text = "Shift"
        binding.shift.spinner.adapter = ArrayAdapter(
            requireContext(),
            R.layout.spinner_item,
            HelperClass.getShifts()
        ).apply { setDropDownViewResource(R.layout.spinner_dropdown_item) }
    }

}