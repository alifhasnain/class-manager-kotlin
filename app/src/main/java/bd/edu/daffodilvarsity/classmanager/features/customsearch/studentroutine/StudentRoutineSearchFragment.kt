package bd.edu.daffodilvarsity.classmanager.features.customsearch.studentroutine

import android.os.Bundle
import android.view.View
import android.widget.ArrayAdapter
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

import bd.edu.daffodilvarsity.classmanager.R
import bd.edu.daffodilvarsity.classmanager.common.models.DataBindingFragment
import bd.edu.daffodilvarsity.classmanager.databinding.FragmentStudentRoutineSearchBinding
import bd.edu.daffodilvarsity.classmanager.features.customsearch.adapters.ClassesRecyclerAdapter
import bd.edu.daffodilvarsity.classmanager.features.customsearch.viewmodel.CustomRoutineSearchViewModel
import bd.edu.daffodilvarsity.classmanager.utils.HelperClass
import bd.edu.daffodilvarsity.classmanager.utils.sharedprefutils.SectionSharedPrefHelper
import dagger.hilt.android.AndroidEntryPoint


@AndroidEntryPoint
class StudentRoutineSearchFragment : DataBindingFragment<FragmentStudentRoutineSearchBinding>(R.layout.fragment_student_routine_search) {

    private val adapter = ClassesRecyclerAdapter()

    private val viewModel: CustomRoutineSearchViewModel by viewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.viewmodel = viewModel

        initializeSpinners()

        initializeRecyclerView()
        initializeObservers()

    }

    private fun initializeObservers() {
        viewModel.classes.observe(viewLifecycleOwner) { adapter.submitItems(it) }
    }

    private fun initializeRecyclerView() {
        binding.recyclerView.layoutManager = LinearLayoutManager(requireContext())
        binding.recyclerView.adapter = adapter
        adapter.stateRestorationPolicy = RecyclerView.Adapter.StateRestorationPolicy.PREVENT_WHEN_EMPTY
    }

    private fun initializeSpinners() {

        binding.campus.apply {
            adapter = ArrayAdapter(
                context,
                R.layout.spinner_item,
                HelperClass.getCampuses()
            ).apply { setDropDownViewResource(R.layout.spinner_dropdown_item) }
        }

        binding.level.apply {
            adapter = ArrayAdapter(
                context,
                R.layout.spinner_item,
                mutableListOf("Level 1","Level 2","Level 3","Level 4")
            ).apply { setDropDownViewResource(R.layout.spinner_dropdown_item) }
        }

        binding.term.apply {
            adapter = ArrayAdapter(
                context,
                R.layout.spinner_item,
                mutableListOf("Term 1","Term 2","Term 3")
            ).apply { setDropDownViewResource(R.layout.spinner_dropdown_item) }
        }

        binding.section.apply {
            adapter = ArrayAdapter(
                context,
                R.layout.spinner_item,
                SectionSharedPrefHelper.getSections(context)
            ).apply { setDropDownViewResource(R.layout.spinner_dropdown_item) }
        }

        binding.shift.apply {
            adapter = ArrayAdapter(
                context,
                R.layout.spinner_item,
                HelperClass.getShifts()
            ).apply { setDropDownViewResource(R.layout.spinner_dropdown_item) }
        }

    }

}
