package bd.edu.daffodilvarsity.classmanager.features.routine.ui

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import bd.edu.daffodilvarsity.classmanager.R
import bd.edu.daffodilvarsity.classmanager.common.extensions.makeToast
import bd.edu.daffodilvarsity.classmanager.common.models.ViewBindingFragment
import bd.edu.daffodilvarsity.classmanager.databinding.FragmentRoutineTabItemBinding
import bd.edu.daffodilvarsity.classmanager.features.routine.adapters.ClassesListListAdapter
import bd.edu.daffodilvarsity.classmanager.features.routine.viewmodels.RoutineSharedViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.launch

/**
 * A simple [Fragment] subclass.
 */
@AndroidEntryPoint
class RoutineTabItemFragment : ViewBindingFragment<FragmentRoutineTabItemBinding>(R.layout.fragment_routine_tab_item, { FragmentRoutineTabItemBinding.bind(it) }) {

    private val adapter = ClassesListListAdapter()

    private val sharedViewModel: RoutineSharedViewModel by activityViewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initializeRecyclerView()

        initializeObservers()

    }

    private fun initializeRecyclerView() {
        binding.recyclerView.adapter = adapter
        binding.recyclerView.layoutManager = LinearLayoutManager(requireContext())
        adapter.onNotificationClicked = { myclass ->
            sharedViewModel.toggleNotification(myclass.id,!myclass.notification)
            makeToast(if(!myclass.notification) "Notification On" else "Notification Off")
        }
    }

    private fun initializeObservers() {
        val dayOfWeek = requireArguments().getString("day")
        lifecycleScope.launch {
            sharedViewModel.userClasses?.map { classes ->
                classes.filter { it.dayOfWeek == dayOfWeek }
            }?.collect {
                adapter.submitList(it)
                if (it.isEmpty()) {
                    binding.noClasses.visibility = View.VISIBLE
                } else {
                    binding.noClasses.visibility = View.GONE
                }
            }
        }
    }

}
