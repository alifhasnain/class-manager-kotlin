package bd.edu.daffodilvarsity.classmanager.features.admin.parseroutine

import android.annotation.SuppressLint
import android.graphics.Color
import android.graphics.Point
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.*
import androidx.fragment.app.DialogFragment
import bd.edu.daffodilvarsity.classmanager.R
import bd.edu.daffodilvarsity.classmanager.databinding.DialogUploadParsedRoutineBinding
import com.wada811.viewbinding.viewBinding


class ConfirmationDialog: DialogFragment() {

    private val binding by viewBinding { DialogUploadParsedRoutineBinding.bind(it) }

    var okayClickListener: ((Boolean) -> Unit)? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        dialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        return inflater.inflate(R.layout.dialog_upload_parsed_routine,container,false)
    }

    @SuppressLint("SetTextI18n")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.denyButton.setOnClickListener { dismiss() }
        binding.okayButton.setOnClickListener {
            okayClickListener?.invoke(true)
            dismiss()
        }

        binding.campus.title.text = "Campus"
        binding.department.title.text = "Department"
        binding.shift.title.text = "Shift"
        binding.totalClasses.title.text = "Total Classes"
        binding.empty.title.text = "Empty"
        binding.nonEmpty.title.text = "Non-Empty"

        binding.campus.data.text = arguments?.getString("campus") ?: ""
        binding.department.data.text = arguments?.getString("department") ?: ""
        binding.shift.data.text = arguments?.getString("shift") ?: ""
        binding.totalClasses.data.text = arguments?.getString("total") ?: ""
        binding.empty.data.text = arguments?.getString("empty") ?: ""
        binding.nonEmpty.data.text = arguments?.getString("nonEmpty") ?: ""
    }

    override fun onResume() {
        // TODO- Niloy (06 Sep 2020): Fix the deprecation issue in this
        // Store access variables for window and blank point
        val window: Window? = dialog!!.window
        val size = Point()
        // Store dimensions of the screen in `size`
        val display: Display = window!!.windowManager.defaultDisplay
        display.getSize(size)
        // Set the width of the dialog proportional to 75% of the screen width
        window.setLayout((size.x * 0.85).toInt(), WindowManager.LayoutParams.WRAP_CONTENT)
        window.setGravity(Gravity.CENTER)
        // Call super onResume after sizing
        super.onResume()
    }
}