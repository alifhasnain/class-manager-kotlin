package bd.edu.daffodilvarsity.classmanager.common.models

import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
@Entity(indices = [Index(value = ["courseCode" , "department" , "shift"] , unique = true)])
data class Course
(
    @PrimaryKey val id : Long,
    val level: Int,
    val term: Int,
    @field:Json(name = "course_code") val courseCode: String,
    @field:Json(name = "course_name") val courseName: String,
    val shift : String,
    val department : String
)