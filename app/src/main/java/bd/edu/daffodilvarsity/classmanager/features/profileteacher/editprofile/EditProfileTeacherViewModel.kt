package bd.edu.daffodilvarsity.classmanager.features.profileteacher.editprofile

import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.*
import bd.edu.daffodilvarsity.classmanager.common.models.ProfileTeacher
import bd.edu.daffodilvarsity.classmanager.common.repository.Repository
import kotlinx.coroutines.launch
import okio.IOException
import timber.log.Timber

class EditProfileTeacherViewModel @ViewModelInject constructor(
    private val repo: Repository,
    @Assisted private val savedStateHandle: SavedStateHandle
): ViewModel() {

    private val _profile = MutableLiveData<ProfileTeacher>()
    val profile: LiveData<ProfileTeacher> = _profile

    private val _toast = MutableLiveData<String>()
    val toast: LiveData<String> = _toast

    private val _progressBarVisibility = MutableLiveData(false)
    val progressBarVisibility: LiveData<Boolean> = _progressBarVisibility

    private val _saveButtonEnabled = MutableLiveData(true)
    val saveButtonEnabled: LiveData<Boolean> = _saveButtonEnabled

    fun initializeProfile(profile: ProfileTeacher) { _profile.value = profile }

    fun saveProfile(profile: ProfileTeacher) = safeScope {
        _profile.value = repo.saveTeacherProfileToServer(profile)
        _toast.value = "saved"
    }

    private fun safeScope(block: (suspend () -> Unit)) = viewModelScope.launch {
        try {
            _progressBarVisibility.value = true
            _saveButtonEnabled.value = false
            block()
        } catch (io: IOException) {
            Timber.e(io)
            _toast.value = io.message
        } catch (e: Exception) {
            Timber.e(e)
            _toast.value = "Failed to connect. Please check your connection and try again"
        } finally {
            _progressBarVisibility.value = false
            _saveButtonEnabled.value = true
        }
    }

}