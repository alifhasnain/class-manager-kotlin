package bd.edu.daffodilvarsity.classmanager.utils.sharedprefutils

import android.content.Context
import android.content.Context.MODE_PRIVATE
import android.content.SharedPreferences
import com.auth0.android.jwt.JWT


object AuthSharedPrefHelper {

    private const val AUTH_SHARED_PREF_TAG = "class_manager_auth_shared_pref"

    private const val ACCESS_TOKEN_TAG = "access_token_shared_pref"

    private const val REFRESH_TOKEN_TAG = "refresh_token_shared_pref"

    private const val USER_LOGIN_ID = "user-login-id"

    private const val SIGNED_IN = "signed-in"

    const val USER_TYPE_STUDENT = "student"
    const val USER_TYPE_TEACHER = "teacher"
    const val USER_TYPE_ADMIN = "admin"

    fun getAccessToken(context: Context): String {
        val sharedPref = getAuthSharedPref(context)
        return sharedPref.getString(ACCESS_TOKEN_TAG, "")!!
    }

    fun saveAccessToken(context: Context, token: String) {
        getAuthSharedPref(context).edit().putString(ACCESS_TOKEN_TAG, token).apply()
    }

    fun getRefreshToken(context: Context) = getAuthSharedPref(context).getString(REFRESH_TOKEN_TAG, "null")!!

    fun saveRefreshToken(context: Context, token: String) {
        getAuthSharedPref(context).edit().putString(REFRESH_TOKEN_TAG, token).apply()
    }

    fun saveLoginIdAndPassword(context: Context, id: String , password : String) {
        getAuthSharedPref(context).edit().putString(USER_LOGIN_ID,id).putString("pass",password).apply()
    }

    fun getLoginID(context: Context) : String {
        return getAuthSharedPref(context).getString(USER_LOGIN_ID,"")!!
    }

    fun getPassword(context: Context) : String = getAuthSharedPref(context).getString("pass","")!!

    fun getUserType(context: Context) : String {
        val authToken = getAccessToken(context)
        val jwt = JWT(authToken)
        return jwt.getClaim("role").asString() ?: ""
    }

    fun isSignedIn(context: Context): Boolean {
        return getAccessToken(context).isNotEmpty() && getAuthSharedPref(context).getBoolean(SIGNED_IN,false)
    }

    fun saveSignedIn(context: Context) {
        getAuthSharedPref(context).edit().putBoolean(SIGNED_IN,true).apply()
    }

    fun clearAuthSharedPref(context: Context) = getAuthSharedPref(context).edit().clear().apply()

    private fun getAuthSharedPref(context: Context): SharedPreferences {
        return context.getSharedPreferences(AUTH_SHARED_PREF_TAG, MODE_PRIVATE)
    }

}