package bd.edu.daffodilvarsity.classmanager.features.admin.miscellaneous

import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.*
import bd.edu.daffodilvarsity.classmanager.common.extensions.networkErrorMessage
import bd.edu.daffodilvarsity.classmanager.common.extensions.setValueThenNullify
import bd.edu.daffodilvarsity.classmanager.common.models.LoadStates
import bd.edu.daffodilvarsity.classmanager.common.repository.Repository
import kotlinx.coroutines.launch

@Suppress("BlockingMethodInNonBlockingContext")
class MiscellaneousFragmentViewModel @ViewModelInject constructor (
    private val repo: Repository,
    @Assisted private val savedStateHandle: SavedStateHandle
): ViewModel() {

    private val _routineVersion = MutableLiveData("null")
    val routineVersion: LiveData<String> = _routineVersion

    private val _appVersion = MutableLiveData("null")
    val appVersion: LiveData<String> = _appVersion

    private val _courseVersion = MutableLiveData("null")
    val courseVersion: LiveData<String> = _courseVersion

    private val _sectionVersion = MutableLiveData("null")
    val sectionVersion: LiveData<String> = _sectionVersion

    val routineVersionState = MutableLiveData<LoadStates>(LoadStates.NotEmpty)
    val appVersionState = MutableLiveData<LoadStates>(LoadStates.NotEmpty)
    val courseVersionState = MutableLiveData<LoadStates>(LoadStates.NotEmpty)
    val sectionVersionState = MutableLiveData<LoadStates>(LoadStates.NotEmpty)
    val resetBookCountState = MutableLiveData<LoadStates>(LoadStates.NotEmpty)

    val toast = MutableLiveData<String>()

    init {
        refreshAll()
    }

    fun refreshAll() {
        toast.setValueThenNullify("Refreshing all data from server")
        loadAppVersion()
        loadRoutineVersion()
        loadCourseVersion()
        loadSectionVersion()
    }

    fun loadAppVersion() = viewModelScope.launch {
        try {
            appVersionState.value = LoadStates.Loading
            _appVersion.value = repo.getAppVersion()
        } catch (e: Exception) {
            toast.setValueThenNullify(e.networkErrorMessage)
        } finally {
            appVersionState.value = LoadStates.NotEmpty
        }
    }

    fun loadRoutineVersion() = viewModelScope.launch {
        try {
            routineVersionState.value = LoadStates.Loading
            _routineVersion.value = repo.getRoutineVersion()
        } catch (e: Exception) {
            toast.setValueThenNullify(e.networkErrorMessage)
        } finally {
            routineVersionState.value = LoadStates.NotEmpty
        }
    }

    fun loadCourseVersion() = viewModelScope.launch {
        try {
            courseVersionState.value = LoadStates.Loading
            _courseVersion.value = repo.getCourseVersion()
        } catch (e: Exception) {
            toast.setValueThenNullify(e.networkErrorMessage)
        } finally {
            courseVersionState.value = LoadStates.NotEmpty
        }
    }

    fun loadSectionVersion() = viewModelScope.launch {
        try {
            sectionVersionState.value = LoadStates.Loading
            _sectionVersion.value = repo.getSectionVersion()
        } catch (e: Exception) {
            toast.setValueThenNullify(e.networkErrorMessage)
        } finally {
            sectionVersionState.value = LoadStates.NotEmpty
        }
    }

    fun increaseAppVersion() = viewModelScope.launch {
        try {
            appVersionState.value = LoadStates.Loading
            _appVersion.value = repo.updateAppVersion("inc").string()
        } catch (e: Exception) {
            toast.setValueThenNullify(e.networkErrorMessage)
        } finally {
            appVersionState.value = LoadStates.NotEmpty
        }
    }

    fun increaseRoutineVersion() = viewModelScope.launch {
        try {
            routineVersionState.value = LoadStates.Loading
            _routineVersion.value = repo.updateRoutineVersion("inc").string()
        } catch (e: Exception) {
            toast.setValueThenNullify(e.networkErrorMessage)
        } finally {
            routineVersionState.value = LoadStates.NotEmpty
        }
    }

    fun increaseCourseVersion() = viewModelScope.launch {
        try {
            courseVersionState.value = LoadStates.Loading
            _courseVersion.value = repo.updateCourseVersion("inc").string()
        } catch (e: Exception) {
            toast.setValueThenNullify(e.networkErrorMessage)
        } finally {
            courseVersionState.value = LoadStates.NotEmpty
        }
    }

    fun increaseSectionVersion() = viewModelScope.launch {
        try {
            sectionVersionState.value = LoadStates.Loading
            _sectionVersion.value = repo.updateSectionVersion("inc").string()
        } catch (e: Exception) {
            toast.setValueThenNullify(e.networkErrorMessage)
        } finally {
            sectionVersionState.value = LoadStates.NotEmpty
        }
    }

    fun decreaseAppVersion() = viewModelScope.launch {
        try {
            appVersionState.value = LoadStates.Loading
            _appVersion.value = repo.updateAppVersion("dec").string()
        } catch (e: Exception) {
            toast.setValueThenNullify(e.networkErrorMessage)
        } finally {
            appVersionState.value = LoadStates.NotEmpty
        }
    }

    fun decreaseRoutineVersion() = viewModelScope.launch {
        try {
            routineVersionState.value = LoadStates.Loading
            _routineVersion.value = repo.updateRoutineVersion("dec").string()
        } catch (e: Exception) {
            toast.setValueThenNullify(e.networkErrorMessage)
        } finally {
            routineVersionState.value = LoadStates.NotEmpty
        }
    }

    fun decreaseCourseVersion() = viewModelScope.launch {
        try {
            courseVersionState.value = LoadStates.Loading
            _courseVersion.value = repo.updateCourseVersion("dec").string()
        } catch (e: Exception) {
            toast.setValueThenNullify(e.networkErrorMessage)
        } finally {
            courseVersionState.value = LoadStates.NotEmpty
        }
    }

    fun decreaseSectionVersion() = viewModelScope.launch {
        try {
            sectionVersionState.value = LoadStates.Loading
            _sectionVersion.value = repo.updateSectionVersion("dec").string()
        } catch (e: Exception) {
            toast.setValueThenNullify(e.networkErrorMessage)
        } finally {
            sectionVersionState.value = LoadStates.NotEmpty
        }
    }

    fun resetBookCount() = viewModelScope.launch {
        try {
            resetBookCountState.value = LoadStates.Loading
            val responseMessage = repo.resetBookCount()
            toast.setValueThenNullify(responseMessage.message)
        } catch (e: Exception) {
            toast.setValueThenNullify(e.networkErrorMessage)
        } finally {
            resetBookCountState.value = LoadStates.NotEmpty
        }
    }

}