package bd.edu.daffodilvarsity.classmanager.features.admin.updateroutine

import android.app.TimePickerDialog
import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.widget.ArrayAdapter
import androidx.appcompat.app.AlertDialog
import androidx.core.os.bundleOf
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import bd.edu.daffodilvarsity.classmanager.R
import bd.edu.daffodilvarsity.classmanager.common.extensions.fromPOJOToJSON
import bd.edu.daffodilvarsity.classmanager.common.extensions.makeToast
import bd.edu.daffodilvarsity.classmanager.common.models.ClassDetails
import bd.edu.daffodilvarsity.classmanager.common.models.DataBindingFragment
import bd.edu.daffodilvarsity.classmanager.databinding.FragmentUpdateRoutineBinding
import bd.edu.daffodilvarsity.classmanager.utils.HelperClass
import dagger.hilt.android.AndroidEntryPoint
import timber.log.Timber


@AndroidEntryPoint
class UpdateRoutineFragment : DataBindingFragment<FragmentUpdateRoutineBinding>(R.layout.fragment_update_routine) {

    private val viewModel by viewModels<UpdateRoutineViewModel>()

    private val adapter by lazy { UpdateRoutineRecyclerAdapter() }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setHasOptionsMenu(true)
        binding.presenter = this
        binding.viewModel = viewModel

        initializeSpinners()
        initializeObservers()
        initializeRecyclerView()
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        menu.add("Add Class")
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.title == "Add Class") { addClass() }
        return super.onOptionsItemSelected(item)
    }

    fun fetchClasses() {
        if (verifyStartAndEndTime()) {
            viewModel.fetchClasses(
                HelperClass.getTimeSecondFromString(binding.startTime.text.toString()),
                HelperClass.getTimeSecondFromString(binding.endTime.text.toString()),
                binding.daySpinner.selectedItem.toString(),
                binding.campusSpinner.selectedItem.toString()
            )
        } else {
            makeToast("Please specify time range")
        }
    }

    fun pickStartTime() {
        TimePickerDialog(requireContext(), { _, hourOfDay, minute ->
            binding.startTime.text = HelperClass.getTimeStringFromSecond(hourOfDay * 3600 + minute * 60)
        }, 0, 0, false).show()
    }

    fun pickEndTime() {
        TimePickerDialog(requireContext(), { _, hourOfDay, minute ->
            binding.endTime.text = HelperClass.getTimeStringFromSecond(hourOfDay * 3600 + minute * 60)
        }, 0, 0, false).show()
    }

    private fun addClass() {
        findNavController().navigate(R.id.action_updateRoutineFragment_to_addOrUpdateClassDialog, bundleOf("action" to "add"))
    }

    private fun editClass(classDetails: ClassDetails) {
        val classDetailsJSON = classDetails.fromPOJOToJSON()
        findNavController().navigate(R.id.action_updateRoutineFragment_to_addOrUpdateClassDialog, bundleOf("action" to "update", "data" to classDetailsJSON))
    }

    private fun deleteClass(id: String) = AlertDialog.Builder(requireContext()).apply {
        setTitle("Please verify")
        setMessage("Are you sure you want to delete this?")
        setPositiveButton("Proceed") { _, _ ->
            viewModel.deleteClass(id)
        }
            .setNegativeButton("No", null)
    }.create().show()

    private fun verifyStartAndEndTime(): Boolean = try {
        HelperClass.getTimeSecondFromString(binding.startTime.text.toString())
        HelperClass.getTimeSecondFromString(binding.endTime.text.toString())
        true
    } catch (e: Exception) {
        Timber.e(e)
        false
    }

    private fun initializeRecyclerView() {
        binding.recyclerView.adapter = adapter
        adapter.longClickListener = {
            AlertDialog.Builder(requireContext()).apply {
                setItems(arrayOf("Edit", "Delete")) { _, position ->
                    if (position == 0) {
                        editClass(it)
                    } else if (position == 1) {
                        deleteClass(it.id.toString())
                    }
                }
            }.create().show()
        }
    }

    private fun initializeObservers() {
        findNavController().getBackStackEntry(R.id.updateRoutineFragment)
            .savedStateHandle
            .getLiveData<Boolean>("refresh")
            .observe(viewLifecycleOwner) {
                viewModel.fetchClassesFromCachedQueryData()
            }

        viewModel.classes.observe(viewLifecycleOwner) { adapter.submitItem(it) }
        viewModel.toast.observe(viewLifecycleOwner) { makeToast(it) }
    }

    private fun initializeSpinners() {

        binding.campusSpinner.adapter = ArrayAdapter(
            requireContext(),
            R.layout.spinner_item,
            HelperClass.getCampuses()
        ).apply { setDropDownViewResource(R.layout.spinner_dropdown_item) }

        binding.daySpinner.adapter = ArrayAdapter(
            requireContext(),
            R.layout.spinner_item,
            HelperClass.getSevenDaysOfWeek()
        ).apply { setDropDownViewResource(R.layout.spinner_dropdown_item) }

    }

}