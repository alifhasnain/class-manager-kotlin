package bd.edu.daffodilvarsity.classmanager.features.notification.viewmodel

import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.*
import bd.edu.daffodilvarsity.classmanager.common.extensions.networkErrorMessage
import bd.edu.daffodilvarsity.classmanager.common.models.LoadStates
import bd.edu.daffodilvarsity.classmanager.common.repository.Repository
import bd.edu.daffodilvarsity.classmanager.utils.Event
import kotlinx.coroutines.launch

class NotificationViewModel @ViewModelInject constructor(
    private val repo: Repository,
    @Assisted private val savedStateHandle: SavedStateHandle
): ViewModel() {

    val uiState: MutableLiveData<LoadStates> = MutableLiveData(LoadStates.Empty("Nothing found"))

    private val _toast: MutableLiveData<Event<String>> = MutableLiveData()
    val toast: LiveData<Event<String>> = _toast

    val notifications = repo.getNotifications()

    fun updateCheckStatus(pk: Long, read: Boolean) = viewModelScope.launch {
        try {
            repo.updateReadStatus(pk, read)
        } catch (e: Exception) {
            _toast.value = Event(e.networkErrorMessage)
        }
    }

    fun markAllAsRead() = viewModelScope.launch {
        try {
            repo.markAllNotificationAsRead()
        } catch (e: Exception) {
            _toast.value = Event(e.networkErrorMessage)
        }
    }

    fun clearAllNotification() = viewModelScope.launch {
        try {
            repo.clearAllNotifications()
        } catch (e: Exception) {
            _toast.value = Event(e.networkErrorMessage)
        }
    }

}