package bd.edu.daffodilvarsity.classmanager.features.profilestudent.viewprofile

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import androidx.fragment.app.DialogFragment
import bd.edu.daffodilvarsity.classmanager.R
import bd.edu.daffodilvarsity.classmanager.common.models.RegisteredCourse
import bd.edu.daffodilvarsity.classmanager.databinding.DialogSelectSectionAndCourseCodeBinding
import com.wada811.viewbinding.viewBinding

class CourseAndSectionSelectorDialog(private val courses: List<String>, private val sections: List<String>) : DialogFragment() {

    var onClickListener: ((RegisteredCourse) -> Unit)? = null

    private val binding by viewBinding { DialogSelectSectionAndCourseCodeBinding.bind(it) }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.dialog_select_section_and_course_code,container,false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initializeSpinners()
        binding.cancel.setOnClickListener { dismiss() }
        binding.select.setOnClickListener {
            onClickListener?.invoke(
                RegisteredCourse(
                    binding.courseSelector.selectedItem.toString().split(":")[0].trim(),
                    binding.sectionSelector.selectedItem.toString()
                )
            )
            dismiss()
        }
    }

    private fun initializeSpinners() {
        binding.courseSelector.apply {
            adapter = ArrayAdapter(
                context,
                R.layout.spinner_item,
                courses
            ).apply { setDropDownViewResource(R.layout.spinner_dropdown_item) }
        }
        binding.sectionSelector.apply {
            adapter = ArrayAdapter(
                context,
                R.layout.spinner_item,
                sections
            ).apply { setDropDownViewResource(R.layout.spinner_dropdown_item) }
        }
    }
}