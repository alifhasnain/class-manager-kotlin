package bd.edu.daffodilvarsity.classmanager.features.admin.crlist

import android.annotation.SuppressLint
import android.content.DialogInterface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import bd.edu.daffodilvarsity.classmanager.R
import bd.edu.daffodilvarsity.classmanager.common.extensions.fromJSONToPOJO
import bd.edu.daffodilvarsity.classmanager.common.extensions.makeToast
import bd.edu.daffodilvarsity.classmanager.common.models.CRProfile
import bd.edu.daffodilvarsity.classmanager.databinding.DialogAddOrEditCRDetailsBinding
import bd.edu.daffodilvarsity.classmanager.utils.HelperClass
import bd.edu.daffodilvarsity.classmanager.utils.sharedprefutils.SectionSharedPrefHelper
import com.wada811.databinding.dataBinding
import dagger.hilt.android.AndroidEntryPoint

@SuppressLint("SetTextI18n")
@AndroidEntryPoint
class AddOrEditCRDetailsDialog : DialogFragment() {

    private val binding by dataBinding<DialogAddOrEditCRDetailsBinding>()

    private val viewModel: UpdateCRDetailsViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NORMAL, R.style.FullScreenDialog)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.dialog_add_or_edit_c_r_details, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.viewmodel = viewModel
        initializeSpinners()
        initializeProperties()
        initializeObservers()
    }

    override fun onDismiss(dialog: DialogInterface) {
        super.onDismiss(dialog)
        findNavController().previousBackStackEntry?.savedStateHandle?.set("reload", true)
    }

    private fun addCRProfile() {
        if (checkFieldsAreEmptyOrNot()) {
            val profile = CRProfile(
                0,
                binding.studentId.editText!!.text.toString().trim(),
                binding.name.editText!!.text.toString().trim(),
                binding.email.editText!!.text.toString().trim(),
                binding.level.spinner.selectedItem.toString().toInt(),
                binding.term.spinner.selectedItem.toString().toInt(),
                binding.section.spinner.selectedItem.toString(),
                binding.phoneNo.editText!!.text.toString().trim(),
                binding.shift.spinner.selectedItem.toString(),
                binding.campus.spinner.selectedItem.toString(),
                binding.department.spinner.selectedItem.toString(),
            )
            viewModel.addCRProfile(profile)
        }
    }

    private fun updateCRProfile() {
        if (checkFieldsAreEmptyOrNot()) {
            val oldProfile = arguments?.getString("data")!!.fromJSONToPOJO<CRProfile>()
            val profile = CRProfile(
                oldProfile.pk,
                binding.studentId.editText!!.text.toString().trim(),
                binding.name.editText!!.text.toString().trim(),
                binding.email.editText!!.text.toString().trim(),
                binding.level.spinner.selectedItem.toString().toInt(),
                binding.term.spinner.selectedItem.toString().toInt(),
                binding.section.spinner.selectedItem.toString(),
                binding.phoneNo.editText!!.text.toString().trim(),
                binding.shift.spinner.selectedItem.toString(),
                binding.campus.spinner.selectedItem.toString(),
                binding.department.spinner.selectedItem.toString(),
            )
            viewModel.updateCRProfile(profile)
        }
    }

    private fun checkFieldsAreEmptyOrNot(): Boolean {

        var result = true

        binding.studentId.error = null
        binding.name.error = null
        binding.email.error = null
        binding.phoneNo.error = null

        if (binding.studentId.editText!!.text.toString().isEmpty()) {
            binding.studentId.error = "Can't be empty"
            result = false
        }

        if (binding.name.editText!!.text.toString().isEmpty()) {
            binding.name.error = "Can't be empty"
            result = false
        }

        if (binding.email.editText!!.text.toString().isEmpty()) {
            binding.email.error = "Can't be empty"
            result = false
        }

        if (binding.phoneNo.editText!!.text.toString().isEmpty()) {
            binding.phoneNo.error = "Can't be empty"
            result = false
        }

        return result
    }

    private fun initializeProperties() {

        val action = arguments?.getString("action")

        binding.save.setOnClickListener {
            if (action == "add") {
                addCRProfile()
            } else if (action == "update") {
                updateCRProfile()
            }
        }

        binding.toolbar.setNavigationOnClickListener { dismiss() }

        if (action == "add") {
            binding.toolbar.title = "Add Info"
            binding.save.text = "Add"
        } else if (action == "update") {
            binding.toolbar.title = "Update Info"
            populateViewsWithData()
        }
    }

    private fun initializeObservers() {
        viewModel.toast.observe(viewLifecycleOwner) { makeToast(it) }
    }

    @Suppress("UNCHECKED_CAST")
    private fun populateViewsWithData() {
        val profile = arguments?.getString("data")!!.fromJSONToPOJO<CRProfile>()
        binding.save.text = "Update"
        binding.studentId.editText?.setText(profile.id)
        binding.name.editText?.setText(profile.name)
        binding.email.editText?.setText(profile.email)
        binding.phoneNo.editText?.setText(profile.phoneNo)
        binding.level.spinner.setSelection(
            (binding.level.spinner.adapter as ArrayAdapter<String>).getPosition(profile.level.toString())
        )
        binding.term.spinner.setSelection(
            (binding.term.spinner.adapter as ArrayAdapter<String>).getPosition(profile.term.toString())
        )
        binding.shift.spinner.setSelection(
            (binding.shift.spinner.adapter as ArrayAdapter<String>).getPosition(profile.shift)
        )
        binding.section.spinner.setSelection(
            (binding.section.spinner.adapter as ArrayAdapter<String>).getPosition(profile.section)
        )
        binding.campus.spinner.setSelection(
            (binding.campus.spinner.adapter as ArrayAdapter<String>).getPosition(profile.campus)
        )
    }

    private fun initializeSpinners() {
        binding.level.title.text = "Level"
        binding.level.spinner.adapter = ArrayAdapter(
            requireContext(),
            R.layout.spinner_item,
            HelperClass.getLevels()
        ).apply { setDropDownViewResource(R.layout.spinner_dropdown_item) }

        binding.term.title.text = "Term"
        binding.term.spinner.adapter = ArrayAdapter(
            requireContext(),
            R.layout.spinner_item,
            HelperClass.getTerms()
        ).apply { setDropDownViewResource(R.layout.spinner_dropdown_item) }

        binding.shift.title.text = "Shift"
        binding.shift.spinner.adapter = ArrayAdapter(
            requireContext(),
            R.layout.spinner_item,
            HelperClass.getShifts()
        ).apply { setDropDownViewResource(R.layout.spinner_dropdown_item) }

        binding.section.title.text = "Section"
        binding.section.spinner.adapter = ArrayAdapter(
            requireContext(),
            R.layout.spinner_item,
            SectionSharedPrefHelper.getSections(requireContext())
        ).apply { setDropDownViewResource(R.layout.spinner_dropdown_item) }

        binding.campus.title.text = "Campus"
        binding.campus.spinner.adapter = ArrayAdapter(
            requireContext(),
            R.layout.spinner_item,
            HelperClass.getCampuses()
        ).apply { setDropDownViewResource(R.layout.spinner_dropdown_item) }

        binding.department.title.text = "Department"
        binding.department.spinner.adapter = ArrayAdapter(
            requireContext(),
            R.layout.spinner_item,
            HelperClass.getDepartments()
        ).apply { setDropDownViewResource(R.layout.spinner_dropdown_item) }
    }
}