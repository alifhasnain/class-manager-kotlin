package bd.edu.daffodilvarsity.classmanager.features.admin.crlist

import android.annotation.SuppressLint
import android.content.ActivityNotFoundException
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.widget.ArrayAdapter
import androidx.appcompat.app.AlertDialog
import androidx.core.os.bundleOf
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import bd.edu.daffodilvarsity.classmanager.R
import bd.edu.daffodilvarsity.classmanager.common.extensions.fromPOJOToJSON
import bd.edu.daffodilvarsity.classmanager.common.extensions.makeToast
import bd.edu.daffodilvarsity.classmanager.common.models.DataBindingFragment
import bd.edu.daffodilvarsity.classmanager.databinding.FragmentUpdateCRDetailsBinding
import bd.edu.daffodilvarsity.classmanager.utils.HelperClass
import com.google.android.material.transition.MaterialSharedAxis
import dagger.hilt.android.AndroidEntryPoint
import timber.log.Timber


@AndroidEntryPoint
class UpdateCRDetailsFragment : DataBindingFragment<FragmentUpdateCRDetailsBinding>(R.layout.fragment_update_c_r_details) {

    private val viewModel: UpdateCRDetailsViewModel by viewModels()

    private val adapter = UpdateCRDetailsRecyclerAdapter()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        enterTransition = MaterialSharedAxis(MaterialSharedAxis.Z, true)
        exitTransition = MaterialSharedAxis(MaterialSharedAxis.Z, true)
        initializeProperties()
        initializeSpinners()
        initializeRecyclerView()
        initializeObservers()
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        menu.add("Add CR Details")
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.title == "Add CR Details") {
            findNavController().navigate(
                R.id.action_updateCRDetails_to_addOrEditCRDetailsDialog,
                bundleOf("action" to "add")
            )
        }
        return super.onOptionsItemSelected(item)
    }

    fun fetchCRProfiles() {
        viewModel.fetchCRList(
            binding.level.spinner.selectedItem.toString(),
            binding.term.spinner.selectedItem.toString(),
            binding.shift.spinner.selectedItem.toString(),
            binding.campus.spinner.selectedItem.toString()
        )
    }

    private fun initializeObservers() {

        findNavController().getBackStackEntry(R.id.updateCRDetails)
            .savedStateHandle
            .getLiveData<Boolean>("reload")
            .observe(viewLifecycleOwner) {
                viewModel.fetchCRList(
                    binding.level.spinner.selectedItem.toString(),
                    binding.term.spinner.selectedItem.toString(),
                    binding.shift.spinner.selectedItem.toString(),
                    binding.campus.spinner.selectedItem.toString()
                )
            }

        viewModel.toast.observe(viewLifecycleOwner) { makeToast(it) }
        viewModel.crProfileList.observe(viewLifecycleOwner) { adapter.submitItems(it) }
    }

    private fun initializeRecyclerView() {

        binding.recyclerView.adapter = adapter

        adapter.longClickListener = { profile ->
            AlertDialog.Builder(requireContext()).apply {
                setItems(arrayOf("Edit", "Delete")) { _ , position ->
                    if (position == 0) {
                        findNavController().navigate(
                            R.id.action_updateCRDetails_to_addOrEditCRDetailsDialog,
                            bundleOf("action" to "update", "data" to profile.fromPOJOToJSON())
                        )
                    } else if (position == 1) {
                        viewModel.deleteCRProfile(
                            profile.pk.toString(),
                            binding.level.spinner.selectedItem.toString(),
                            binding.term.spinner.selectedItem.toString(),
                            binding.shift.spinner.selectedItem.toString(),
                            binding.campus.spinner.selectedItem.toString()
                        )
                    }
                }
            }.create().show()
        }

        adapter.sendMailClickListener = { email ->
            val intent = Intent(Intent.ACTION_SENDTO)
            intent.data = Uri.parse("mailto:")
            intent.putExtra(Intent.EXTRA_EMAIL, arrayOf(email))
            try {
                startActivity(Intent.createChooser(intent, "Send mail..."))
            } catch (e: ActivityNotFoundException) {
                Timber.e(e)
                makeToast(e.message)
            }
        }
        adapter.dialClickListener = { contactNo ->
            val intent = Intent(Intent.ACTION_DIAL)
            intent.data = Uri.parse("tel:$contactNo")
            startActivity(intent)
        }
    }

    private fun initializeProperties() {
        setHasOptionsMenu(true)
        binding.presenter = this
        binding.viewModel = viewModel
    }

    @SuppressLint("SetTextI18n")
    private fun initializeSpinners() {
        binding.level.title.text = "Level"
        binding.level.spinner.adapter = ArrayAdapter(
            requireContext(),
            R.layout.spinner_item,
            HelperClass.getLevels()
        ).apply { setDropDownViewResource(R.layout.spinner_dropdown_item) }

        binding.term.title.text = "Term"
        binding.term.spinner.adapter = ArrayAdapter(
            requireContext(),
            R.layout.spinner_item,
            HelperClass.getTerms()
        ).apply { setDropDownViewResource(R.layout.spinner_dropdown_item) }

        binding.shift.title.text = "Shift"
        binding.shift.spinner.adapter = ArrayAdapter(
            requireContext(),
            R.layout.spinner_item,
            HelperClass.getShifts()
        ).apply { setDropDownViewResource(R.layout.spinner_dropdown_item) }

        binding.campus.title.text = "Campus"
        binding.campus.spinner.adapter = ArrayAdapter(
            requireContext(),
            R.layout.spinner_item,
            HelperClass.getCampuses()
        ).apply { setDropDownViewResource(R.layout.spinner_dropdown_item) }
    }

}