package bd.edu.daffodilvarsity.classmanager.features.teacherprofilelist.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import bd.edu.daffodilvarsity.classmanager.common.models.ProfileTeacher
import bd.edu.daffodilvarsity.classmanager.databinding.ListItemTeacherProfilesBinding

class TeacherProfileListRecyclerAdapter: PagingDataAdapter<ProfileTeacher, RecyclerView.ViewHolder>(PROFILE_COMPARATOR) {
    
    var onLongClickListener: ((ProfileTeacher) -> Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val binding = ListItemTeacherProfilesBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as ViewHolder).bind(getItem(position), this)
    }

    fun onLongClick(profile: ProfileTeacher): Boolean {
        onLongClickListener?.invoke(profile)
        return true
    }

    class ViewHolder(private val binding: ListItemTeacherProfilesBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bind(profile: ProfileTeacher?, adapter: TeacherProfileListRecyclerAdapter) {
            binding.item = profile
            binding.adapter = adapter
            binding.executePendingBindings()
        }
    }

    companion object {

        private val PROFILE_COMPARATOR = object : DiffUtil.ItemCallback<ProfileTeacher>() {

            override fun areItemsTheSame(oldItem: ProfileTeacher, newItem: ProfileTeacher): Boolean {
                return oldItem.employeeID == newItem.employeeID
            }

            override fun areContentsTheSame(oldItem: ProfileTeacher, newItem: ProfileTeacher): Boolean {
                return oldItem == newItem
            }

        }
    }
}