package bd.edu.daffodilvarsity.classmanager.features.profileteacher.viewprofile

import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.navigation.findNavController
import bd.edu.daffodilvarsity.classmanager.R
import bd.edu.daffodilvarsity.classmanager.common.extensions.makeToast
import bd.edu.daffodilvarsity.classmanager.common.models.DataBindingFragment
import bd.edu.daffodilvarsity.classmanager.common.models.ProfileTeacher
import bd.edu.daffodilvarsity.classmanager.databinding.FragmentTeacherProfileBinding
import bd.edu.daffodilvarsity.classmanager.features.routine.viewmodels.RoutineSharedViewModel
import bd.edu.daffodilvarsity.classmanager.utils.sharedprefutils.ProfileSharedPrefHelper
import com.google.android.material.transition.MaterialSharedAxis
import dagger.hilt.android.AndroidEntryPoint

/**
 * [TeacherProfileFragment] Fragment responsible for
 * showing teacher profile.
 */
@AndroidEntryPoint
class TeacherProfileFragment : DataBindingFragment<FragmentTeacherProfileBinding>(R.layout.fragment_teacher_profile) {

    private val sharedViewModel: RoutineSharedViewModel by activityViewModels()

    private val viewModel: TeacherProfileViewModel by viewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        enterTransition = MaterialSharedAxis(MaterialSharedAxis.Z, true)
        exitTransition = MaterialSharedAxis(MaterialSharedAxis.Z, true)
        setHasOptionsMenu(true)

        binding.viewmodel = viewModel
        initializeTitles()
        viewModel.initializeProfile(ProfileSharedPrefHelper.getTeacherProfile(requireContext()))
        viewModel.loadProfile()
        initializeObservers()
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.fragment_student_profile, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if(item.itemId == R.id.edit_profile) {
            view?.findNavController()?.navigate(R.id.teacherProfileFragment_to_editProfileTeacher)
        }
        return super.onOptionsItemSelected(item)
    }

    private fun initializeObservers() {
        viewModel.profile.observe(viewLifecycleOwner, {profile ->
            ProfileSharedPrefHelper.saveTeacherProfile(requireContext(),profile)
            initializeDescriptions(profile)
            sharedViewModel.loadTeacherClasses(profile.initial)
        })
        viewModel.toast.observe(viewLifecycleOwner, {
            makeToast(it)
        })
    }

    private fun initializeDescriptions(profile: ProfileTeacher) {
        binding.name.description.text = profile.name
        binding.email.description.text = profile.email
        binding.employeeId.description.text = profile.employeeID
        binding.initial.description.text = profile.initial
        binding.department.description.text = profile.department
        binding.designation.description.text = profile.designation
        binding.bookCount.description.text = profile.bookCount.toString()
        binding.contactNo.description.text = profile.contactNo
    }

    private fun initializeTitles() {
        binding.name.title.text = getString(R.string.name)
        binding.email.title.text = getString(R.string.email)
        binding.employeeId.title.text = getString(R.string.employee_id)
        binding.initial.title.text = getString(R.string.teacher_initial)
        binding.department.title.text = getString(R.string.department)
        binding.designation.title.text = getString(R.string.designation)
        binding.bookCount.title.text = getString(R.string.book_count)
        binding.contactNo.title.text = getString(R.string.contact_no)
    }

}
