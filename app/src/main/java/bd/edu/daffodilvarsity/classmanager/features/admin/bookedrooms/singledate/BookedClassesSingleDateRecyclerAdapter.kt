package bd.edu.daffodilvarsity.classmanager.features.admin.bookedrooms.singledate

import bd.edu.daffodilvarsity.classmanager.R
import bd.edu.daffodilvarsity.classmanager.common.adapter.SingleItemBaseAdapter
import bd.edu.daffodilvarsity.classmanager.common.models.BookedRoom
import java.util.*

class BookedClassesSingleDateRecyclerAdapter: SingleItemBaseAdapter<BookedRoom>() {

    private val bookedClasses: MutableList<BookedRoom> = mutableListOf()

    var onCancelClick: ((id: BookedRoom) -> Unit)? = null

    fun submitData(updatedItems: List<BookedRoom>) {
        bookedClasses.clear()
        bookedClasses.addAll(updatedItems)
        notifyDataSetChanged()
    }

    override fun clickedItem(item: BookedRoom) {
        onCancelClick?.invoke(item)
    }

    override fun getItemForPosition(position: Int): BookedRoom {
        return bookedClasses[position]
    }

    override fun getLayoutIdForPosition(position: Int): Int {
        return R.layout.list_item_booked_classes_single_date
    }

    override fun getItemCount(): Int {
        return bookedClasses.size
    }

    fun checkIfCancelable(item: BookedRoom): Boolean {
        val today = Calendar.getInstance().time
        return item.reservationDate.after(today)
    }

}