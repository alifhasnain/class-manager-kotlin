package bd.edu.daffodilvarsity.classmanager.common.models

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import java.util.*


@Entity
@JsonClass(generateAdapter = true)
data class BookedRoom(
    @PrimaryKey val id: Long,
    @field:Json(name = "course_code") val courseCode: String,
    @field:Json(name = "course_name") val courseName: String,
    val room: String,
    val section: String,
    @field:Json(name = "start_time") val startTime: Int,
    @field:Json(name = "end_time") val endTime: Int,
    val shift: String,
    val campus: String,
    val department: String,
    @field:Json(name = "teacher_name") val teacherName: String,
    @field:Json(name = "teacher_initial") val teacherInitial: String,
    @field:Json(name = "teacher_email") val teacherEmail: String,
    @field:Json(name = "reservation_date") val reservationDate: Date,
    @field:Json(name = "time_when_booked") val timeWhenBooked: Date
)