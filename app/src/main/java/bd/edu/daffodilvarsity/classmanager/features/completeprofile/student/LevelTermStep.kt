package bd.edu.daffodilvarsity.classmanager.features.completeprofile.student

import android.view.View
import android.view.ViewGroup
import android.widget.NumberPicker
import ernestoyaquello.com.verticalstepperform.Step

/*
* Level & Term Input Step For Students
*/
class LevelTermStep(private val stepName: String) : Step<String>(stepName) {

    private val input by lazy {
        NumberPicker(context).apply {
            minValue = 1
            maxValue = if (stepName == "Level") { 4 } else { 3 }
            val params = ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
            )
            layoutParams = params
        }
    }

    override fun createStepContentLayout(): View {
        input.setOnValueChangedListener { _, _, _ ->
            markAsCompletedOrUncompleted(true)
        }
        return input
    }

    override fun isStepDataValid(stepData: String?): IsDataValid {
        val inputNumber = input.value.toString()
        if (inputNumber.isEmpty()) {
            return IsDataValid(false)
        }
        return if (stepName == "Level") {
            val level = inputNumber.toInt()
            if (level in 1..4) {
                IsDataValid(true)
            } else {
                IsDataValid(false)
            }
        } else {
            val term = inputNumber.toInt()
            if (term in 1..3) {
                IsDataValid(true)
            } else {
                IsDataValid(false)
            }
        }
    }

    override fun onStepMarkedAsCompleted(animated: Boolean) {

    }

    override fun getStepDataAsHumanReadableString(): String {
        return input.value.toString()
    }

    override fun getStepData(): String {
        return input.value.toString()
    }

    override fun onStepOpened(animated: Boolean) {

    }

    override fun onStepMarkedAsUncompleted(animated: Boolean) {

    }

    override fun onStepClosed(animated: Boolean) {

    }

    override fun restoreStepData(data: String?) {
        input.value = data?.toInt() ?: 0
    }
}