package bd.edu.daffodilvarsity.classmanager.features.profileteacher.viewprofile

import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.*
import bd.edu.daffodilvarsity.classmanager.common.extensions.networkErrorMessage
import bd.edu.daffodilvarsity.classmanager.common.extensions.setValueThenNullify
import bd.edu.daffodilvarsity.classmanager.common.models.ProfileTeacher
import bd.edu.daffodilvarsity.classmanager.common.repository.Repository
import kotlinx.coroutines.launch

class TeacherProfileViewModel @ViewModelInject constructor(
    private val repo: Repository,
    @Assisted private val savedStateHandle: SavedStateHandle
): ViewModel() {

    private val _profile = MutableLiveData<ProfileTeacher>()
    val profile: LiveData<ProfileTeacher> = _profile

    private val _toast = MutableLiveData<String>()
    val toast: LiveData<String> = _toast

    private val _isRefreshing = MutableLiveData<Boolean>()
    val isRefreshing: LiveData<Boolean> = _isRefreshing

    fun initializeProfile(profile: ProfileTeacher) { _profile.value = profile }

    fun loadProfile() = safeScope { _profile.value = repo.fetchTeacherProfileFromServer() }

    private fun safeScope(block: (suspend () -> Unit)) = viewModelScope.launch {
        try {
            _isRefreshing.value = true
            block()
        } catch (e: Exception) {
            _toast.setValueThenNullify(e.networkErrorMessage)
        } finally {
            _isRefreshing.value = false
        }
    }

}