package bd.edu.daffodilvarsity.classmanager.features.admin.bookedrooms.rangeddate

import bd.edu.daffodilvarsity.classmanager.R
import bd.edu.daffodilvarsity.classmanager.common.adapter.SingleItemBaseAdapter
import bd.edu.daffodilvarsity.classmanager.common.models.BookedRoom

class BookedRoomsRangedSearchRecyclerAdapter: SingleItemBaseAdapter<BookedRoom>() {

    private val bookedRooms: MutableList<BookedRoom> = mutableListOf()

    fun submitItems(updatedItems: List<BookedRoom>) {
        bookedRooms.clear()
        bookedRooms.addAll(updatedItems)
        notifyDataSetChanged()
    }

    override fun clickedItem(item: BookedRoom) {

    }

    override fun getItemForPosition(position: Int): BookedRoom {
        return bookedRooms[position]
    }

    override fun getLayoutIdForPosition(position: Int): Int {
        return R.layout.list_item_booked_rooms_ranged_date_search
    }

    override fun getItemCount(): Int {
        return bookedRooms.size
    }
}