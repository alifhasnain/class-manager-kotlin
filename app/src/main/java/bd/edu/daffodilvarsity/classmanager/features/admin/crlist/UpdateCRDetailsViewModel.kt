package bd.edu.daffodilvarsity.classmanager.features.admin.crlist

import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.*
import bd.edu.daffodilvarsity.classmanager.common.extensions.networkErrorMessage
import bd.edu.daffodilvarsity.classmanager.common.extensions.setValueThenNullify
import bd.edu.daffodilvarsity.classmanager.common.models.CRProfile
import bd.edu.daffodilvarsity.classmanager.common.models.LoadStates
import bd.edu.daffodilvarsity.classmanager.common.repository.Repository
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.launch
import kotlinx.coroutines.supervisorScope

class UpdateCRDetailsViewModel @ViewModelInject constructor(
    private val repo: Repository,
    @Assisted private val savedStateHandle: SavedStateHandle
): ViewModel() {

    private val _uiState = MutableLiveData<LoadStates>(LoadStates.Empty("Empty"))
    val uiState: LiveData<LoadStates> = _uiState

    private val _crProfileList = MutableLiveData<List<CRProfile>>()
    val crProfileList: LiveData<List<CRProfile>> = _crProfileList

    val toast: MutableLiveData<String> = MutableLiveData()

    fun fetchCRList(level: String, term: String, shift: String, campus: String) = safeScope {
        _crProfileList.value = repo.getCRDetailsList(level, term, shift, campus)
        _uiState.value = LoadStates.decideEmptyOrNot(crProfileList.value, "Nothing found")
    }

    fun addCRProfile(profile: CRProfile) = safeScope {
        _uiState.value = LoadStates.Loading
        val responseMessage = repo.addCRProfile(profile)
        toast.value = responseMessage.message
        _uiState.value = LoadStates.NotEmpty
    }

    fun updateCRProfile(profile: CRProfile) = safeScope {
        _uiState.value = LoadStates.Loading
        val responseMessage = repo.updateCRProfile(profile)
        toast.value = responseMessage.message
        _uiState.value = LoadStates.NotEmpty
    }

    fun deleteCRProfile(pk: String, level: String, term: String, shift: String, campus: String) = safeScope {
        val handler = CoroutineExceptionHandler { _, throwable ->
            toast.value = (throwable as Exception).networkErrorMessage
        }
        _uiState.value = LoadStates.Loading
        supervisorScope {
            launch(handler) {
                val responseMessage = repo.deleteCRProfile(pk)
                toast.value = responseMessage.message
            }.join()
            fetchCRList(level, term, shift, campus)
        }
    }

    private fun safeScope(block: (suspend () -> Unit)) = viewModelScope.launch {
        try {
            _uiState.value = LoadStates.Loading
            block()
        } catch (e: Exception) {
            toast.setValueThenNullify(e.networkErrorMessage)
            _uiState.value = LoadStates.Error("Error while loading")
        }
    }
}