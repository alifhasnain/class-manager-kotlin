package bd.edu.daffodilvarsity.classmanager.common.network

import bd.edu.daffodilvarsity.classmanager.common.models.*
import bd.edu.daffodilvarsity.classmanager.utils.HelperClass
import okhttp3.ResponseBody
import retrofit2.http.*

interface UserApi {

    @GET("check-profile-completion")
    suspend fun checkProfileCompletion(@Header("Authorization") accessToken: String): ResponseBody

    @FormUrlEncoded
    @POST("faculty-login")
    suspend fun loginFaculty(
        @Field("employer_id") id: String,
        @Field("password") password: String,
        @Field("clear_old_tokens") logoutFromOtherDevices: Boolean
    ): ResponseBody


    @FormUrlEncoded
    @POST("student-login")
    suspend fun loginStudent(
        @Field("student_id") id: String,
        @Field("password") password: String,
        @Field("clear_old_tokens") logoutFromOtherDevices: Boolean
    ): ResponseBody

    @POST("refresh-token")
    suspend fun renewAccessToken(@Header("Authorization") refreshToken: String): ResponseBody

    @GET("courses-list")
    suspend fun getCourseList(
        @Header("Authorization") accessToken: String,
        @Query("department") department: String = "Computer Science & Engineering"
    ): MutableList<Course>

    @GET("courses-list")
    suspend fun getCourseList(
        @Header("Authorization") accessToken: String,
        @Query("department") department: String = "Computer Science & Engineering",
        @Query("level") level: String,
        @Query("term") term: String,
        @Query("shift") shift: String
    ): MutableList<Course>

    @GET("section-list")
    suspend fun getSectionList(@Header("Authorization") accessToken: String): ResponseBody

    @GET("profile")
    suspend fun getStudentProfile(
        @Header("Authorization") accessToken: String
    ): ProfileStudent

    @GET("profile")
    suspend fun getTeacherProfile(
        @Header("Authorization") accessToken: String
    ): ProfileTeacher

    @POST("profile")
    suspend fun editProfile(
        @Header("Authorization") accessToken: String,
        @Body student: ProfileStudent
    ): ProfileStudent

    @POST("profile")
    suspend fun editProfile(
        @Header("Authorization") accessToken: String,
        @Body teacher: ProfileTeacher
    ): ProfileTeacher

    @POST("complete-profile")
    suspend fun completeProfile(
        @Header("Authorization") accessToken: String,
        @Body student: ProfileStudent
    ): ResponseMessage

    @POST("complete-profile")
    suspend fun completeProfile(
        @Header("Authorization") accessToken: String,
        @Body teacher: ProfileTeacher
    ): ResponseMessage

    @GET("registered-courses")
    suspend fun getRegisteredCourses(
        @Header("Authorization") accessToken: String
    ): MutableList<RegisteredCourse>

    @POST("update-courses/add")
    suspend fun addCourse(
        @Header("Authorization") accessToken: String,
        @Body course: RegisteredCourse
    ): MutableList<RegisteredCourse>

    @POST("update-courses/remove")
    suspend fun removeCourse(
        @Header("Authorization") accessToken: String,
        @Body course: RegisteredCourse
    ): MutableList<RegisteredCourse>

    @FormUrlEncoded
    @POST("update-courses")
    suspend fun updateCourses(
        @Header("Authorization") accessToken: String,
        @Field("department") department: String,
        @Field("shift") shift: String,
        @Field("section") section: String,
        @Field("level") level: String,
        @Field("term") term: String
    ): MutableList<RegisteredCourse>

    @GET("routine")
    suspend fun getWholeRoutine(
        @Header("Authorization") accessToken: String,
        @Query("whole") wholeRoutine: String = "true"
    ): MutableList<ClassDetails>

    @GET("book-room")
    suspend fun getAvailableRoomForBook(
        @Header("Authorization") accessToken: String,
        @Query("start_time") startTime: Int,
        @Query("end_time") endTime: Int,
        @Query("campus") campus: String,
        @Query("reservation_date") reservationDateTimeStamp: Long,
        @Query("department") department: String = HelperClass.CSE_DEPARTMENT
    ): MutableList<ClassDetails>

    @POST("book-room")
    suspend fun bookRoomTeacher(
        @Header("Authorization") accessToken: String,
        @Body roomBookingInfo: RoomBookingInfo
    ): ResponseMessage

    @POST("cancel-book")
    suspend fun cancelBook(
        @Header("Authorization") accessToken: String,
        @Query("book_id") bookID: String
    ): ResponseMessage

    @GET("booked-rooms")
    suspend fun teacherBookedRoms(@Header("Authorization") accessToken: String): List<BookedRoom>

    @GET("teacher-profiles")
    suspend fun fetchTeacherProfileList(
        @Header("Authorization") accessToken: String,
        @Query("queryString") queryString: String?,
        @Query("limit") limit: Int?,
        @Query("page") pageNo: Int?
    ): List<ProfileTeacher>

    @GET("cr-list")
    suspend fun fetchCRList(
        @Header("Authorization") accessToken: String,
        @Query("level") level: String,
        @Query("term") term: String,
        @Query("shift") shift: String,
        @Query("campus") campus: String
    ): List<CRProfile>

    @POST("update-fcm-token-student")
    suspend fun updateFCMToken(
        @Header("Authorization") accessToken: String,
        @Query("token") token: String
    ): ResponseMessage

    @POST("sign-out")
    suspend fun signOut(
        @Header("Authorization") refreshToken: String,
    ): ResponseMessage

    @GET("course-version")
    suspend fun getCourseVersion(
        @Header("Authorization") accessToken: String
    ): ResponseBody

    @GET("section-version")
    suspend fun getSectionVersion(
        @Header("Authorization") accessToken: String
    ): ResponseBody

    @GET("app-version")
    suspend fun getAppVersion(
        @Header("Authorization") accessToken: String
    ): ResponseBody

    @GET("routine-version")
    suspend fun getRoutineVersion(
        @Header("Authorization") accessToken: String
    ): ResponseBody

}