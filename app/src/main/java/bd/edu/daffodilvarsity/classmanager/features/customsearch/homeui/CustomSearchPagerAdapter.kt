package bd.edu.daffodilvarsity.classmanager.features.customsearch.homeui

import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import bd.edu.daffodilvarsity.classmanager.features.customsearch.courscoderoutine.CourseCodeSearchFragment
import bd.edu.daffodilvarsity.classmanager.features.customsearch.studentroutine.StudentRoutineSearchFragment
import bd.edu.daffodilvarsity.classmanager.features.customsearch.teacherroutine.TeacherRoutineSearchFragment

class CustomSearchPagerAdapter(fragment: Fragment): FragmentStateAdapter(fragment) {

    override fun getItemCount(): Int {
        return 3
    }

    override fun createFragment(position: Int): Fragment {
        return when(position) {
            0 -> TeacherRoutineSearchFragment()
            1 -> StudentRoutineSearchFragment()
            else -> CourseCodeSearchFragment()
        }
    }

}