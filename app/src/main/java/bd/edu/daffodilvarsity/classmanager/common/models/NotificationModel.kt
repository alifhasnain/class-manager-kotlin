package bd.edu.daffodilvarsity.classmanager.common.models

import android.annotation.SuppressLint
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.*

@Entity(tableName = "Notifications")
@SuppressLint("SimpleDateFormat")
data class NotificationModel(
    @PrimaryKey(autoGenerate = true) val id: Long?,
    val title: String,
    val body: String,
    val issuedAt: Date,
    val read: Boolean = false
)