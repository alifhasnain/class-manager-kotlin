package bd.edu.daffodilvarsity.classmanager.features.roombooking.bookedrooms.viewmodel

import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.*
import bd.edu.daffodilvarsity.classmanager.common.extensions.networkErrorMessage
import bd.edu.daffodilvarsity.classmanager.common.extensions.setValueThenNullify
import bd.edu.daffodilvarsity.classmanager.common.models.BookedRoom
import bd.edu.daffodilvarsity.classmanager.common.repository.Repository
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.launch
import kotlinx.coroutines.supervisorScope

class BookedRoomsViewModel @ViewModelInject constructor(
    private val repo: Repository,
    @Assisted savedStateHandle: SavedStateHandle
): ViewModel() {

    val bookedRoom: LiveData<List<BookedRoom>> = liveData {
        emitSource(repo.fetchTeacherBookedClassesFromCache())
    }

    val isRefreshing: MutableLiveData<Boolean> = MutableLiveData()

    val toast: MutableLiveData<String> = MutableLiveData()

    init { refreshBookedRoomsFromServer() }

    fun refreshBookedRoomsFromServer() = safeScope { repo.refreshCachedBookedRoomsFromServer() }

    fun cancelBooking(bookID: String) = safeScope {
        val handler = CoroutineExceptionHandler {_,e ->
            toast.setValueThenNullify((e as Exception).networkErrorMessage)
        }
        supervisorScope {
            launch(handler) {
                toast.setValueThenNullify("Processing your request please wait")
                val response = repo.cancelRoomBookTeacher(bookID)
                toast.setValueThenNullify(response.message)
            }.join()

            launch(handler) {
                refreshBookedRoomsFromServer()
            }
        }
    }

    private fun safeScope(block: (suspend () -> Unit)) = viewModelScope.launch {
        try {
            isRefreshing.value = true
            block()
        } catch (e: Exception) {
            toast.setValueThenNullify(e.networkErrorMessage)
        } finally {
            isRefreshing.value = false
        }
    }
}