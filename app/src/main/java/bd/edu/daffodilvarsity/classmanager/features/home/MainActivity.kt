package bd.edu.daffodilvarsity.classmanager.features.home

import android.annotation.SuppressLint
import android.content.ActivityNotFoundException
import android.content.Intent
import android.graphics.Typeface
import android.net.Uri
import android.os.Bundle
import android.util.TypedValue
import android.view.Gravity
import android.view.MenuItem
import android.widget.TextView
import androidx.activity.viewModels
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import androidx.core.view.GravityCompat
import androidx.lifecycle.lifecycleScope
import androidx.navigation.findNavController
import androidx.work.*
import bd.edu.daffodilvarsity.classmanager.R
import bd.edu.daffodilvarsity.classmanager.common.extensions.*
import bd.edu.daffodilvarsity.classmanager.common.workers.CleanUpAfterLogout
import bd.edu.daffodilvarsity.classmanager.databinding.ActivityMainBinding
import bd.edu.daffodilvarsity.classmanager.features.about.AboutActivity
import bd.edu.daffodilvarsity.classmanager.features.application.DOWNLOAD_PROGRESS_CHANNEL_ID
import bd.edu.daffodilvarsity.classmanager.features.login.SignInActivity
import bd.edu.daffodilvarsity.classmanager.features.routine.viewmodels.RoutineSharedViewModel
import bd.edu.daffodilvarsity.classmanager.utils.sharedprefutils.*
import bd.edu.daffodilvarsity.classmanager.utils.sharedprefutils.ThemeSharedPrefHelper.THEME_CYAN
import bd.edu.daffodilvarsity.classmanager.utils.sharedprefutils.ThemeSharedPrefHelper.THEME_DEFAULT
import bd.edu.daffodilvarsity.classmanager.utils.sharedprefutils.ThemeSharedPrefHelper.THEME_NIGHT
import bd.edu.daffodilvarsity.classmanager.utils.sharedprefutils.ThemeSharedPrefHelper.THEME_TEAL
import com.google.android.material.appbar.AppBarLayout
import com.google.android.material.navigation.NavigationView
import com.google.firebase.ktx.Firebase
import com.google.firebase.messaging.ktx.messaging
import com.wada811.viewbinding.viewBinding
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.NonCancellable
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.tasks.await
import kotlinx.coroutines.withContext
import okio.IOException
import timber.log.Timber


@AndroidEntryPoint
class MainActivity : AppCompatActivity() ,NavigationView.OnNavigationItemSelectedListener {

    private var currentTimeInMillis: Long = 0

    private val binding by viewBinding { ActivityMainBinding.bind(it) }

    private val viewModel: RoutineSharedViewModel by viewModels()

    private val navController by lazy { findNavController(R.id.fragment_container) }

    private val notificationCount by lazy {
        try {
            val textView = binding.navigationView.menu.findItem(R.id.notification_student).actionView as TextView
            textView.apply {
                gravity = Gravity.CENTER_VERTICAL
                typeface = Typeface.DEFAULT_BOLD
                val typedValue = TypedValue()
                this.context.theme.resolveAttribute(R.attr.colorAccent, typedValue, true)
                setTextColor(typedValue.data)
            }
        } catch (n : NullPointerException) {
            Timber.e("Notification count was not added because user isn't a teacher/admin")
            null
        } catch (e: Exception) {
            Timber.e(e)
            null
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setupTheme()
        setContentView(R.layout.activity_main)

        if (savedInstanceState == null) { enableToolbarScrolling(true) }
        setUpNavDrawer()

        loadClassesFromLocalDB()
    }

    override fun onStart() {
        super.onStart()

        initializeObservers()
        checkAppVersion()
        checkRoutineVersion()
        checkSectionVersion()
        checkCourseVersion()
        viewModel.updateFCMToken()
    }

    override fun onStop() {
        val notificationManagerCompat = NotificationManagerCompat.from(this)
        notificationManagerCompat.cancel(554)
        super.onStop()
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.notification_student -> {
                navController.navigate(R.id.launch_notification)
                enableToolbarScrolling(true)
                setActionBarTitle("Notification")
            }
            R.id.profile_teacher -> {
                navController.navigate(R.id.launch_teacher_profile)
                enableToolbarScrolling(false)
                setActionBarTitle("Profile")
            }
            R.id.profile_student -> {
                navController.navigate(R.id.launch_student_profile)
                enableToolbarScrolling(false)
                setActionBarTitle("Profile")
            }
            R.id.classes -> {
                lifecycleScope.launch {
                    /*
                    * adding some delay
                    * to reduce lag
                    * */
                    delay(100)
                    navController.navigate(R.id.launch_routine)
                    enableToolbarScrolling(true)
                    setActionBarTitle("Classes")
                }
            }
            R.id.book_classes -> {
                navController.navigate(R.id.launch_book_rooms)
                enableToolbarScrolling(true)
                setActionBarTitle("Book Classes")
            }
            R.id.custom_room_search -> {
                navController.navigate(R.id.launch_custom_search)
                enableToolbarScrolling(true)
                setActionBarTitle("Custom Search")
            }
            R.id.empty_rooms -> {
                navController.navigate(R.id.launch_empty_rooms)
                enableToolbarScrolling(true)
                setActionBarTitle("Empty Rooms")
            }
            R.id.booked_classroom -> {
                navController.navigate(R.id.launch_booked_rooms)
                enableToolbarScrolling(true)
                setActionBarTitle("Booked Rooms")
            }
            R.id.cr_list -> {
                navController.navigate(R.id.launch_cr_list)
                enableToolbarScrolling(false)
                setActionBarTitle("CR List")
            }
            R.id.parse_routine -> {
                navController.navigate(R.id.launch_parse_routine)
                enableToolbarScrolling(false)
                setActionBarTitle("Parse Routine")
            }
            R.id.update_routine -> {
                navController.navigate(R.id.launch_update_routine)
                enableToolbarScrolling(false)
                setActionBarTitle("Update Routine")
            }
            R.id.teacher_profiles_list -> {
                navController.navigate(R.id.launch_teacher_profile_list)
                enableToolbarScrolling(true)
                setActionBarTitle("Teacher Profiles")
            }
            R.id.book_classes_for_others -> {
                navController.navigate(R.id.launch_book_rooms_admin)
                enableToolbarScrolling(true)
                setActionBarTitle("Book Classes")
            }
            R.id.available_courses -> {
                navController.navigate(R.id.launch_available_course)
                enableToolbarScrolling(false)
                setActionBarTitle("Available Courses")
            }
            R.id.available_sections -> {
                navController.navigate(R.id.launch_available_section)
                enableToolbarScrolling(false)
                setActionBarTitle("Available Sections")
            }
            R.id.update_cr -> {
                navController.navigate(R.id.launch_update_cr)
                enableToolbarScrolling(false)
                setActionBarTitle("Update CR")
            }
            R.id.miscellaneous -> {
                navController.navigate(R.id.launch_miscellaneous)
                enableToolbarScrolling(false)
                setActionBarTitle("Miscellaneous")
            }
            R.id.search_booked_classes_date -> {
                navController.navigate(R.id.launch_booked_rooms_by_date)
                enableToolbarScrolling(false)
                setActionBarTitle("Search Classes")
            }
            R.id.search_booked_classes_range -> {
                navController.navigate(R.id.launch_booked_rooms_by_range)
                enableToolbarScrolling(false)
                setActionBarTitle("Search Classes")
            }
            R.id.sign_out -> signOut()

            R.id.report_bug -> {
                sendEmail(arrayOf("hasnain.alif20@gmail.com"), "Bug in Class Manager App", "")
            }
            R.id.about -> startActivity<AboutActivity>()
        }
        binding.drawerLayout.closeDrawer(GravityCompat.START)
        return true
    }

    override fun onBackPressed() {
        if (binding.drawerLayout.isDrawerOpen(GravityCompat.START)) {
            binding.drawerLayout.closeDrawer(GravityCompat.START)
        } else if(getNavBackStackCount() > 0) {
            navController.navigateUp()
        } else {
            if (System.currentTimeMillis() < (currentTimeInMillis + 1500)) {
                super.onBackPressed()
                finish()
            } else {
                currentTimeInMillis = System.currentTimeMillis()
                makeToast("press back again to exit")
            }
        }
    }

    private fun loadClassesFromLocalDB() {

        val userType = AuthSharedPrefHelper.getUserType(this)

        if (userType == AuthSharedPrefHelper.USER_TYPE_ADMIN || userType == AuthSharedPrefHelper.USER_TYPE_TEACHER) {
            val profile = ProfileSharedPrefHelper.getTeacherProfile(this)
            viewModel.loadTeacherClasses(profile.initial)
        } else {
            val profile = ProfileSharedPrefHelper.getStudentProfile(this)
            val registeredCourses = ProfileSharedPrefHelper.getRegisteredCourses(this)
            viewModel.loadStudentClasses(registeredCourses,profile.shift,profile.campus,profile.department)
        }
    }

    private fun checkAppVersion() {
        lifecycleScope.launch {
            runWithoutException {
                val prefAppVersion = VersionSharedPrefHelper.getAppVersion(this@MainActivity)
                val networkAppVersion = viewModel.appVersion()
                if (prefAppVersion!= null && prefAppVersion != networkAppVersion) {
                    AlertDialog.Builder(this@MainActivity).apply {
                        setTitle("Update available!")
                        setMessage("New update available on play store. Please update your app " +
                                "for bug fixed and to get access to other additional features.")
                        setPositiveButton("Update") { _,_ ->
                            openAppInPlayStore()
                        }
                        setNegativeButton("Cancel",null)
                        setNeutralButton("Don't ask for this update") { _,_ ->
                            VersionSharedPrefHelper.saveAppVersion(this@MainActivity)
                        }
                    }.create().show()
                }
            }
        }
    }

    private fun checkRoutineVersion() {
        lifecycleScope.launch {
            runWithoutException {
                val netVersion = viewModel.routineVersion()
                val prefVersion = VersionSharedPrefHelper.getRoutineVersion(this@MainActivity)
                if(netVersion != prefVersion) {
                    viewModel.refreshAllClassesAndCoursesFromServer()
                }
            }
        }
    }

    private fun checkCourseVersion() = lifecycleScope.launch {
        try {

            val courseVersion = viewModel.courseVersion()

            if (courseVersion != VersionSharedPrefHelper.getCourseVersion(this@MainActivity)) {
                try {
                    viewModel.refreshAllCoursesFromServer()
                    VersionSharedPrefHelper.saveCourseVersion(this@MainActivity, courseVersion)
                } catch (e: Exception) {
                    makeToast("Failed to download updated courses. Please check your connection")
                }
            }
        } catch (e: Exception) {
            Timber.e(e)
        }
    }

    private fun checkSectionVersion() = lifecycleScope.launch {

        try {

            val sectionVersion = viewModel.sectionVersion()

            if (sectionVersion != VersionSharedPrefHelper.getSectionVersion(this@MainActivity)) {
                try {
                    val sectionList = viewModel.fetchSectionsFromServer()
                    VersionSharedPrefHelper.saveSectionVersion(this@MainActivity, sectionVersion)
                    SectionSharedPrefHelper.saveSections(this@MainActivity, sectionList)
                } catch (e: Exception) {
                    Timber.e(e)
                    makeToast("Failed to download updated sections. Please check your connection")
                }
            }
        } catch (e: Exception) {
            Timber.e(e)
        }
    }

    private fun setUpNavDrawer() {

        when {
            AuthSharedPrefHelper.getUserType(this) == AuthSharedPrefHelper.USER_TYPE_STUDENT -> {
                binding.navigationView.inflateMenu(R.menu.nav_drawer_menu_student)
            }
            AuthSharedPrefHelper.getUserType(this) == AuthSharedPrefHelper.USER_TYPE_TEACHER -> {
                binding.navigationView.inflateMenu(R.menu.nav_drawer_menu_teacher)
            }
            else -> {
                binding.navigationView.inflateMenu(R.menu.nav_drawer_menu_admin)
            }
        }

        setSupportActionBar(binding.toolbar)
        val drawerToggle = ActionBarDrawerToggle(
            this,
            binding.drawerLayout,
            binding.toolbar,
            R.string.navigation_drawer_open,
            R.string.navigation_drawer_close
        )
        binding.drawerLayout.addDrawerListener(drawerToggle)
        drawerToggle.syncState()

        binding.navigationView.setNavigationItemSelectedListener(this)
        binding.navigationView.setCheckedItem(R.id.classes)

    }

    @SuppressLint("SetTextI18n")
    private fun initializeObservers() {

        viewModel.downloadingRoutine.observe(this, {
            it?.let {
                val (downloading,message) = it
                showDownloadingNotification(downloading,message)
            }
        })

        viewModel.routineVersion.observe(this, { version ->
            VersionSharedPrefHelper.saveRoutineVersion(this,version)
        })

        viewModel.notificationCount().observe(this) { count ->
            if (count > 10) {
                notificationCount?.text = "10+"
            } else {
                notificationCount?.text = count.toString()
            }
        }
    }

    private fun showDownloadingNotification(downloading: Boolean, message: String) {
        val notification = NotificationCompat.Builder(
            applicationContext,
            DOWNLOAD_PROGRESS_CHANNEL_ID
        ).apply {
            setContentTitle("Routine Download")
            setContentText(message)
            setSmallIcon(R.drawable.ic_download)
            setProgress(0,0,downloading)
            priority = NotificationCompat.PRIORITY_HIGH
            setSound(null)
        }.build()

        NotificationManagerCompat.from(this).notify(554,notification)

    }

    private fun setupTheme() {
        when(ThemeSharedPrefHelper.getCurrentTheme(this)) {
            THEME_DEFAULT -> setTheme(R.style.AppTheme_Default)
            THEME_NIGHT -> setTheme(R.style.AppTheme_Night)
            THEME_CYAN -> setTheme(R.style.AppTheme_Cyan)
            THEME_TEAL -> setTheme(R.style.AppTheme_Teal)
        }
    }

    private fun enableToolbarScrolling(enable: Boolean) {
        val params = binding.toolbar.layoutParams as AppBarLayout.LayoutParams
        if (enable) {
            params.scrollFlags =
                AppBarLayout.LayoutParams.SCROLL_FLAG_SCROLL or AppBarLayout.LayoutParams.SCROLL_FLAG_ENTER_ALWAYS
        } else {
            params.scrollFlags = 0
        }
        binding.toolbar.layoutParams = params
    }

    private fun openAppInPlayStore() {
        val appPackageName = packageName
        try {
            startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=$appPackageName")))
        } catch (anfe: ActivityNotFoundException) {
            Timber.e(anfe)
            startActivity(
                Intent(
                    Intent.ACTION_VIEW,
                    Uri.parse("https://play.google.com/store/apps/details?id=$appPackageName")
                )
            )
        }
    }

    private fun getNavBackStackCount(): Int {
        val navHostFragment = supportFragmentManager.findFragmentById(R.id.fragment_container)
        return navHostFragment?.childFragmentManager?.backStackEntryCount ?: 0
    }

    private fun signOut() {
        makeToast("Signing out")
        lifecycleScope.launch {
            try {
                cleanUpExistingData(AuthSharedPrefHelper.getRefreshToken(applicationContext))
                AuthSharedPrefHelper.clearAuthSharedPref(applicationContext)
                ProfileSharedPrefHelper.clearProfileSharedPref(applicationContext)
                VersionSharedPrefHelper.clearSharedPref(applicationContext)
                viewModel.deleteNotificationAndBookedClasses()
            } catch (io: IOException) {
                Timber.e(io)
                makeToast(io.message)
            } catch (e: Exception) {
                makeToast(e.message)
                Timber.e(e)
            } finally {
                withContext(NonCancellable) {
                    runWithoutException { Firebase.messaging.deleteToken().await() }
                }
                clearStackAndStartActivity<SignInActivity>()
            }
        }
    }

    private fun cleanUpExistingData(refreshToken: String) {

        val data = Data.Builder().apply {
            putString("refreshToken", refreshToken)
        }.build()

        val workConstraints = Constraints.Builder().apply {
            setRequiredNetworkType(NetworkType.CONNECTED)
        }.build()

        val workRequest = OneTimeWorkRequest.Builder(CleanUpAfterLogout::class.java).apply {
            setInputData(data)
            setConstraints(workConstraints)
        }.build()

        WorkManager.getInstance(this).enqueue(workRequest)

    }
}
