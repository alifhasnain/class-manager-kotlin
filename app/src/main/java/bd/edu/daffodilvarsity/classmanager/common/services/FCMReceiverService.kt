package bd.edu.daffodilvarsity.classmanager.common.services

import android.annotation.SuppressLint
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import androidx.navigation.NavDeepLinkBuilder
import bd.edu.daffodilvarsity.classmanager.R
import bd.edu.daffodilvarsity.classmanager.common.database.ClassManagerDao
import bd.edu.daffodilvarsity.classmanager.common.extensions.runWithoutException
import bd.edu.daffodilvarsity.classmanager.common.models.NotificationModel
import bd.edu.daffodilvarsity.classmanager.features.application.FCM_NOTIFICATION_CHANNEL_ID
import bd.edu.daffodilvarsity.classmanager.features.home.MainActivity
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import dagger.hilt.EntryPoint
import dagger.hilt.InstallIn
import dagger.hilt.android.EntryPointAccessors
import dagger.hilt.android.components.ApplicationComponent
import timber.log.Timber
import java.text.SimpleDateFormat
import java.util.*
import kotlin.random.Random

@SuppressLint("MissingFirebaseInstanceTokenRefresh")
class FCMReceiverService: FirebaseMessagingService() {

    @EntryPoint
    @InstallIn(ApplicationComponent::class)
    interface FCMReceiverServiceEntryPoint {
        fun getDao(): ClassManagerDao
    }

    override fun onMessageReceived(message: RemoteMessage) {
        super.onMessageReceived(message)
        runWithoutException { onReceiveDataMessage(message.data) }
        runWithoutException { onReceiveNotificationMessage(message.notification!!) }
    }

    private fun onReceiveNotificationMessage(notificationMessage: RemoteMessage.Notification) {
        val notificationManager = NotificationManagerCompat.from(applicationContext)

        val notification = NotificationCompat.Builder(applicationContext, FCM_NOTIFICATION_CHANNEL_ID).apply {
            setContentTitle(notificationMessage.title)
            setContentText(notificationMessage.body)
            setSmallIcon(R.drawable.ic_notification_active)
            setStyle(NotificationCompat.BigTextStyle().bigText(notificationMessage.body))
            setCategory(NotificationCompat.CATEGORY_REMINDER)
            setAutoCancel(true)
            priority = NotificationCompat.PRIORITY_HIGH
        }.build()

        notificationManager.notify(Random(4000).nextInt(), notification)
    }

    @SuppressLint("SimpleDateFormat")
    private fun onReceiveDataMessage(data: Map<String,String>) {
        val formatter = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
        formatter.timeZone = TimeZone.getTimeZone("GMT")
        val notification = NotificationModel(null,data["title"]!!,data["message"]!!,formatter.parse(data["time"]!!)!!)

        val hiltEntryPoint = EntryPointAccessors.fromApplication(applicationContext, FCMReceiverServiceEntryPoint::class.java)
        val routineDAO = hiltEntryPoint.getDao()

        routineDAO.insertNotification(notification)
        showNotification(notification.title, notification.body)
        Timber.e(notification.toString())
    }

    private fun showNotification(title: String, message: String) {

        val notificationManager = NotificationManagerCompat.from(applicationContext)

        val pendingIntent = NavDeepLinkBuilder(applicationContext).apply {
            setComponentName(MainActivity::class.java)
            setGraph(R.navigation.nav_graph_home)
            setDestination(R.id.notificationFragment)
        }.createPendingIntent()

        val notification = NotificationCompat.Builder(applicationContext, FCM_NOTIFICATION_CHANNEL_ID).apply {
            setContentTitle(title)
            setContentText(message)
            setSmallIcon(R.drawable.ic_notification_active)
            setStyle(NotificationCompat.BigTextStyle().bigText(message))
            setCategory(NotificationCompat.CATEGORY_REMINDER)
            setAutoCancel(true)
            setContentIntent(pendingIntent)
            priority = NotificationCompat.PRIORITY_HIGH
        }.build()

        notificationManager.notify(Random(4000).nextInt(), notification)
    }
}