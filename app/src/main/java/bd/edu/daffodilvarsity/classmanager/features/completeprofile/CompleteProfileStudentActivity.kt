package bd.edu.daffodilvarsity.classmanager.features.completeprofile

import android.os.Bundle
import android.view.View
import androidx.lifecycle.lifecycleScope
import bd.edu.daffodilvarsity.classmanager.R
import bd.edu.daffodilvarsity.classmanager.common.auth.Authenticator
import bd.edu.daffodilvarsity.classmanager.common.extensions.clearStackAndStartActivity
import bd.edu.daffodilvarsity.classmanager.common.extensions.hideKeyboard
import bd.edu.daffodilvarsity.classmanager.common.extensions.makeToast
import bd.edu.daffodilvarsity.classmanager.common.extensions.runWithoutException
import bd.edu.daffodilvarsity.classmanager.common.models.ProfileStudent
import bd.edu.daffodilvarsity.classmanager.common.models.ViewBindingAppCompatActivity
import bd.edu.daffodilvarsity.classmanager.common.repository.Repository
import bd.edu.daffodilvarsity.classmanager.databinding.ActivityCompleteProfileStudentBinding
import bd.edu.daffodilvarsity.classmanager.features.completeprofile.common.UneditableStep
import bd.edu.daffodilvarsity.classmanager.features.completeprofile.student.CampusStep
import bd.edu.daffodilvarsity.classmanager.features.completeprofile.student.LevelTermStep
import bd.edu.daffodilvarsity.classmanager.features.completeprofile.student.SectionStep
import bd.edu.daffodilvarsity.classmanager.features.completeprofile.student.ShiftStep
import bd.edu.daffodilvarsity.classmanager.features.home.MainActivity
import bd.edu.daffodilvarsity.classmanager.utils.sharedprefutils.AuthSharedPrefHelper
import bd.edu.daffodilvarsity.classmanager.utils.sharedprefutils.ProfileSharedPrefHelper
import bd.edu.daffodilvarsity.classmanager.utils.sharedprefutils.SectionSharedPrefHelper
import bd.edu.daffodilvarsity.classmanager.utils.sharedprefutils.VersionSharedPrefHelper
import com.auth0.android.jwt.JWT
import dagger.hilt.android.AndroidEntryPoint
import ernestoyaquello.com.verticalstepperform.Step
import ernestoyaquello.com.verticalstepperform.listener.StepperFormListener
import kotlinx.coroutines.async
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.launch
import okio.IOException
import timber.log.Timber
import javax.inject.Inject

@AndroidEntryPoint
class CompleteProfileStudentActivity : ViewBindingAppCompatActivity<ActivityCompleteProfileStudentBinding>(R.layout.activity_complete_profile_student,ActivityCompleteProfileStudentBinding::bind), StepperFormListener {

    @Inject lateinit var repo: Repository

    @Inject lateinit var authenticator: Authenticator

    private lateinit var steps: List<Step<String>>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (savedInstanceState == null) { safeScope { initializeStepperForm() } }

    }


    override fun onSaveInstanceState(savedInstanceState: Bundle) {

        if (::steps.isInitialized) {
            savedInstanceState.putString("campus", steps[4].stepData ?: "")
            savedInstanceState.putString("level", steps[5].stepData ?: "")
            savedInstanceState.putString("term", steps[6].stepData ?: "")
            savedInstanceState.putString("section", steps[7].stepData ?: "")
            savedInstanceState.putString("shift", steps[8].stepData ?: "")
        }

        super.onSaveInstanceState(savedInstanceState)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) = safeScope {

        initializeStepperForm()

        if (savedInstanceState.containsKey("student_id")) {
            steps[0].restoreStepData(savedInstanceState.getString("student_id"))
        }

        if (savedInstanceState.containsKey("campus")) {
            steps[4].restoreStepData(savedInstanceState.getString("campus"))
        }

        if (savedInstanceState.containsKey("level")) {
            steps[5].restoreStepData(savedInstanceState.getString("level"))
        }

        if (savedInstanceState.containsKey("term")) {
            steps[6].restoreStepData(savedInstanceState.getString("term"))
        }

        if (savedInstanceState.containsKey("section")) {
            steps[7].restoreStepData(savedInstanceState.getString("section"))
        }

        if (savedInstanceState.containsKey("shift")) {
            steps[8].restoreStepData(savedInstanceState.getString("shift"))
        }

        super.onRestoreInstanceState(savedInstanceState)
    }

    override fun onCompletedForm() = safeScope {

        makeToast("Saving data please wait")

        runWithoutException { hideKeyboard() }

        val student = ProfileStudent(
            steps[0].stepData,
            steps[1].stepData,
            steps[2].stepData,
            steps[7].stepData,
            steps[5].stepData,
            steps[6].stepData,
            steps[3].stepData,
            steps[4].stepData,
            steps[8].stepData
        )
        repo.refreshAllCoursesFromServer()

        val courseVersion = coroutineScope { async { repo.getCourseVersion() } }

        val sectionVersion = coroutineScope { async { repo.getSectionVersion() } }

        val sections = coroutineScope { async { repo.fetchAllSectionFromServer() } }

        val responseMessage = repo.completeStudentProfile(student)

        if (responseMessage.success) {

            val registeredCourses = repo.fetchRegisteredCourses()

            ProfileSharedPrefHelper.saveRegisteredCourses(this,registeredCourses)

            VersionSharedPrefHelper.saveCourseVersion(this, courseVersion.await())
            VersionSharedPrefHelper.saveSectionVersion(this, sectionVersion.await())
            SectionSharedPrefHelper.saveSections(this, sections.await())

            AuthSharedPrefHelper.saveSignedIn(this)
            ProfileSharedPrefHelper.saveStudentProfile(this,student)

            makeToast(responseMessage.message)

            clearStackAndStartActivity<MainActivity>()
        }
    }

    override fun onCancelledForm() {

    }

    private suspend fun initializeStepperForm() {
        val studentMap = getStudentData()
        steps = listOf(
            UneditableStep(
                "Student ID",
                studentMap["student_id"] ?: "Error"
            ),
            UneditableStep(
                "Name",
                studentMap["name"] ?: "Error"
            ),
            UneditableStep(
                "Email",
                studentMap["email"] ?: "Error"
            ),
            UneditableStep(
                "Department",
                "Computer Science & Engineering"
            ),
            CampusStep(),
            LevelTermStep("Level"),
            LevelTermStep("Term"),
            SectionStep(),
            ShiftStep()
        )

        binding.stepperForm.setup(this@CompleteProfileStudentActivity, steps).init()
        binding.stepperForm.visibility = View.VISIBLE

        binding.stepperForm.markStepAsCompleted(0, true)
        binding.stepperForm.markStepAsCompleted(1, true)
        binding.stepperForm.markStepAsCompleted(2, true)
        binding.stepperForm.markStepAsCompleted(3, true)
        binding.stepperForm.goToStep(4, true)
    }

    private suspend fun getStudentData(): Map<String, String> {
        val jwt = JWT(authenticator.getAccessToken())
        return jwt.claims.entries.associate {
            it.key to it.value.asString()!!
        }
    }

    private fun safeScope(block : (suspend() -> Unit)) {
        lifecycleScope.launch {
            try {
                block.invoke()
            }
            catch (io : IOException){
                makeToast(io.message ?: "Error occurred")
                binding.stepperForm.cancelFormCompletionOrCancellationAttempt()
                Timber.e(io)
            } catch (e: Exception) {
                binding.stepperForm.cancelFormCompletionOrCancellationAttempt()
                makeToast(e.message ?: "")
                Timber.e(e)
            }
        }
    }
}
