package bd.edu.daffodilvarsity.classmanager.features.completeprofile.student

import android.view.View
import android.view.ViewGroup
import android.widget.NumberPicker
import ernestoyaquello.com.verticalstepperform.Step
/*
* Shift Input Step For Students
*/
class ShiftStep : Step<String>("Shift") {

    private val shiftPicker: NumberPicker by lazy {
        NumberPicker(context).apply {
            val shifts = arrayOf("Day","Evening")
            minValue = 0
            maxValue = shifts.size - 1
            displayedValues = shifts

            val params = ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
            )
            layoutParams = params
        }
    }

    override fun createStepContentLayout(): View {
        shiftPicker.setOnValueChangedListener { _, _, _ ->
            markAsCompletedOrUncompleted(true)
        }
        return shiftPicker
    }

    override fun isStepDataValid(stepData: String?): IsDataValid {
        return if (stepData == "Day" || stepData == "Evening") {
            IsDataValid(true)
        } else {
            IsDataValid(false)
        }
    }

    override fun getStepData(): String {
        return shiftPicker.displayedValues[shiftPicker.value]
    }

    override fun getStepDataAsHumanReadableString(): String {
        return shiftPicker.displayedValues[shiftPicker.value]
    }

    override fun onStepMarkedAsCompleted(animated: Boolean) {

    }

    override fun onStepMarkedAsUncompleted(animated: Boolean) {

    }

    override fun onStepOpened(animated: Boolean) {

    }

    override fun onStepClosed(animated: Boolean) {

    }

    override fun restoreStepData(data: String?) {
        shiftPicker.value = shiftPicker.displayedValues.indexOf(data)
    }
}
