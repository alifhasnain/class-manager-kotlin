package bd.edu.daffodilvarsity.classmanager.features.profilestudent.editprofile

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.View
import android.widget.ArrayAdapter
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import bd.edu.daffodilvarsity.classmanager.R
import bd.edu.daffodilvarsity.classmanager.common.extensions.bindSpinner
import bd.edu.daffodilvarsity.classmanager.common.extensions.makeToast
import bd.edu.daffodilvarsity.classmanager.common.models.DataBindingFragment
import bd.edu.daffodilvarsity.classmanager.common.models.ProfileStudent
import bd.edu.daffodilvarsity.classmanager.databinding.FragmentEditProfileStudentBinding
import bd.edu.daffodilvarsity.classmanager.features.routine.viewmodels.RoutineSharedViewModel
import bd.edu.daffodilvarsity.classmanager.utils.HelperClass
import bd.edu.daffodilvarsity.classmanager.utils.sharedprefutils.ProfileSharedPrefHelper
import bd.edu.daffodilvarsity.classmanager.utils.sharedprefutils.SectionSharedPrefHelper
import dagger.hilt.android.AndroidEntryPoint

/**
 * A simple [Fragment] subclass.
 */
@SuppressLint("SetTextI18n")
@AndroidEntryPoint
class EditProfileStudent : DataBindingFragment<FragmentEditProfileStudentBinding>(R.layout.fragment_edit_profile_student) {

    private val sharedViewModel: RoutineSharedViewModel by activityViewModels()

    private val viewModel: EditProfileStudentViewModel by viewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.presenter = this
        binding.viewmodel = viewModel

        val profile = ProfileSharedPrefHelper.getStudentProfile(requireContext())
        viewModel.initializeProfile(profile)
        initializeSpinners(profile)

        initializeTitles()
        initializeViewModelObservers()
    }

    fun saveProfile() {

        val profileOld = ProfileSharedPrefHelper.getStudentProfile(requireContext())

        val profile = ProfileStudent(
            binding.id.description.text.toString(),
            binding.name.description.text.toString(),
            binding.email.description.text.toString(),
            binding.section.spinner.selectedItem.toString(),
            binding.level.spinner.selectedItem.toString(),
            binding.term.spinner.selectedItem.toString(),
            binding.department.spinner.selectedItem.toString(),
            binding.campus.spinner.selectedItem.toString(),
            binding.shift.spinner.selectedItem.toString()
        )

        //makeToast(binding.shift.spinner.selectedItem.toString())

        if(profile.shift != profileOld.shift || profile.section != profileOld.section || profile.level != profileOld.level || profile.term != profileOld.term) {
            val dialog = AlertDialog.Builder(requireContext())
                .setTitle("Please confirm")
                .setMessage("Do you want to change your courses according to your level & term?")
                .setPositiveButton("yes") { _, _ ->
                    viewModel.saveProfileAndUpdateCourses(profile)
                }
                .setNegativeButton("no") { _, _ ->
                    viewModel.saveProfile(profile)
                }.create()
            dialog.show()
        } else {
            viewModel.saveProfile(profile)
        }
    }

    private fun initializeViewModelObservers() {

        viewModel.profile.observe(viewLifecycleOwner, { profile ->
            ProfileSharedPrefHelper.saveStudentProfile(requireContext(),profile)
            val courses = ProfileSharedPrefHelper.getRegisteredCourses(requireContext())
            sharedViewModel.loadStudentClasses(courses, profile.shift, profile.campus, profile.department)
            initializeDescription(profile)
        })

        viewModel.registeredCourse.observe(viewLifecycleOwner, {updatedCourses ->
            ProfileSharedPrefHelper.saveRegisteredCourses(requireContext(), updatedCourses)
            val profile = ProfileSharedPrefHelper.getStudentProfile(requireContext())
            sharedViewModel.loadStudentClasses(updatedCourses, profile.shift, profile.campus, profile.department)
        })

        viewModel.toast.observe(viewLifecycleOwner, { makeToast(it) })
    }

    private fun initializeTitles() {
        binding.name.title.text = "Name"
        binding.id.title.text = "Student ID"
        binding.email.title.text = "Email"
    }

    private fun initializeDescription(profile: ProfileStudent) {
        binding.name.description.text = profile.name
        binding.id.description.text = profile.id
        binding.email.description.text = profile.email
    }

    @Suppress("UNCHECKED_CAST")
    private fun initializeSpinners(profile: ProfileStudent?) {

        binding.department.title.text = "Department"
        binding.department.spinner.bindSpinner(HelperClass.getDepartments())

        binding.campus.title.text = "Campus"
        binding.campus.spinner.bindSpinner(HelperClass.getCampuses())

        binding.shift.title.text = "Shift"
        binding.shift.spinner.bindSpinner(HelperClass.getShifts())

        binding.section.title.text = "Section"
        binding.section.spinner.bindSpinner(SectionSharedPrefHelper.getSections(requireContext()))

        binding.level.title.text = "Level"
        binding.level.spinner.bindSpinner(HelperClass.getLevels())

        binding.term.title.text = "Term"
        binding.term.spinner.bindSpinner(HelperClass.getTerms())

        profile?.let {
            binding.department.spinner.setSelection(
                (binding.department.spinner.adapter as ArrayAdapter<String>).getPosition(profile.department)
            )
            binding.campus.spinner.setSelection(
                (binding.campus.spinner.adapter as ArrayAdapter<String>).getPosition(profile.campus)
            )
            binding.shift.spinner.setSelection(
                (binding.shift.spinner.adapter as ArrayAdapter<String>).getPosition(profile.shift)
            )
            binding.section.spinner.setSelection(
                (binding.section.spinner.adapter as ArrayAdapter<String>).getPosition(profile.section)
            )
            binding.level.spinner.setSelection(
                (binding.level.spinner.adapter as ArrayAdapter<String>).getPosition(profile.level)
            )
            binding.term.spinner.setSelection(
                (binding.term.spinner.adapter as ArrayAdapter<String>).getPosition(profile.term)
            )
        }
    }

}
