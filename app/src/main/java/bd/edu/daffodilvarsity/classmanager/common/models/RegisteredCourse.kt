package bd.edu.daffodilvarsity.classmanager.common.models

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class RegisteredCourse(
    @field:Json(name = "course_code") val courseCode : String,
    val section : String
)