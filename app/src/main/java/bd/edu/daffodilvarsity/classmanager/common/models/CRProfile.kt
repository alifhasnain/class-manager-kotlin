package bd.edu.daffodilvarsity.classmanager.common.models

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class CRProfile(
    val pk: Long = 0,
    val id: String,
    val name: String,
    val email: String,
    val level: Int,
    val term: Int,
    val section: String,
    @field:Json(name = "phone_no") val phoneNo: String,
    val shift: String,
    val campus: String,
    val department: String
)