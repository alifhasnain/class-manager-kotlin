package bd.edu.daffodilvarsity.classmanager.features.routine.viewmodels

import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.*
import androidx.sqlite.db.SimpleSQLiteQuery
import bd.edu.daffodilvarsity.classmanager.common.extensions.networkErrorMessage
import bd.edu.daffodilvarsity.classmanager.common.extensions.setValueThenNullify
import bd.edu.daffodilvarsity.classmanager.common.models.ClassDetails
import bd.edu.daffodilvarsity.classmanager.common.models.RegisteredCourse
import bd.edu.daffodilvarsity.classmanager.common.repository.Repository
import com.google.firebase.messaging.FirebaseMessaging
import kotlinx.coroutines.async
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.launch
import kotlinx.coroutines.tasks.await

class RoutineSharedViewModel @ViewModelInject constructor(
    private val repo: Repository,
    @Assisted private val savedStateHandle: SavedStateHandle
): ViewModel() {

    var userClasses: Flow<List<ClassDetails>>? = null

    private val _downloadingRoutine: MutableLiveData<Pair<Boolean,String>> = MutableLiveData()

    val downloadingRoutine: LiveData<Pair<Boolean,String>> = _downloadingRoutine
    private val _showProgressBar: MutableLiveData<Boolean> = MutableLiveData()

    val showProgressBar: LiveData<Boolean> = _showProgressBar
    private val _routineVersion: MutableLiveData<String> = MutableLiveData()

    val routineVersion: LiveData<String> = _routineVersion

    val toast: MutableLiveData<String> = MutableLiveData()

    fun loadStudentClasses(registeredCourse: MutableList<RegisteredCourse>, shift: String, campus: String, department: String) {
        if (registeredCourse.isEmpty()) {
            userClasses = flow { emit(emptyList<ClassDetails>()) }
            return
        }
        val queryString = generateQueryString(registeredCourse,shift,campus,department)
        val sqliteQuery = SimpleSQLiteQuery(queryString)
        userClasses = repo.getStudentRoutineAsFlow(sqliteQuery)
    }

    fun loadTeacherClasses(initial: String) {
        userClasses = repo.getTeacherWholeRoutineAsFlow(initial)
    }

    fun toggleNotification(id: Long, notification: Boolean) {
        viewModelScope.launch { repo.toggleNotification(id,notification) }
    }

    fun refreshAllClassesAndCoursesFromServer() = viewModelScope.launch {
        var success = true
        try {
            coroutineScope {
                _showProgressBar.value = true
                _downloadingRoutine.value = true to "Downloading routine"
                val routineVersion = async { repo.getRoutineVersion() }
                repo.updateRoutineAndCourseFromDatabase()
                _routineVersion.value = routineVersion.await()
            }
        } catch (e: Exception) {
            toast.setValueThenNullify(e.networkErrorMessage)
            success = false
        } finally {
            _showProgressBar.value = false
            if(success) {
                _downloadingRoutine.value = false to "Download complete"
            } else {
                _downloadingRoutine.value = false to "Download Failed"
            }
            //Setting the value to null so we stop showing unnecessary notification
            // while we turn off & turn on screen
            _downloadingRoutine.value = null
        }
    }

    suspend fun refreshAllCoursesFromServer() = repo.refreshAllCoursesFromServer()

    suspend fun fetchSectionsFromServer() = repo.fetchAllSectionFromServer()

    fun updateFCMToken() = viewModelScope.launch {
        try {
            delay(2000)
            val token = FirebaseMessaging.getInstance().token.await()
            repo.updateFCMToken(token)
        } catch (e: Exception) {
            e.networkErrorMessage
        }
    }

    fun notificationCount() = repo.notificationCount()

    suspend fun deleteNotificationAndBookedClasses() = repo.deleteNotificationsAndBookedRooms()

    suspend fun appVersion() = repo.getAppVersion()

    suspend fun routineVersion() = repo.getRoutineVersion()

    suspend fun courseVersion() = repo.getCourseVersion()

    suspend fun sectionVersion() = repo.getSectionVersion()

    private fun generateQueryString(registeredCourse: MutableList<RegisteredCourse>,shift: String, campus: String,department: String): String {
        val queryBuilder = StringBuilder("select * from ClassDetails where (")
        registeredCourse.forEachIndexed { index, item ->
            queryBuilder.append("""(courseCode = "${item.courseCode}" and section = "${item.section}")""")
            if(index!=registeredCourse.size-1) {
                queryBuilder.append(" OR ")
            }
        }
        queryBuilder.append(""") AND shift = "$shift" AND campus = "$campus" AND department = "$department" order by startTime""")
        return queryBuilder.toString()
    }

}