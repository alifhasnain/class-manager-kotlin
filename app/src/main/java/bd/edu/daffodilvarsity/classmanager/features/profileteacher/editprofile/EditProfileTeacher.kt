package bd.edu.daffodilvarsity.classmanager.features.profileteacher.editprofile

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import bd.edu.daffodilvarsity.classmanager.R
import bd.edu.daffodilvarsity.classmanager.common.extensions.makeToast
import bd.edu.daffodilvarsity.classmanager.common.models.DataBindingFragment
import bd.edu.daffodilvarsity.classmanager.common.models.ProfileTeacher
import bd.edu.daffodilvarsity.classmanager.databinding.FragmentEditProfileTeacherBinding
import bd.edu.daffodilvarsity.classmanager.features.routine.viewmodels.RoutineSharedViewModel
import bd.edu.daffodilvarsity.classmanager.utils.sharedprefutils.ProfileSharedPrefHelper
import dagger.hilt.android.AndroidEntryPoint

/**
 * A simple [Fragment] subclass.
 */
@AndroidEntryPoint
class EditProfileTeacher : DataBindingFragment<FragmentEditProfileTeacherBinding>(R.layout.fragment_edit_profile_teacher) {

    private val sharedViewModel: RoutineSharedViewModel by activityViewModels()

    private val viewModel: EditProfileTeacherViewModel by viewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.presenter = this
        binding.viewmodel = viewModel

        val profile = ProfileSharedPrefHelper.getTeacherProfile(requireContext())
        viewModel.initializeProfile(profile)
        initializeObservers()

    }

    fun saveProfile() {

        val oldProfile = viewModel.profile.value!!

        viewModel.saveProfile(ProfileTeacher(
            oldProfile.employeeID,
            oldProfile.name,
            oldProfile.email,
            oldProfile.department,
            binding.initial.editText!!.text.toString(),
            oldProfile.designation,
            binding.contactNo.editText!!.text.toString(),
            oldProfile.bookCount
        ))
    }

    private fun initializeObservers() {

        viewModel.profile.observe(viewLifecycleOwner, { profile ->
            ProfileSharedPrefHelper.saveTeacherProfile(requireContext(), profile)
            populateViews(profile)
            sharedViewModel.loadTeacherClasses(profile.initial)
        })

        viewModel.toast.observe(viewLifecycleOwner, {
            makeToast(it)
        })

    }

    private fun populateViews(profile: ProfileTeacher) {
        binding.name.editText?.setText(profile.name)
        binding.email.editText?.setText(profile.email)
        binding.employeeId.editText?.setText(profile.employeeID)
        binding.initial.editText?.setText(profile.initial)
        binding.designation.editText?.setText(profile.designation)
        binding.department.editText?.setText(profile.department)
        binding.contactNo.editText?.setText(profile.contactNo)
    }

}
