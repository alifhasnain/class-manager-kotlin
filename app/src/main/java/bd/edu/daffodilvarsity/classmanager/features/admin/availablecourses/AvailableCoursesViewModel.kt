package bd.edu.daffodilvarsity.classmanager.features.admin.availablecourses

import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.*
import bd.edu.daffodilvarsity.classmanager.common.extensions.networkErrorMessage
import bd.edu.daffodilvarsity.classmanager.common.models.Course
import bd.edu.daffodilvarsity.classmanager.common.models.LoadStates
import bd.edu.daffodilvarsity.classmanager.common.repository.Repository
import bd.edu.daffodilvarsity.classmanager.utils.Event
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.launch
import kotlinx.coroutines.supervisorScope

class AvailableCoursesViewModel @ViewModelInject constructor(
    private val repo: Repository,
    @Assisted private val savedStateHandle: SavedStateHandle
) : ViewModel() {

    private val _toast = MutableLiveData<Event<String>>()
    val toast: LiveData<Event<String>> = _toast

    private val _uiStates = MutableLiveData<LoadStates>(LoadStates.Empty("No item"))
    val uiStates: LiveData<LoadStates> = _uiStates

    private val _courses = MutableLiveData<List<Course>>()
    val courses: LiveData<List<Course>> = _courses

    private var queryParameters: Triple<String, String, String>? = null

    fun fetchCourses(level: String, term: String, shift: String) = safeScope {
        queryParameters = Triple(level, term, shift)
        _courses.value = repo.getCourseListFromServer(level, term, shift)
        _uiStates.value = LoadStates.decideEmptyOrNot(courses.value, "Nothing found")
    }

    private fun fetchCoursesWithCachedQueryData() = safeScope {
        queryParameters?.let { (level, term, shift) ->
            _uiStates.value = LoadStates.Loading
            _courses.value = repo.getCourseListFromServer(level, term, shift)
            _uiStates.value = LoadStates.decideEmptyOrNot(courses.value, "Nothing found")
        }
    }

    fun addCourse(course: Course) = safeScope {

        val handler = CoroutineExceptionHandler { _,e ->
            _toast.value = Event((e as Exception).networkErrorMessage)
        }

        supervisorScope {
            launch(handler) {
                val responseMessage = repo.addCourse(course)
                _toast.value = Event(responseMessage.message)
            }.join()

            fetchCoursesWithCachedQueryData()
        }
    }

    fun updateCourse(course: Course) = safeScope {
        val handler = CoroutineExceptionHandler { _,e ->
            _toast.value = Event((e as Exception).networkErrorMessage)
        }

        supervisorScope {
            launch(handler) {
                val responseMessage = repo.updateCourse(course)
                _toast.value = Event(responseMessage.message)
            }.join()

            fetchCoursesWithCachedQueryData()
        }
    }

    fun deleteCourse(course: Course) = safeScope {

        val handler = CoroutineExceptionHandler { _,e ->
            _toast.value = Event((e as Exception).networkErrorMessage)
        }

        supervisorScope {

            launch(handler) {
                val responseMessage = repo.deleteCourse(course.id)
                _toast.value = Event(responseMessage.message)
            }.join()

            fetchCoursesWithCachedQueryData()

        }
    }

    private fun safeScope(block: suspend () -> Unit)  = viewModelScope.launch {
        try {
            _uiStates.value = LoadStates.Loading
            block()
        } catch (e: Exception) {
            _toast.value = Event(e.networkErrorMessage)
            _uiStates.value = LoadStates.Error("Failed to load data")
        }
    }

}