package bd.edu.daffodilvarsity.classmanager.features.crlist

import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.*
import bd.edu.daffodilvarsity.classmanager.common.extensions.networkErrorMessage
import bd.edu.daffodilvarsity.classmanager.common.extensions.setValueThenNullify
import bd.edu.daffodilvarsity.classmanager.common.models.CRProfile
import bd.edu.daffodilvarsity.classmanager.common.models.LoadStates
import bd.edu.daffodilvarsity.classmanager.common.repository.Repository
import kotlinx.coroutines.launch

class CRListViewModel @ViewModelInject constructor(
    private val repo: Repository,
    @Assisted private val savedStateHandle: SavedStateHandle
): ViewModel() {

    private val _uiState = MutableLiveData<LoadStates>(LoadStates.Empty("Empty"))
    val uiState: LiveData<LoadStates> = _uiState

    val toast: MutableLiveData<String> = MutableLiveData()

    private val _crProfileList = MutableLiveData<List<CRProfile>>()
    val crProfileList: LiveData<List<CRProfile>> = _crProfileList

    fun fetchCRList(level: String, term: String, shift: String, campus: String) = safeScope {
        _crProfileList.value = repo.getCRDetailsList(level, term, shift, campus)
        _uiState.value = LoadStates.decideEmptyOrNot(crProfileList.value, "Nothing found")
    }

    private fun safeScope(block: (suspend () -> Unit)) = viewModelScope.launch {
        try {
            _uiState.value = LoadStates.Loading
            block()
        } catch (e: Exception) {
            toast.setValueThenNullify(e.networkErrorMessage)
            _uiState.value = LoadStates.Error("Error while loading")
        }
    }

}