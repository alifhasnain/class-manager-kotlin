package bd.edu.daffodilvarsity.classmanager.features.admin.availablesections

import bd.edu.daffodilvarsity.classmanager.R
import bd.edu.daffodilvarsity.classmanager.common.adapter.SingleItemBaseAdapter

class AvailableSectionRecyclerAdapter: SingleItemBaseAdapter<Pair<String, String>>() {

    var longClickListener: ((Pair<String,String>) -> Unit)? = null

    private val sections = mutableListOf<Pair<String,String>>()

    fun updateItems(updatedSections: List<Pair<String,String>>) {
        sections.clear()
        sections.addAll(updatedSections)
        notifyDataSetChanged()
    }

    fun onLongClick(section: Pair<String,String>): Boolean {
        longClickListener?.invoke(section)
        return true
    }

    override fun clickedItem(item: Pair<String,String>) {

    }

    override fun getItemForPosition(position: Int): Pair<String,String> = sections[position]

    override fun getLayoutIdForPosition(position: Int): Int = R.layout.list_item_available_sections

    override fun getItemCount(): Int = sections.size
}