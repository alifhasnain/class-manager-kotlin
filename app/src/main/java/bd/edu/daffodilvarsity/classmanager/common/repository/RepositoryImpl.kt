package bd.edu.daffodilvarsity.classmanager.common.repository

import androidx.lifecycle.LiveData
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import androidx.sqlite.db.SimpleSQLiteQuery
import bd.edu.daffodilvarsity.classmanager.common.auth.Authenticator
import bd.edu.daffodilvarsity.classmanager.common.database.ClassManagerDao
import bd.edu.daffodilvarsity.classmanager.common.models.*
import bd.edu.daffodilvarsity.classmanager.common.network.AdminApi
import bd.edu.daffodilvarsity.classmanager.common.network.UserApi
import bd.edu.daffodilvarsity.classmanager.features.teacherprofilelist.paging.TeacherProfilePagingSource
import bd.edu.daffodilvarsity.classmanager.utils.HelperClass
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.withContext
import org.json.JSONArray
import org.json.JSONObject

@Suppress("BlockingMethodInNonBlockingContext")
class RepositoryImpl (
    private val authenticator: Authenticator,
    private val userClient: UserApi,
    private val dao: ClassManagerDao,
    private val adminClient: AdminApi
): Repository {

    private val token: (suspend () -> String) = { "Bearer ${authenticator.getAccessToken()}" }

    /*
    * Functions related with USER (Teacher/Student)
    * */

    // Common

    override suspend fun updateFCMToken(token: String) = userClient.updateFCMToken(token(), token)

    override suspend fun getAllSectionListFromServer() = withContext(Dispatchers.IO) {
        val sections = mutableListOf<Pair<String,String>>()
        val sectionsJsonArray = JSONArray(userClient.getSectionList(token()).string())
        for (i in 0 until sectionsJsonArray.length()) {
            sections.add(sectionsJsonArray.getJSONObject(i).getString("id") to sectionsJsonArray.getJSONObject(i).getString("section"))
        }
        sections
    }

    override fun getTeacherProfileStream(
        queryString: String,
        limit: Int
    ): Flow<PagingData<ProfileTeacher>> = Pager(
        config = PagingConfig(pageSize = limit),
        pagingSourceFactory = { TeacherProfilePagingSource(queryString, authenticator, userClient) }
    ).flow

    override suspend fun getCourseListFromServer(level: String, term: String, shift: String) = userClient.getCourseList(token(), level = level, term = term, shift = shift)

    // Teachers

    override suspend fun completeTeacherProfile(profile: ProfileTeacher) = userClient.completeProfile(token(), profile)

    override suspend fun fetchTeacherProfileFromServer(): ProfileTeacher = userClient.getTeacherProfile(token())

    override suspend fun saveTeacherProfileToServer(profile: ProfileTeacher): ProfileTeacher = userClient.editProfile(token(), profile)

    override suspend fun getCRDetailsList(level: String, term: String, shift: String, campus: String) = userClient.fetchCRList(token(), level, term, shift, campus)

    // Students

    override suspend fun completeStudentProfile(profile: ProfileStudent) = userClient.completeProfile(token(), profile)

    override suspend fun fetchStudentProfileFromServer(): ProfileStudent = userClient.getStudentProfile(token())

    override suspend fun saveStudentProfileToServer(profile: ProfileStudent): ProfileStudent = userClient.editProfile(token(),profile)

    override suspend fun saveProfileAndUpdateRegisteredCourse(profile: ProfileStudent): Pair<ProfileStudent,MutableList<RegisteredCourse>> {
        val updatedProfile = coroutineScope { async { saveStudentProfileToServer(profile) } }
        val updatedCourses = coroutineScope {
            async {
                userClient.updateCourses(
                    token(),
                    profile.department,
                    profile.shift,
                    profile.section,
                    profile.level,
                    profile.term
                )
            }
        }
        return updatedProfile.await() to updatedCourses.await()
    }

    override suspend fun fetchRegisteredCourses() = userClient.getRegisteredCourses(token())

    override suspend fun addRegisteredCourse(course: RegisteredCourse) = userClient.addCourse(token(),course)

    override suspend fun removeRegisteredCourse(course: RegisteredCourse) = userClient.removeCourse(token(), course)

    /*
    * Functions related to Routine
    * */

    /*
    * Fetches courses from locally cached courses
    * */
    override suspend fun getCoursesFromLocal(
        level: Int,
        term: Int,
        shift: String,
        department: String
    ): MutableList<Course> = dao.getCoursesWithLevelTerm(level, term, shift, department)
    /*
    * Fetches courses from locally cached courses
    * */
    override fun getCoursesFromLocalAsLiveData(
        shift: String,
        department: String
    ) = dao.getCoursesWithShiftAndDepartment(shift,department)

    override fun getDistinctSectionsFromLocal() = dao.getDistinctSections()

    override suspend fun getConcatenateCourseCodeAndCourseName(
        shift: String,
        department: String
    ): List<String> = withContext(Dispatchers.Default) {
        val courses = dao.getAllCourses(shift, department)
        courses.map {
            "${it.courseCode} : ${it.courseName}"
        }
    }

    override suspend fun updateRoutineAndCourseFromDatabase() {
        val accessToken = token()
        val classes = coroutineScope { async { userClient.getWholeRoutine(accessToken) } }
        val courses = coroutineScope { async { userClient.getCourseList(accessToken) } }
        dao.refreshAllClasses(classes.await())
        dao.refreshAllCourses(courses.await())
    }

    override suspend fun refreshAllCoursesFromServer() {
        val courses = userClient.getCourseList(token())
        dao.refreshAllCourses(courses)
    }

    override suspend fun fetchAllSectionFromServer(): MutableList<String> {
        val sections = mutableListOf<String>()
        val sectionJSONArray = JSONArray(userClient.getSectionList(token()).string())
        for (i in 0 until sectionJSONArray.length()) {
            sections.add(sectionJSONArray.getJSONObject(i).getString("section"))
        }
        return sections
    }

    override fun getTeacherRoutineForSpecificDayAsFlow(initial: String, day: String) = dao.getTeacherRoutine(initial, day)

    override fun getTeacherWholeRoutineAsFlow(initial: String) = dao.getTeacherWholeRoutineAsFlow(initial)

    override suspend fun getTeacherRoutine(
        initial: String,
        day: String
    ): MutableList<ClassDetails> = withContext(Dispatchers.Default) {
        if (day == "All") {
            val classes = dao.getTeacherWholeRoutineAsList(initial)
            HelperClass.sortClassesByWeek(classes)
        } else {
            dao.getTeacherRoutineForSpecificDayAsList(initial, day)
        }
    }

    override fun getStudentRoutineAsFlow(rawQuery: SimpleSQLiteQuery) = dao.getStudentRoutine(rawQuery)

    override suspend fun getStudentRoutineDayOfWeekSorted(
        courseList: MutableList<String>,
        section: String,
        shift: String,
        campus: String,
        department: String
    ): MutableList<ClassDetails> = withContext(Dispatchers.Default) {
        val classes = dao.getStudentRoutine(courseList, section, shift, campus, department)
        HelperClass.sortClassesByWeek(classes)
    }

    override suspend fun getRoutineWithCourseCode(
        courseCode: String,
        shift: String,
        campus: String,
        department: String
    ): MutableList<ClassDetails> = withContext(Dispatchers.Default) {
        val classes = dao.getRoutineWithCourseCode(courseCode, shift, campus, department)
        HelperClass.sortClassesByWeek(classes)
    }

    override suspend fun getCourseVersion(): String {
        val jsonObject = JSONObject(userClient.getCourseVersion(token()).string())
        return jsonObject.getString("version")
    }

    override suspend fun getSectionVersion(): String {
        val jsonObject = JSONObject(userClient.getSectionVersion(token()).string())
        return jsonObject.getString("version")
    }

    override suspend fun getRoutineVersion(): String {
        val jsonObj = JSONObject(userClient.getRoutineVersion(token()).string())
        return jsonObj.getString("version")
    }

    override suspend fun getAppVersion(): String {
        val jsonObject = JSONObject(userClient.getAppVersion(token()).string())
        return jsonObject.getString("version")
    }

    override suspend fun getEmptyRooms(
        startTime: Int,
        endTime: Int,
        campus: String,
        day: String
    ): MutableList<ClassDetails> = dao.getEmptyRooms(day,startTime,endTime,campus)

    override suspend fun toggleNotification(id: Long, notification: Boolean) = dao.toggleNotification(id, notification)

    //Function related to notification

    override suspend fun deleteNotificationsAndBookedRooms() {
        dao.deleteAllFCMNotifications()
        dao.deleteAllBookedClasses()
    }

    override suspend fun insertNotification(notification: NotificationModel) = dao.insertNotification(notification)

    override fun getNotifications() = dao.getNotifications()

    override fun notificationCount() = dao.getUnreadNotificationCount()

    override suspend fun updateReadStatus(pk: Long, read: Boolean) = dao.updateReadStatus(pk,read)

    override suspend fun markAllNotificationAsRead() = dao.markAllAsRead()

    override suspend fun clearAllNotifications() = dao.deleteAllFCMNotifications()

    /*
    * Functions Related to Room Booking
    * */
    override suspend fun getAvailableRoomForBook(
        startTime: Int,
        endTime: Int,
        campus: String,
        reservationTimeStamp: Long
    ): List<Pair<Long,String>> = withContext(Dispatchers.Default) {
        val availableRooms = userClient.getAvailableRoomForBook(token(),startTime,endTime,campus,reservationTimeStamp)
        availableRooms.map { it.id to it.room }
    }

    override suspend fun bookRoomTeacher(roomBookingInfo: RoomBookingInfo): ResponseMessage
            = userClient.bookRoomTeacher(token(), roomBookingInfo)

    override suspend fun bookRoomAdmin(bookingInfo: RoomBookingInfo): ResponseMessage
            = adminClient.bookRoomAdmin(token(), bookingInfo)

    override suspend fun cancelRoomBookTeacher(bookID: String): ResponseMessage = userClient.cancelBook(token(),bookID)

    override fun fetchTeacherBookedClassesFromCache(): LiveData<List<BookedRoom>> = dao.getAllBookedClasses()

    override suspend fun refreshCachedBookedRoomsFromServer() {
        val bookedClasses = userClient.teacherBookedRoms(token())
        dao.refreshAllBookedClasses(bookedClasses)
    }

    /*
    * Function that to Admin can perform
    * */

    override suspend fun uploadRoutine(routineData: MutableMap<String, String>) = adminClient.uploadRoutine(token(), routineData)

    override suspend fun fetchRoutine(startTime: Int, endTime: Int, campus: String, day: String) = adminClient.routine(token(),startTime, endTime, campus, day)

    override suspend fun addSingleClass(classDetails: ClassDetails) = adminClient.addClass(token(), classDetails)

    override suspend fun updateSingleClass(classDetails: ClassDetails) = adminClient.updateSingleClass(token(),classDetails)

    override suspend fun deleteSingleClass(id: String) = adminClient.deleteSingleClass(token(),id)

    override suspend fun getBookedRoomsSingleDate(dateTimestamp: Long) = adminClient.getBookedRoomsAtSingleDay(token(),dateTimestamp)

    override suspend fun getBookedRoomsRangedDate(startDate: Long, endDate: Long) = adminClient.getBookedRoomsWithRangedDate(token(), startDate, endDate)

    override suspend fun cancelRoomBookAdmin(id: Long, mailTitle: String, mailBody: String) = adminClient.cancelBook(token(),id,mailTitle,mailBody)

    override suspend fun addCRProfile(profile: CRProfile) = adminClient.addCRProfile(token(), profile)

    override suspend fun updateCRProfile(profile: CRProfile) = adminClient.updateCRProfile(token(), profile)

    override suspend fun deleteCRProfile(pk: String) = adminClient.deleteCRProfile(token(), pk)

    override suspend fun deleteCourse(id: Long) = adminClient.deleteCourse(token(), id)

    override suspend fun addCourse(course: Course) = adminClient.addSingleCourse(token(), course)

    override suspend fun updateCourse(course: Course) = adminClient.updateSingleCourse(token(), course)

    override suspend fun addSection(section: String) = adminClient.addSection(token(), section)

    override suspend fun deleteSection(id: String) = adminClient.deleteSection(token(), id)

    override suspend fun updateRoutineVersion(path: String) = adminClient.updateRoutineVersion(token(), path)

    override suspend fun updateAppVersion(path: String) = adminClient.updateAppVersion(token(), path)

    override suspend fun updateCourseVersion(path: String) = adminClient.updateCourseVersion(token(), path)

    override suspend fun updateSectionVersion(path: String) = adminClient.updateSectionVersion(token(), path)

    override suspend fun resetBookCount() = adminClient.resetBookCount(token())
}