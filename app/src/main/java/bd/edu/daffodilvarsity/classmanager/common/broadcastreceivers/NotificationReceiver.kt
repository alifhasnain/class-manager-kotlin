package bd.edu.daffodilvarsity.classmanager.common.broadcastreceivers

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import bd.edu.daffodilvarsity.classmanager.R
import bd.edu.daffodilvarsity.classmanager.features.application.ROUTINE_REMINDER_CHANNEL_ID

const val REMINDER_NOTIFICATION = 124
class NotificationReceiver: BroadcastReceiver() {

    override fun onReceive(context: Context?, intent: Intent?) {

        val message = intent?.extras?.getString("message", "")

        val notificationManager = NotificationManagerCompat.from(context!!)

        val notification = NotificationCompat.Builder(context, ROUTINE_REMINDER_CHANNEL_ID).apply {
            setSmallIcon(R.drawable.ic_push_notification)
            setContentTitle("Reminder")
            setContentText(message)
            setStyle(NotificationCompat.BigTextStyle().bigText(message))
            setCategory(NotificationCompat.CATEGORY_REMINDER)
            setAutoCancel(true)
            priority = NotificationCompat.PRIORITY_HIGH
        }.build()

        notificationManager.notify(REMINDER_NOTIFICATION, notification)
    }
}