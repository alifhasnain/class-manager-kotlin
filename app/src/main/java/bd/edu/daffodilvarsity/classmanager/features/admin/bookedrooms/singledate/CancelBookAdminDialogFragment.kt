package bd.edu.daffodilvarsity.classmanager.features.admin.bookedrooms.singledate

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment
import bd.edu.daffodilvarsity.classmanager.R
import bd.edu.daffodilvarsity.classmanager.common.models.BookedRoom
import bd.edu.daffodilvarsity.classmanager.databinding.DialogFragmentCancelBookAdminBinding
import bd.edu.daffodilvarsity.classmanager.utils.HelperClass
import com.wada811.viewbinding.viewBinding
import java.text.SimpleDateFormat


class CancelBookAdminDialogFragment(val room: BookedRoom) : DialogFragment() {

    var onConfirmListener: ((Triple<Long, String, String>) -> Unit)? = null

    @SuppressLint("SimpleDateFormat")
    private val dateFormatter = SimpleDateFormat("EEE, d MMM, yyyy")

    private val binding by viewBinding { DialogFragmentCancelBookAdminBinding.bind(it) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NORMAL, R.style.FullScreenDialog)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.dialog_fragment_cancel_book_admin, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.toolbar.setNavigationOnClickListener { dismiss() }
        binding.cancelBook.setOnClickListener { cancelBook() }
        initializeViewData()
    }

    private fun cancelBook() {
        fun sendDataAndDismiss() {
            onConfirmListener?.invoke(
                Triple(
                    room.id,
                    binding.mailTitle.text.toString().trim(),
                    binding.mailBody.text.toString().trim()
                )
            )
            dismiss()
        }
        if(binding.mailTitle.text.toString().isEmpty() || binding.mailBody.text.toString().isEmpty()) {
            AlertDialog.Builder(this.requireContext()).apply {
                setTitle("Please verify")
                setMessage("Email title or body is not included do you want to proceed without sending the teacher any email?")
                setPositiveButton("Proceed") { _, _ ->
                    sendDataAndDismiss()
                }
                setNegativeButton("No", null)
            }.show()
        } else {
            AlertDialog.Builder(this.requireContext()).apply {
                setTitle("Please verify")
                setMessage("Are you sure you want to proceed?")
                setPositiveButton("Proceed") { _, _ ->
                    sendDataAndDismiss()
                }
                setNegativeButton("No", null)
            }.show()
        }
    }

    @SuppressLint("SetTextI18n")
    private fun initializeViewData() {

        binding.time.text = HelperClass.getTimeStringFromSecond(room.startTime) + "-" + HelperClass.getTimeStringFromSecond(room.endTime)
        binding.date.text = dateFormatter.format(room.reservationDate)

        binding.teacherDetails.title.text = "Teacher"
        binding.teacherDetails.description.text = "${room.teacherName} (${room.teacherInitial})"

        binding.teacherEmail.title.text = "Email"
        binding.teacherEmail.description.text = room.teacherEmail

        binding.courseDetails.title.text = "Course"
        binding.courseDetails.description.text = "${room.courseName} (${room.courseCode})"

        binding.room.title.text = "Room"
        binding.room.description.text = room.room

        binding.section.title.text = "Section"
        binding.section.description.text = room.section

        binding.shift.title.text = "Shift"
        binding.shift.description.text = room.shift

        binding.department.title.text = "Department"
        binding.department.description.text = room.department

        binding.campus.title.text = "Campus"
        binding.campus.description.text = room.campus

    }

}