package bd.edu.daffodilvarsity.classmanager.common.models

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class RoomBookingInfo(
    @field:Json(name = "reservation_date")val reservationDateTimestamp: Long,
    @field:Json(name = "class_id") val classPK :String,
    @field:Json(name = "course_code") val courseCode: String,
    @field:Json(name = "course_name") val courseName: String,
    @field:Json(name = "teacher_data") val teacherInfo: String?,
    val section: String,
    val department: String,
    val shift: String
)