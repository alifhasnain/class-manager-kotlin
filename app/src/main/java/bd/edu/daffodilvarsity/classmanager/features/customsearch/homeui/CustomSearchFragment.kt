package bd.edu.daffodilvarsity.classmanager.features.customsearch.homeui

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import bd.edu.daffodilvarsity.classmanager.R
import bd.edu.daffodilvarsity.classmanager.common.extensions.enforceSingleScrollDirection
import bd.edu.daffodilvarsity.classmanager.common.extensions.recyclerView
import bd.edu.daffodilvarsity.classmanager.common.models.DataBindingFragment
import bd.edu.daffodilvarsity.classmanager.common.viewpager2transformers.ViewPager2PageTransformation
import bd.edu.daffodilvarsity.classmanager.databinding.FragmentCustomSearchBinding
import com.google.android.material.tabs.TabLayoutMediator
import com.google.android.material.transition.MaterialSharedAxis
import dagger.hilt.android.AndroidEntryPoint

/**
 * A simple [Fragment] subclass.
 */
@AndroidEntryPoint
class CustomSearchFragment : DataBindingFragment<FragmentCustomSearchBinding>(R.layout.fragment_custom_search) {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        enterTransition = MaterialSharedAxis(MaterialSharedAxis.Z, true)
        exitTransition = MaterialSharedAxis(MaterialSharedAxis.Z, true)

        initializeViewPager()

    }

    private fun initializeViewPager() {
        binding.viewPager.adapter = CustomSearchPagerAdapter(this)
        TabLayoutMediator(binding.tabLayout,binding.viewPager) { tab, position ->
            when(position) {
                0 -> tab.text = "Teacher Routine"
                1 -> tab.text = "Student Routine"
                2 -> tab.text = "Course Code"
            }
        }.attach()
        binding.viewPager.setPageTransformer(ViewPager2PageTransformation())
        binding.viewPager.recyclerView.enforceSingleScrollDirection()
    }

}
