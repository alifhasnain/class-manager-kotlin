package bd.edu.daffodilvarsity.classmanager.features.admin.bookedrooms.rangeddate

import android.annotation.SuppressLint
import android.app.DatePickerDialog
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import bd.edu.daffodilvarsity.classmanager.R
import bd.edu.daffodilvarsity.classmanager.common.extensions.makeToast
import bd.edu.daffodilvarsity.classmanager.common.models.BookedRoom
import bd.edu.daffodilvarsity.classmanager.common.models.DataBindingFragment
import bd.edu.daffodilvarsity.classmanager.databinding.FragmentBookedRoomsRangedDateBinding
import bd.edu.daffodilvarsity.classmanager.features.admin.bookedrooms.common.BookedRoomsAdminViewModel
import bd.edu.daffodilvarsity.classmanager.features.admin.bookedrooms.common.CreateDocumentCustomContract
import bd.edu.daffodilvarsity.classmanager.utils.HelperClass
import com.google.android.material.transition.MaterialSharedAxis
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch
import java.io.FileOutputStream
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*

@AndroidEntryPoint
class BookedRoomsRangedDate : DataBindingFragment<FragmentBookedRoomsRangedDateBinding>(R.layout.fragment_booked_rooms_ranged_date) {

    private val adapter by lazy { BookedRoomsRangedSearchRecyclerAdapter() }

    private val viewModel: BookedRoomsAdminViewModel by viewModels()

    @SuppressLint("SimpleDateFormat")
    private val dateFormatter = SimpleDateFormat("d MMM, yyyy")

    private var startDate: Calendar? = null
    private var endDate: Calendar? = null

    private var csvString : String? = null

    private val createFile =  registerForActivityResult(CreateDocumentCustomContract()) {
        it?.let { saveCSVStringToFile(it) }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setHasOptionsMenu(true)
        enterTransition = MaterialSharedAxis(MaterialSharedAxis.Z, true)
        exitTransition = MaterialSharedAxis(MaterialSharedAxis.Z, true)
        binding.presenter = this
        binding.viewModel = viewModel

        initializeRecyclerView()
        initializeObservers()
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        menu.add("Generate CSV")
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.title == "Generate CSV" && validateInputs()) {
            generateCSVForBookedRooms()
        }
        return super.onOptionsItemSelected(item)
    }

    fun fetchBookedRooms() {
        if (validateInputs()) {
            viewModel.fetchBookedRoomsWithRange(startDate!!.timeInMillis,endDate!!.timeInMillis)
        }
    }

    fun pickStartDate() {
        val todayDate = startDate ?: Calendar.getInstance()
        DatePickerDialog(
            requireContext(),
            { _, year, month, dayOfMonth ->
                val date = GregorianCalendar(year, month, dayOfMonth)
                startDate = date.clone() as Calendar
                binding.startDate.text = dateFormatter.format(date.time)
            },
            todayDate.get(Calendar.YEAR),
            todayDate.get(Calendar.MONTH),
            todayDate.get(Calendar.DAY_OF_MONTH)
        ).show()
    }

    fun pickEndDate() {
        val todayDate = endDate ?: Calendar.getInstance()
        DatePickerDialog(
            requireContext(),
            { _, year, month, dayOfMonth ->
                val date = GregorianCalendar(year, month, dayOfMonth)
                endDate = date.clone() as Calendar
                binding.endDate.text = dateFormatter.format(date.time)
            },
            todayDate.get(Calendar.YEAR),
            todayDate.get(Calendar.MONTH),
            todayDate.get(Calendar.DAY_OF_MONTH)
        ).show()
    }

    private fun generateCSVForBookedRooms() = lifecycleScope.launch {
        val bookedRooms = viewModel.fetchAndReturnBookedRoomsWithRange(startDate!!.timeInMillis, endDate!!.timeInMillis)
        val csv = CSVGenerator()
        csv.addHeader(listOf("Reservation Date", "Time", "Teacher Name", "Teacher Email", "Course Code",
        "Course Name", "Room", "Campus", "Shift", "Section", "Booked At"))
        csv.addLines(bookedRooms)
        csvString = csv.csvString

        createFile.launch(Intent(Intent.ACTION_CREATE_DOCUMENT).apply {
            addCategory(Intent.CATEGORY_OPENABLE)
            type = "text/comma-separated-values"
            putExtra(Intent.EXTRA_TITLE, "From : ${dateFormatter.format(startDate!!.time)} TO : ${dateFormatter.format(endDate!!.time)}")
        })

    }

    private fun saveCSVStringToFile(uri: Uri) {
        try {
            csvString?.let {
                val pfd = requireActivity().contentResolver.openFileDescriptor(uri,"w")
                val fileOutputStream = FileOutputStream(pfd?.fileDescriptor)
                fileOutputStream.write(csvString?.toByteArray())
                csvString = null
                fileOutputStream.close()
                makeToast("File saved")
            }
        } catch (e: IOException) {
            makeToast("Error occurred while saving file")
        }
    }

    private fun validateInputs(): Boolean {
        if(startDate == null || endDate == null ||startDate?.compareTo(endDate) ?: -1 > 0) {
            makeToast("Invalid date range")
            return false
        }
        return true
    }

    private fun initializeObservers() {
        viewModel.bookedRoomsInRangedDate.observe(viewLifecycleOwner) { adapter.submitItems(it) }
    }

    private fun initializeRecyclerView() {
        binding.recyclerView.adapter = adapter
    }

    private class CSVGenerator {

        @SuppressLint("SimpleDateFormat")
        private val dateFormat = SimpleDateFormat("dd/MM/yyyy")

        private val csv = StringBuilder()

        val csvString
            get() = csv.toString()

        fun addHeader(headers: List<String>) {
            var line = ""
            headers.forEachIndexed { index, s ->
                line += if (index != headers.size - 1) {
                    "$s,"
                } else {
                    "$s\n"
                }
            }
            csv.append(line)
        }

        fun addLines(items: List<BookedRoom>) {
            var line = ""
            items.forEachIndexed { index, bookedRoom ->
                line = "${dateFormat.format(bookedRoom.reservationDate)},${HelperClass.getTimeStringFromSecond(bookedRoom.startTime)}" +
                        "-${HelperClass.getTimeStringFromSecond(bookedRoom.endTime)},${bookedRoom.teacherName}," +
                        "${bookedRoom.teacherEmail},${bookedRoom.courseCode},${bookedRoom.courseName}," +
                        "${bookedRoom.room},${bookedRoom.campus},${bookedRoom.shift},${bookedRoom.section}," +
                        "${dateFormat.format(bookedRoom.timeWhenBooked)}\n"
                csv.append(line)
            }
        }

    }


}