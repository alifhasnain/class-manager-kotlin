package bd.edu.daffodilvarsity.classmanager.features.roombooking.bookrooms.ui

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import bd.edu.daffodilvarsity.classmanager.R
import bd.edu.daffodilvarsity.classmanager.common.extensions.fromPOJOToJSON
import bd.edu.daffodilvarsity.classmanager.common.models.RoomBookingInfo
import bd.edu.daffodilvarsity.classmanager.databinding.FragmentFinishBookDialogBinding
import bd.edu.daffodilvarsity.classmanager.features.roombooking.bookrooms.viewmodel.BookRoomViewModel
import bd.edu.daffodilvarsity.classmanager.utils.HelperClass
import bd.edu.daffodilvarsity.classmanager.utils.sharedprefutils.ProfileSharedPrefHelper
import com.wada811.databinding.dataBinding
import dagger.hilt.android.AndroidEntryPoint
import timber.log.Timber


@AndroidEntryPoint
class FinishBookDialogFragment : DialogFragment() {

    private val reservationDate: String
        get() = arguments?.getString("reservationDate") ?: ""

    private val reservationDateTimestamp: Long
        get() = arguments?.getLong("reservationDateTimestamp") ?: 0

    private val room: String
        get() = arguments?.getString("room") ?: ""

    private val time: String
        get() = arguments?.getString("time") ?: ""

    private val pk: String
        get() = arguments?.getString("pk") ?: ""

    private val binding: FragmentFinishBookDialogBinding by dataBinding()

    private val viewModel: BookRoomViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NORMAL, R.style.FullScreenDialog)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        return inflater.inflate(R.layout.fragment_finish_book_dialog, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.presenter = this

        binding.toolbar.setNavigationOnClickListener { dismiss() }

        initializeTitles()
        initializeDescriptions()
        loadTeacherDistinctCourses()
        initializeObservers()
    }

    override fun onStart() {
        super.onStart()
        dialog?.let {
            val width = ViewGroup.LayoutParams.MATCH_PARENT
            val height = ViewGroup.LayoutParams.MATCH_PARENT
            it.window?.setLayout(width,height)
        }
    }

    fun proceedRoomBook() {
        val course = binding.courses.spinner.selectedItem.toString().split(":")
        var courseCode = "Undefined"
        var courseName = "Undefined"
        try {
            courseCode = course[0].trim()
            courseName = course[1].trim()
        } catch (e: Exception) {
            Timber.d(e)
        }

        Timber.e(reservationDateTimestamp.toString())

        val bookRoomInfo = RoomBookingInfo(
            reservationDateTimestamp,
            pk,
            courseCode,
            courseName,
            null,
            binding.sections.spinner.selectedItem.toString(),
            binding.department.spinner.selectedItem.toString(),
            binding.shift.spinner.selectedItem.toString()
        )
        findNavController().previousBackStackEntry?.savedStateHandle?.set("data", bookRoomInfo.fromPOJOToJSON())
        dismiss()
    }

    private fun loadTeacherDistinctCourses() {
        val profile = ProfileSharedPrefHelper.getTeacherProfile(requireContext())
        viewModel.loadDistinctCourseCodesAndNamesByTeacher(profile.initial)
    }

    private fun initializeObservers() {
        viewModel.teacherDistinctCourses.observe(viewLifecycleOwner, {
            binding.courses.spinner.apply {
                adapter = ArrayAdapter(
                    requireContext(),
                    R.layout.spinner_item,
                    it.also { it.add(0,"Undefined") }
                ).apply { setDropDownViewResource(R.layout.spinner_dropdown_item) }
            }
        })
        viewModel.teacherDistinctSections.observe(viewLifecycleOwner, {
            binding.sections.spinner.apply {
                adapter = ArrayAdapter(
                    requireContext(),
                    R.layout.spinner_item,
                    it.also { it.add(0,"Undefined") }
                ).apply { setDropDownViewResource(R.layout.spinner_dropdown_item) }
            }
        })
    }

    @SuppressLint("SetTextI18n")
    private fun initializeTitles() {
        binding.toolbar.title = "Finish Book"
        binding.room.title.text = "Room"
        binding.reservationDate.title.text = "Reservation Date"
        binding.time.title.text = "Time"
        binding.courses.title.text = "Course"
        binding.sections.title.text = "Section"
        binding.shift.title.text = "Shift"
        binding.department.title.text = "Department"
    }

    private fun initializeDescriptions() {
        binding.reservationDate.description.text = reservationDate
        binding.room.description.text = room
        binding.time.description.text = time

        binding.shift.spinner.apply {
            adapter = ArrayAdapter(
                requireContext(),
                R.layout.spinner_item,
                HelperClass.getShifts()
            ).apply { setDropDownViewResource(R.layout.spinner_dropdown_item) }
        }

        binding.department.spinner.apply {
            adapter = ArrayAdapter(
                requireContext(),
                R.layout.spinner_item,
                HelperClass.getDepartments()
            ).apply { setDropDownViewResource(R.layout.spinner_dropdown_item) }
        }
    }

}
