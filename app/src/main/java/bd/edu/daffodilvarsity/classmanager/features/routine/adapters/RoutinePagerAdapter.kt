package bd.edu.daffodilvarsity.classmanager.features.routine.adapters


import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import bd.edu.daffodilvarsity.classmanager.features.routine.ui.RoutineTabItemFragment
import bd.edu.daffodilvarsity.classmanager.utils.HelperClass

class RoutinePagerAdapter(fragment: Fragment): FragmentStateAdapter(fragment) {

    override fun getItemCount(): Int = 7

    override fun createFragment(position: Int): Fragment {
        return RoutineTabItemFragment()
            .apply {
            arguments = bundleOf(
                "day" to HelperClass.getDayOfWeek(position),
            )
        }
    }

}