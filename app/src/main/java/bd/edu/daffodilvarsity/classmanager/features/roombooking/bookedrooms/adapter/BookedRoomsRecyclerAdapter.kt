package bd.edu.daffodilvarsity.classmanager.features.roombooking.bookedrooms.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.RecyclerView
import bd.edu.daffodilvarsity.classmanager.R
import bd.edu.daffodilvarsity.classmanager.common.diffcallbacks.DiffCallbacks.BOOKED_ROOM_ITEM_DIFF_CALLBACK
import bd.edu.daffodilvarsity.classmanager.common.models.BookedRoom
import bd.edu.daffodilvarsity.classmanager.databinding.ListItemBookedRoomsBinding
import bd.edu.daffodilvarsity.classmanager.utils.HelperClass
import com.google.android.material.button.MaterialButton
import java.text.SimpleDateFormat
import java.util.*

class BookedRoomsRecyclerAdapter: RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val differ = AsyncListDiffer(this, BOOKED_ROOM_ITEM_DIFF_CALLBACK)

    private var bookedRooms: List<BookedRoom>
        get() = differ.currentList
        set(value) = differ.submitList(value)

    @SuppressLint("SimpleDateFormat")
    private val dateFormatter = SimpleDateFormat("EEE, d MMM, yyyy")

    private lateinit var binding: ListItemBookedRoomsBinding

    var onCancelClicked: ((BookedRoom) -> Unit)? = null

    fun updateItems(updatedRooms: List<BookedRoom>) { bookedRooms = updatedRooms }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        binding = DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.list_item_booked_rooms,parent,false)
        val viewHolder = ViewHolder(binding.root)
        viewHolder.itemView.findViewById<MaterialButton>(R.id.cancel_book).setOnClickListener {
            onCancelClicked?.invoke(bookedRooms[viewHolder.absoluteAdapterPosition])
        }
        return viewHolder
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val binding: ListItemBookedRoomsBinding? = DataBindingUtil.findBinding(holder.itemView)

        binding?.courseName?.text = bookedRooms[position].courseName

        binding?.courseCode?.text = bookedRooms[position].courseCode

        binding?.room?.text = bookedRooms[position].room

        val time = "${HelperClass.getTimeStringFromSecond(bookedRooms[position].startTime)}-${HelperClass.getTimeStringFromSecond(bookedRooms[position].endTime)}"
        binding?.time?.text = time

        binding?.date?.text = dateFormatter.format(bookedRooms[position].reservationDate)

        binding?.section?.text = bookedRooms[position].section

        binding?.shift?.text = bookedRooms[position].shift

        binding?.department?.text = HelperClass.getDepartmentShortFrom(bookedRooms[position].department)

        binding?.campus?.text = bookedRooms[position].campus

        binding?.cancelBook?.isEnabled = checkIfClassCanBeCancelled(
            Calendar.getInstance().apply { setTime(bookedRooms[position].reservationDate) },
            Calendar.getInstance().apply { setTime(bookedRooms[position].timeWhenBooked) }
        )

        binding?.whenBooked?.text = howMuchAgoRoomWasBooked(
            Calendar.getInstance().apply { setTime(bookedRooms[position].timeWhenBooked) }
        )
    }

    private fun checkIfClassCanBeCancelled(reservationDate: Calendar, timeWhenBooked: Calendar): Boolean {
        val today = Calendar.getInstance().apply {
            set(Calendar.HOUR_OF_DAY,0)
            set(Calendar.MINUTE,0)
            set(Calendar.SECOND,0)
            set(Calendar.MILLISECOND,0)
        }
        if(today.compareTo(reservationDate) == 0 || (Calendar.getInstance().timeInMillis - timeWhenBooked.timeInMillis) > 86400000) {
            return false
        }
        return true
    }

    private fun howMuchAgoRoomWasBooked(date: Calendar): String {
        val timeDiff = (date.timeInMillis - System.currentTimeMillis()) / 1000
        return when(timeDiff/86400) {
            0.toLong() -> "Booked less than a day ago"
            else -> "Booked more than a day ago"
        }
    }

    override fun getItemCount(): Int {
        return bookedRooms.size
    }

    class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView)
}