package bd.edu.daffodilvarsity.classmanager.features.completeprofile.student

import android.view.View
import android.view.ViewGroup
import android.widget.NumberPicker
import ernestoyaquello.com.verticalstepperform.Step

/*
* Campus Picker step for students
*/
class CampusStep : Step<String>("Campus") {

    private val campusPicker: NumberPicker by lazy {
        NumberPicker(context).apply {
            val campusList = arrayOf("Main Campus", "Permanent Campus")
            minValue = 0
            maxValue = campusList.size - 1
            displayedValues = campusList

            val params = ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
            )
            layoutParams = params
        }
    }

    override fun createStepContentLayout(): View {
        campusPicker.setOnValueChangedListener { _, _, _ ->
            markAsCompletedOrUncompleted(true)
        }
        return campusPicker
    }

    override fun isStepDataValid(stepData: String?): IsDataValid {
        return if (stepData == "Main Campus" || stepData == "Permanent Campus") {
            IsDataValid(true)
        } else {
            IsDataValid(false)
        }
    }

    override fun getStepDataAsHumanReadableString(): String {
        return campusPicker.displayedValues[campusPicker.value]
    }

    override fun getStepData(): String {
        return campusPicker.displayedValues[campusPicker.value]
    }

    override fun onStepOpened(animated: Boolean) {

    }

    override fun onStepClosed(animated: Boolean) {

    }

    override fun onStepMarkedAsCompleted(animated: Boolean) {

    }

    override fun onStepMarkedAsUncompleted(animated: Boolean) {

    }

    override fun restoreStepData(data: String?) {
        campusPicker.value = campusPicker.displayedValues.indexOf(data)
    }
}