package bd.edu.daffodilvarsity.classmanager.common.database

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.asLiveData
import androidx.room.Room
import androidx.sqlite.db.SimpleSQLiteQuery
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.SmallTest
import bd.edu.daffodilvarsity.classmanager.common.getOrAwaitValue
import bd.edu.daffodilvarsity.classmanager.common.models.BookedRoom
import bd.edu.daffodilvarsity.classmanager.common.models.ClassDetails
import bd.edu.daffodilvarsity.classmanager.common.models.Course
import bd.edu.daffodilvarsity.classmanager.common.models.NotificationModel
import com.google.common.truth.Truth.assertThat
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.runBlockingTest
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import java.util.*
import java.util.concurrent.Executors

@RunWith(AndroidJUnit4::class)
@SmallTest
@ExperimentalCoroutinesApi
class ClassManagerDAOTest {

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    private lateinit var dao: ClassManagerDao

    private lateinit var db: ClassManagerDB

    private val dummyCourses: List<Course>
    get() {
        val course1 = Course(1, 1, 1, "CSEXXX", "Some-Course", "Day", "CSE")
        val course2 = Course(2, 2, 1, "CSEXYY", "Some-Course", "Day", "CSE")
        val course3 = Course(3, 1, 2, "CSEXXZ", "Some-Course", "Day", "CSE")
        val course4 = Course(4, 1, 1, "CSEYXZ", "Some-Course", "Day", "CSE")
        return listOf(course1, course2, course3, course4)
    }

    private val dummyClasses: List<ClassDetails>
    get() {
        val class1 = ClassDetails(1, "CSE122", "CSEXXX", "COURSE_NAME", "XXXX", 1000, 2000, "CSE", "Day", "Main Campus", "Saturday", "E", true)
        val class2 = ClassDetails(2, "CSE123", "CSEYYY", "COURSE_NAME", "XXXX", 1000, 2000, "CSE", "Day", "Main Campus", "Saturday", "B")
        val class3 = ClassDetails(3, "", "", "COURSE_NAME", "WXYZ", 1000, 2000, "CSE", "Day", "Main Campus", "Sunday", "B")
        val class4 = ClassDetails(4, "CSE124", "CSEXXX", "COURSE_NAME", "WXYZ", 1000, 2000, "CSE", "Day", "Main Campus", "Monday", "A")
        val class5 = ClassDetails(5, "CSE125", "CSEXXX", "COURSE_NAME", "XXXX", 1000, 2000, "CSE", "Day", "Main Campus", "Friday", "C")
        val class6 = ClassDetails(6, "", "", "COURSE_NAME", "WXYZ", 1000, 2000, "CSE", "Day", "Main Campus", "Sunday", "C")
        return listOf(class1, class2, class3, class4, class5, class6)
    }

    private val dummyBookedRooms: List<BookedRoom>
    get() {
        val tempDate = Date(1606384033)
        val room1 = BookedRoom(1, "CSEXXX", "course-name", "room", "X", 1000, 2000, "Day", "Main Campus", "CSE", "NAME", "INITIAL", "email", tempDate, tempDate)
        val room2 = BookedRoom(2, "CSEXXX", "course-name", "room", "X", 1000, 2000, "Day", "Main Campus", "CSE", "NAME", "INITIAL", "email", tempDate, tempDate)
        val room3 = BookedRoom(3, "CSEXXX", "course-name", "room", "X", 1000, 2000, "Day", "Main Campus", "CSE", "NAME", "INITIAL", "email", tempDate, tempDate)
        return listOf(room1, room2, room3)
    }

    private val dummyNotifications: List<NotificationModel>
    get() {
        val tempDate = Date(1606384033)
        val notification1 = NotificationModel(1, "Title", "Body", tempDate)
        val notification2 = NotificationModel(2, "Title", "Body", tempDate, true)
        val notification3 = NotificationModel(3, "Title", "Body", tempDate,)
        return listOf(notification1, notification2, notification3)
    }

    @Before
    fun setUp() {
        db = Room.inMemoryDatabaseBuilder(
            ApplicationProvider.getApplicationContext(),
            ClassManagerDB::class.java
        ).allowMainThreadQueries().build()
        dao = db.classManagerDao()
    }

    @After
    fun tearDown() {
        db.close()
    }

    @Test
    fun insertCourses_insertSuccess_insertedCoursesReturned() = runBlockingTest {
        // Arrange
        // Act
        dao.insertCourses(dummyCourses)

        val courses = dao.getAllCourses("Day", "CSE")
        // Assert
        assertThat(courses.containsAll(dummyCourses)).isTrue()
    }

    @Test
    fun deleteAllCourses_deletionSuccess_noCoursesOnTheList() = runBlockingTest {

        // Arrange
        insertDummyCourses()
        // Act
        dao.deleteAllCourses()

        val courses = dao.getAllCourses("Day", "CSE")
        // Assert
        assertThat(courses).doesNotContain(dummyCourses[0])
        assertThat(courses).doesNotContain(dummyCourses[1])
        assertThat(courses).doesNotContain(dummyCourses[2])
    }

    @Test
    fun getCoursesWithLevelTerm_coursesReturned() = runBlockingTest {
        // Arrange
        insertDummyCourses()
        // Act
        val coursesL1T1 = dao.getCoursesWithLevelTerm(1, 1, "Day", "CSE")
        // Assert
        assertThat(coursesL1T1).hasSize(2)
        assertThat(coursesL1T1).contains(dummyCourses[0])
        assertThat(coursesL1T1).contains(dummyCourses[3])
    }

    @Test
    fun getCoursesWithShiftAndDepartment_coursesReturnedForThatShiftAndDepartment() = runBlockingTest {
        // Arrange
        insertDummyCourses()
        // Act
        val result = dao.getCoursesWithShiftAndDepartment("Day", "CSE").getOrAwaitValue()

        // Assert
        assertThat(result.containsAll(dummyCourses)).isTrue()
    }

    @Test
    fun insertClasses_insertedSuccessfully_insertedClassesReturned() = runBlockingTest {
        // Arrange
        dao.insertClasses(dummyClasses)

        // Act
        val classes = dao.getAllClasses()

        // Assert
        assertThat(classes.containsAll(dummyClasses)).isTrue()
    }

    @Test
    fun deleteAllClasses_deletedSuccessfully_emptyListReturned() = runBlockingTest {
        // Arrange
        insertDummyClasses()
        // Act
        dao.deleteAllClasses()
        val classes = dao.getAllClasses()
        // Assert
        assertThat(classes).isEmpty()
    }

    @Test
    fun refreshAllClasses_oldClassesDeletedNewClassesInserted_newClassesReturned() = runBlocking {
        // Arrange
        /*
        * we need to set transaction executor to
        * SingleThreadExecutor to pass this test
        * that is why Room was initialized again in
        * this function
        * */
        db = Room.inMemoryDatabaseBuilder(
            ApplicationProvider.getApplicationContext(),
            ClassManagerDB::class.java
        ).setTransactionExecutor(Executors.newSingleThreadExecutor()).allowMainThreadQueries().build()
        dao = db.classManagerDao()

        insertDummyClasses()
        // Act
        val cd = ClassDetails(0, "ROOM", "CODE", "COURSE-NAME", "INITIAL", 0,0,"CSE", "Day", "Dhanmondi", "Saturday", "E", false)

        dao.refreshAllClasses(listOf(cd))
        // Assert
        val result = dao.getAllClasses()
        assertThat(result).hasSize(1)
        assertThat(result).contains(cd)
    }

    @Test
    fun getDistinctSection_distinctSection_sectionReturn() = runBlockingTest {
        // Arrange
        insertDummyClasses()
        // Act
        val distinctSections = dao.getDistinctSections().getOrAwaitValue()
        // Assert
        assertThat(distinctSections.containsAll(listOf("A", "B", "C", "E"))).isTrue()
    }

    @Test
    fun getTeacherRoutine_teacherRoutineByInitial_teacherClassesReturned() = runBlockingTest {
        // Arrange
        insertDummyClasses()
        // Act
        val teacherClasses = dao.getTeacherRoutine("XXXX", "Saturday").asLiveData().getOrAwaitValue()
        // Assert
        assertThat(teacherClasses).hasSize(2)
        assertThat(teacherClasses).contains(dummyClasses[0])
        assertThat(teacherClasses).contains(dummyClasses[1])
    }

    @Test
    fun getTeacherWholeRoutineAsList_teacherWholeRoutineWithInitial_wholeRoutineReturned() = runBlockingTest {
        // Arrange
        insertDummyClasses()
        // Act
        val teacherClasses = dao.getTeacherWholeRoutineAsList("XXXX")
        // Assert
        assertThat(teacherClasses).hasSize(3)
        assertThat(teacherClasses).contains(dummyClasses[0])
        assertThat(teacherClasses).contains(dummyClasses[1])
        assertThat(teacherClasses).contains(dummyClasses[4])
    }

    @Test
    fun getTeacherRoutineForSpecificDayAsList_teacherRoutineForADay_routineReturned() = runBlockingTest{
        // Arrange
        insertDummyClasses()
        // Act
        val teacherClasses = dao.getTeacherRoutineForSpecificDayAsList("XXXX", "Friday")
        // Assert
        assertThat(teacherClasses).hasSize(1)
        assertThat(teacherClasses).contains(dummyClasses[4])
    }

    @Test
    fun getStudentRoutineAsList_studentRoutine_studentClassesListReturned() = runBlockingTest {
        // Arrange
        insertDummyClasses()
        // Act
        val query = """select * from ClassDetails where courseCode = "CSEXXX" and shift = "Day""""
        val result = dao.getStudentRoutineAsList(SimpleSQLiteQuery(query))
        // Assert
        assertThat(result).hasSize(3)
        assertThat(result).contains(dummyClasses[0])
        assertThat(result).contains(dummyClasses[3])
        assertThat(result).contains(dummyClasses[4])
    }

    @Test
    fun getStudentRoutine_getStudentRoutine_routineReturned() = runBlockingTest {
        // Arrange
        insertDummyClasses()
        // Act

        val result = dao.getStudentRoutine(
            mutableListOf("CSEXXX", "CSEYYY", "CSEZZZ"),
            "E",
            "Day",
            "Main Campus",
            "CSE"
        )
        // Assert
        assertThat(result).hasSize(1)
        assertThat(result).contains(dummyClasses[0])
    }

    @Test
    fun getRoutineWithCourseCode_routineForSpecificCourseCode_routineReturned() = runBlockingTest {
        // Arrange
        insertDummyClasses()
        // Act
        val result = dao.getRoutineWithCourseCode("CSEYYY", "Day", "Main Campus", "CSE")
        // Assert
        assertThat(result).hasSize(1)
        assertThat(result).contains(dummyClasses[1])
    }

    @Test
    fun getEmptyRooms_emptyRoomReturned() = runBlockingTest {
        // Arrange
        insertDummyClasses()
        // Act
        val result = dao.getEmptyRooms("Sunday", 1000, 2000, "Main Campus")
        // Assert
        assertThat(result).hasSize(2)
        assertThat(result).contains(dummyClasses[2])
        assertThat(result).contains(dummyClasses[5])
    }

    @Test
    fun toggleNotification_changeNotificationStatus_notificationStatusChanged() = runBlockingTest {
        // Arrange
        insertDummyClasses()
        // Act
        dao.toggleNotification(1, false)
        dao.toggleNotification(2, true)
        dao.toggleNotification(3, false)
        val result1 = dao.getStudentRoutineAsList(SimpleSQLiteQuery("select * from ClassDetails where id = \"1\""))[0]
        val result2 = dao.getStudentRoutineAsList(SimpleSQLiteQuery("select * from ClassDetails where id = \"2\""))[0]
        val result3 = dao.getStudentRoutineAsList(SimpleSQLiteQuery("select * from ClassDetails where id = \"3\""))[0]
        // Assert
        assertThat(result1.notification).isFalse()
        assertThat(result2.notification).isTrue()
        assertThat(result3.notification).isFalse()
    }

    /*
    * Test related to local booked room storage
    * */

    @Test
    fun insertBookedRooms_roomInserted() = runBlockingTest {
        // Arrange
        insertDummyBookedRooms()
        // Act
        val rooms = dao.getAllBookedClasses().getOrAwaitValue()
        // Assert
        assertThat(rooms).hasSize(3)
        assertThat(rooms.containsAll(dummyBookedRooms)).isTrue()
    }

    @Test
    fun deleteAllBookedRooms_roomsDeleted() = runBlockingTest {
        // Arrange
        insertDummyBookedRooms()
        // Act
        dao.deleteAllBookedClasses()
        val rooms = dao.getAllBookedClasses().getOrAwaitValue()
        // Assert
        assertThat(rooms).isEmpty()
    }

    /*
    * Test related to Notification
    * */

    @Test
    fun insertNotification_notificationInserted() = runBlockingTest {
        // Arrange
        insertDummyNotification()
        // Act
        val notifications = dao.getNotifications().getOrAwaitValue()
        // Assert
        assertThat(notifications.containsAll(dummyNotifications)).isTrue()
    }

    @Test
    fun getUnreadNotificationCount_unreadNotificationCountReturned() = runBlockingTest {
        // Arrange
        insertDummyNotification()
        // Act
        val count = dao.getUnreadNotificationCount().getOrAwaitValue()
        // Assert
        assertThat(count).isEqualTo(2)
    }

    @Test
    fun updateReadStatus_readStatusUpdated() = runBlockingTest {
        // Arrange
        insertDummyNotification()
        // Act
        dao.updateReadStatus(2, false)
        val unreadCount = dao.getUnreadNotificationCount().getOrAwaitValue()
        // Assert
        assertThat(unreadCount).isEqualTo(3)
    }

    @Test
    fun markAllAsRead_allNotificationIsMarkedAsRead() = runBlockingTest {
        // Arrange
        insertDummyNotification()
        // Act
        dao.updateReadStatus(1, true)
        dao.updateReadStatus(3, true)
        val unreadCount = dao.getUnreadNotificationCount().getOrAwaitValue()
        // Assert
        assertThat(unreadCount).isEqualTo(0)
    }

    @Test
    fun deleteAllFCMNotifications_allNotificationsDeleted() = runBlockingTest {
        // Arrange
        insertDummyNotification()
        // Act
        dao.deleteAllFCMNotifications()
        val notifications = dao.getNotifications().getOrAwaitValue()
        // Assert
        assertThat(notifications).isEmpty()
    }

    /*
    * These are some helper functions
    * */
    private suspend fun insertDummyCourses() {
        dao.insertCourses(dummyCourses)
    }

    private suspend fun insertDummyClasses() {
        dao.insertClasses(dummyClasses)
    }

    private suspend fun insertDummyBookedRooms() {
        dao.insertBookedClasses(dummyBookedRooms)
    }

    private suspend fun insertDummyNotification() {
        for (notification in dummyNotifications) {
            dao.insertNotification(notification)
        }
    }

}