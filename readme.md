

## [**Class Manager**](https://play.google.com/store/apps/details?id=bd.edu.daffodilvarsity.classmanager&hl=en&gl=US) 

**This version isn't released in Google Play Store yet.** This is the modern Kotlin version of Class Manager app which uses a REST backend. This application was developed for DIU to view students' and teachers' routines and room booking. Besides its main purpose, there are a lot of features available for both students and teachers. Java Version:  [**Play Store Link**](https://play.google.com/store/apps/details?id=bd.edu.daffodilvarsity.classmanager&hl=en&gl=US)

- **Tech Stack (Kotlin):** Hilt, Coroutine, Retrofit, Room, ViewModel, LiveData, Navigation Component, Paging3, Work Manager, ViewPager 2, Moshi, FCM etc.

 
 ![](/assets/cm_image1.png){width=150}
 ![](/assets/cm_image2.png){width=150}
 ![](/assets/cm_image3.png){width=150}
 ![](/assets/cm_image4.png){width=150}
 ![](/assets/cm_image5.png){width=150}

